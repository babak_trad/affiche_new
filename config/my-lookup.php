<?php

return [
    'affiche' => [
        'types' => [
            'portable' => [
                'name' => 'پرتابل',
                'id' => 1,
                'creators' => ['tv-production'],
                'contributor-roles' => [
                    'producer',
                    'director',
                    'director_assistant',
                    'sound_recordist',
                    'sound_recordist_assistant',
                    'producer_assistant',
                    'cameraman',
                    'tech_assistant',
                ],
                'visitors' => ['technical-support', 'ware-house', 'physical-security']
            ],
            'car' => [
                'name' => 'خودرو',
                'id' => 2,
                'creators' => ['tv-production'],
                'contributor-roles' => ['contributors'],
                'visitors' => ['transportation', 'physical-security']
            ],
            'radio' => [
                'name' => 'رادیو',
                'id' => 3,
                'creators' => [],
                'visitors' => ['technical-support', 'ware-house', 'physical-security']
            ],
            'video-conference' => [
                'name' => 'ویدئو کنفرانس',
                'id' => 4,
                'creators' => [],
                'visitors' => ['technical-support', 'ware-house', 'physical-security']
            ],
            'sng' => [
                'name' => 'ماهواره',
                'id' => 5,
                'creators' => [],
                'visitors' => ['technical-support', 'ware-house', 'physical-security']
            ],
        ],
        'flow' => [
            'foreward' => 0,
            'backward' => 1,
            'done' => 2
        ],
        'statuses' => [
            'rejected' => 0,
            'active' => 1,
            'archive' => 2,
            'draft' => 3
        ]
    ],
    'ware' => [
        'types' => [
            'uncountable' => [
                'id' => 1,
                'typeOf' => 'default'
            ],
            'countable' => [
                'id' => 2,
                'typeOf' => 'default'
            ],
        ],
        'statuses' => [
            'pending' => 0,
            'accepted' => 1,
            'rejected' => 2
        ]
    ],
    'notifications' => [
        'types' => [
            'affiche' => 1,
            'message' => 2,
            'ticket' => 3
        ]
    ]
];
