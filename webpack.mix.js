const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//  --------------- vendors ----------------
mix.copyDirectory("resources/assets/vendor", "public/vendor/");
mix.copyDirectory("resources/assets/fonts", "public/fonts/");
mix.copy(
    "node_modules/select2/dist/js/select2.min.js",
    "public/vendor/js/select2.min.js"
);
mix.copy(
    "node_modules/select2/dist/css/select2.min.css",
    "public/vendor/css/select2.min.css"
);
mix.copy(
    "node_modules/alpinejs/dist/cdn.min.js",
    "public/vendor/js/alpine.min.js"
);

// ---------------- styles -----------------
mix.styles("resources/assets/css/errors/403.css", "public/css/errors/403.css");
mix.styles("resources/assets/css/errors/405.css", "public/css/errors/405.css");
mix.styles("resources/assets/css/custom.css", "public/css/custom.css");

// ---------------- scripts ----------------
mix.js(
    "resources/assets/js/datatables-init.js",
    "public/js/datatables-init.js"
);
mix.js(
    "resources/assets/js/select2-maintainer.js",
    "public/js/select2-maintainer.js"
);
mix.js("resources/assets/js/main-script.js", "public/js/main-script.js");

// ---------------- GIF's ----------------
mix.copyDirectory("resources/assets/gif/", "public/GIFs");

// ---------------- images ----------------
mix.copyDirectory("resources/assets/images/", "public/images");

// ---------------- sounds ----------------
mix.copyDirectory("resources/assets/sounds/", "public/sounds");
