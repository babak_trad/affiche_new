<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('users.{user_id}', function ($user, $userId) {
    return $user->id === (int) $userId;
});

Broadcast::channel('admins.{user_id}', function ($user, $userId) {
    return Auth::guard('web_admin')->id() === (int) $userId;
});
