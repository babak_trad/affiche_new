<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Auth'], function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('login.form');
    Route::post('/login', 'LoginController@login')->name('login');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('login.form');
    Route::post('/login', 'LoginController@login')->name('login');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});

//admin
Route::group(
    ['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'auth:web_admin'],
    function () {

        //users
        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/', 'UsersController@list')->name('list');
            Route::get('/create', 'UsersController@create')->name('create');
            Route::post('/store', 'UsersController@store')->name('store');
            Route::get('/edit/{id}', 'UsersController@edit')->name('edit');
            Route::put('/update/{id}', 'UsersController@update')->name('update');
            Route::delete('/delete/{id}', 'UsersController@delete')->name('delete');
        });

        //roles
        Route::group(['prefix' => 'roles', 'as' => 'roles.'], function () {
            Route::get('/', 'RoleController@list')->name('list');
            Route::get('/create', 'RoleController@create')->name('create');
            Route::post('/store', 'RoleController@store')->name('store');
            Route::get('/edit/{id}', 'RoleController@edit')->name('edit');
            Route::put('/update/{id}', 'RoleController@update')->name('update');
            Route::delete('/delete/{id}', 'RoleController@delete')->name('delete');
        });

        //units
        Route::group(['prefix' => 'units', 'as' => 'units.'], function () {
            Route::get('/', 'UnitController@list')->name('list');
            Route::get('/create', 'UnitController@create')->name('create');
            Route::post('/store', 'UnitController@store')->name('store');
            Route::get('/edit/{id}', 'UnitController@edit')->name('edit');
            Route::put('/update/{id}', 'UnitController@update')->name('update');
            Route::delete('/delete/{id}', 'UnitController@delete')->name('delete');

            //ajaxs
            Route::post('/roles', 'UnitController@rolesOfUnit');
        });

        //links
        Route::group(['prefix' => 'links', 'as' => 'links.'], function () {
            Route::get('/create', 'LinkController@create')->name('create');
            Route::post('/store', 'LinkController@store')->name('store');
            Route::get('/edit/{id}', 'LinkController@edit')->name('edit');
            Route::put('/update/{id}', 'LinkController@update')->name('update');
            Route::delete('/delete/{link}', 'LinkController@delete')->name('delete');
        });
    }
);

// find users
Route::group(
    ['prefix' => 'users', 'namespace' => 'Admin', 'as' => 'users.', 'middleware' => 'auth:web,web_admin'],
    function () {
        Route::post('/role', 'UsersController@findByRole')->name('role');
        Route::post('/unit', 'UsersController@findByUnit')->name('unit');
    }
);

Route::group(['middleware' => 'auth:web,web_admin'], function () {
    //dashboard
    Route::get('/', 'DashboardController@index')->name('dashboard');

    //affiches
    Route::group(['prefix' => 'affiches', 'namespace' => 'Affiche', 'as' => 'affiches.'], function () {
        Route::get('/list/{type?}', 'AfficheCommonController@list')->name('list');

        Route::post('/action-form/{id}/{action}', 'AfficheCommonController@actionForm')->name('action.form');
        Route::get('/action', 'AfficheCommonController@action')->name('action');
        Route::post('/accept/{id}', 'AfficheCommonController@accept')->name('accept');
        Route::post('/reject/{id}', 'AfficheCommonController@reject')->name('reject');
        Route::post('/user/affiches', 'AfficheCommonController@userAffiches')->name('user.affiches');

        Route::group(['as' => 'creator.'], function () {
            Route::get('/create/{type}', 'AfficheCreatorController@create')->name('create');
            Route::post('/store', 'AfficheCreatorController@store')->name('store');
            Route::get('/edit/{type}/{id}', 'AfficheCreatorController@edit')->name('edit');
            Route::put('/update/{id}', 'AfficheCreatorController@update')->name('update');
            Route::delete('/delete/{id}', 'AfficheCreatorController@delete')->name('delete');
            Route::post('/send/{id}', 'AfficheCreatorController@send')->name('send');
            Route::post('/archive/{id}', 'AfficheCreatorController@archive')->name('archive');
        });

        Route::group(['prefix' => 'contributors', 'as' => 'contributors.'], function () {
            Route::post('/list', 'AfficheContributorsController@list')->name('list');
        });

        Route::group(['prefix' => 'wares', 'as' => 'wares.'], function () {
            Route::post('/list/{affiche_type}/{affiche_id}', 'AfficheWaresController@list')->name('list');
            Route::get('/view', 'AfficheWaresController@view')->name('view');
            Route::get('/edit/{affiche_type}/{affiche_id}', 'AfficheWaresController@edit')->name('edit');
            Route::put('/update/{affiche_id}', 'AfficheWaresController@update')->name('update');
        });

        Route::group(['prefix' => 'alarms', 'as' => 'alarms.'], function () {
            Route::post('/clear', 'AfficheCommonController@clearAlarms')->name('clear');
        });
    });

    //messages
    Route::group(['prefix' => 'messages', 'namespace' => 'Message', 'as' => 'messages.'], function () {
        Route::get('/', 'MessageController@list')->name('list');
        Route::get('/create', 'MessageController@create')->name('create');
        Route::post('/store', 'MessageController@store')->name('store');
        Route::get('/edit/{id}', 'MessageController@edit')->name('edit');
        Route::put('/update/{id}', 'MessageController@update')->name('update');
        Route::delete('/delete/{id}', 'MessageController@delete')->name('delete');
    });

    //wares
    Route::group(['prefix' => 'wares', 'namespace' => 'Ware', 'as' => 'wares.'], function () {
        Route::get('/{type}', 'WaresController@list')->name('list');
        Route::get('/create/{type}', 'WaresController@create')->name('create');
        Route::post('/store', 'WaresController@store')->name('store');
        Route::get('/edit/{type}/{id}', 'WaresController@edit')->name('edit');
        Route::put('/update/{id}', 'WaresController@update')->name('update');
        Route::delete('/delete/{id}', 'WaresController@delete')->name('delete');
    });

    //categories
    Route::group([
        'prefix' => 'categories',
        'namespace' => 'Category',
        'as' => 'categories.',
        'middleware' => 'can:wares.create'
    ], function () {
        Route::get('/', 'CategoriesController@list')->name('list');
        Route::get('/create', 'CategoriesController@create')->name('create');
        Route::post('/store', 'CategoriesController@store')->name('store');
        Route::get('/edit/{category}', 'CategoriesController@edit')->name('edit');
        Route::put('/update/{category}', 'CategoriesController@update')->name('update');
        Route::delete('/delete/{category}', 'CategoriesController@delete')->name('delete');
    });

    //cars
    Route::group(['prefix' => 'cars', 'namespace' => 'Car', 'as' => 'cars.'], function () {
        Route::get('/', 'CarController@list')->name('list');
        Route::get('/create', 'CarController@create')->name('create');
        Route::post('/store', 'CarController@store')->name('store');
        Route::get('/edit/{car}', 'CarController@edit')->name('edit');
        Route::put('/update/{car}', 'CarController@update')->name('update');
        Route::delete('/delete/{car}', 'CarController@delete')->name('delete');
        Route::post('/search', 'CarController@search')->name('search');
    });

    //Links
    Route::get('/links', 'Link\\LinksController@list')->name('links.list');

    //Notifications
    Route::group([
        'prefix' => 'notifications',
        'namespace' => 'Notification',
        'as' => 'notification.'
    ], function () {
        Route::post('/list', 'NotificationController@list')->name('list');
        Route::post('/clear', 'NotificationController@clear')->name('clear');
    });
});
