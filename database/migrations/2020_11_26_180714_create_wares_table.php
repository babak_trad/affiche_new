<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wares', function (Blueprint $table) {
            $table->id();
            $table->string('device_name');
            $table->string('part_number');
            $table->string('serial_number')->unique();
            $table->integer('stock')->default(1);
            $table->tinyInteger('type')->default(1);
            //TODO: When the transferee is deleted, the ware should be deleted?
            $table->unsignedBigInteger('transferee');
            $table->unsignedBigInteger('wareable_id')->nullable();
            $table->string('wareable_type')->nullable();
            $table->timestamps();
        });

        Schema::create('wareables', function (Blueprint $table) {
            $table->unsignedBigInteger('ware_id');
            $table->unsignedBigInteger('wareable_id');
            $table->string('wareable_type');
            $table->tinyInteger('ware_type')->default(1);
            $table->mediumInteger('ware_quantity')->default(1);
            $table->tinyInteger('ware_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wares');
        Schema::dropIfExists('wareables');
    }
}
