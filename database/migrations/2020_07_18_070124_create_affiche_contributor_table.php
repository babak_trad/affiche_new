<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfficheContributorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiche_contributor', function (Blueprint $table) {
            $table->unsignedBigInteger('affiche_id');
            $table->tinyInteger('affiche_type');
            $table->unsignedBigInteger('user_id');

            // $table->foreign('affiche_id')->references('id')->on('portable_affiches')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiche_contributor');
    }
}
