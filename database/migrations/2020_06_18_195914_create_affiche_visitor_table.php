<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfficheVisitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiche_visitor', function (Blueprint $table) {
            $table->unsignedBigInteger('affiche_id');
            $table->tinyInteger('affiche_type');
            $table->unsignedBigInteger('visitor_id');
            $table->unsignedTinyInteger('action')->default(0);
            $table->text('comment')->nullable();
            $table->string('next_unit')->nullable();
            $table->boolean('owner_has_not_seen')->default(false);
            $table->tinyInteger('flow')->default(0);

            // $table->foreign('affiche_id')->references('id')->on('affiches')->onDelete('cascade');
            $table->foreign('visitor_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiche_visitor');
    }
}
