<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortableAffichesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portable_affiches', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->tinyInteger('type')->default(1);
            $table->BigInteger('user_id')->unsigned();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->dateTime('security_enter_date')->nullable();
            $table->dateTime('security_exit_date')->nullable();
            $table->dateTime('warehouse_enter_date')->nullable();
            $table->dateTime('warehouse_exit_date')->nullable();
            $table->tinyInteger('status')->default(3);
            $table->string('current_state')->nullable();
            $table->tinyInteger('flow')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('affiches');
        // Schema::enableForeignKeyConstraints();
    }
}
