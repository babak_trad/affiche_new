<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarAffichesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_affiches', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->tinyInteger('type')->default(2);
            $table->tinyInteger('status')->default(3);
            $table->tinyInteger('flow')->default(0);
            $table->char('current_state');
            $table->unsignedBigInteger('issuer_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();
            $table->mediumText('to_description')->nullable();
            $table->mediumText('for_description')->nullable();
            $table->timestamp('start_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('end_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('execution_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('security_enter_date')->nullable();
            $table->timestamp('security_exit_date')->nullable();
            $table->text('issuer_note')->nullable();
            $table->unsignedBigInteger('carable_id')->nullable();
            $table->string('carable_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_affiches');
    }
}
