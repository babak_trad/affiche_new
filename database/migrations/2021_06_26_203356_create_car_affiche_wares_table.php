<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarAfficheWaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_affiche_wares', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->char('serial_number')->unique();
            $table->unsignedInteger('quantity')->default(1);
            $table->unsignedBigInteger('car_affiche_id');

            $table->foreign('car_affiche_id')->references('id')->on('car_affiches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_affiche_wares');
    }
}
