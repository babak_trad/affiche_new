<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Unit;
use App\Models\User;
use Ybazli\Faker\Facades\Faker as PersianFaker;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => PersianFaker::firstName(),
        'family_name' => PersianFaker::lastName(),
        'employee_num' => $faker->unique()->numberBetween(10, 99),
        'phone_num' => PersianFaker::mobile(),
        'unit_id' => mt_rand(1, 6),
        'password' => bcrypt('1'),
    ];
});
