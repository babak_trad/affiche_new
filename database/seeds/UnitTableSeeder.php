<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('units')->insert([
            'name' => 'فناوری اطلاعات',
            'department' => 'فنی',
            'slug' => 'it'
        ]);

        //2
        DB::table('units')->insert([
            'name' => 'پشتیبانی فنی',
            'department' => 'فنی',
            'slug' => 'technical-support'
        ]);

        //3
        DB::table('units')->insert([
            'name' => 'انبار فنی',
            'department' => 'فنی',
            'slug' => 'ware-house'
        ]);

        //4
        DB::table('units')->insert([
            'name' => 'تولید سیما',
            'department' => 'سیما',
            'slug' => 'tv-production',
        ]);

        //5
        DB::table('units')->insert([
            'name' => 'مایکروویو',
            'department' => 'فنی',
            'slug' => 'microwave',
        ]);

        //6
        DB::table('units')->insert([
            'name' => 'ترابری',
            'department' => 'اداری و مالی',
            'slug' => 'transportation',
        ]);

        //7
        DB::table('units')->insert([
            'name' => 'حراست فیزیکی',
            'department' => 'حراست',
            'slug' => 'physical-security',
        ]);
    }
}
