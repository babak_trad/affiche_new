<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admins
        DB::table('admins')->insert([
            'name' => 'سلمان',
            'email' => 'sali@gmail.com',
            'password' => Hash::make('1')
        ]);

        //Technical Support
        DB::table('users')->insert([
            'name' => 'پرویز',
            'family_name' => 'عبداله زاده',
            'employee_num' => '101',
            'phone_num' => '9123456789',
            'unit_id' => 1,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'عارف',
            'family_name' => 'عبدلی',
            'employee_num' => '102',
            'phone_num' => '9123456789',
            'unit_id' => 2,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'عزیزاله',
            'family_name' => 'مسعودپور',
            'employee_num' => '103',
            'phone_num' => '9123456789',
            'unit_id' => 3,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'علی',
            'family_name' => 'باقری',
            'employee_num' => '104',
            'phone_num' => '9123456789',
            'unit_id' => 3,
            'password' => Hash::make('1'),
        ]);

        //TV and Radio

        DB::table('users')->insert([
            'name' => 'رحمت',
            'family_name' => 'رمضانی',
            'employee_num' => '105',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'یوسف',
            'family_name' => 'علی آبادی',
            'employee_num' => '106',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'محمدرضا',
            'family_name' => 'فرهادی',
            'employee_num' => '107',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'احسان',
            'family_name' => 'عادلی',
            'employee_num' => '108',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'محمدرضا',
            'family_name' => 'راد',
            'employee_num' => '109',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'احسان',
            'family_name' => 'رادی',
            'employee_num' => '110',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'محمدرضا',
            'family_name' => 'رضایی',
            'employee_num' => '111',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'احسان',
            'family_name' => 'رضایی',
            'employee_num' => '112',
            'phone_num' => '9123456789',
            'unit_id' => 4,
            'password' => Hash::make('1'),
        ]);

        //Microwave
        DB::table('users')->insert([
            'name' => 'عبدالمحمد',
            'family_name' => 'احمدی',
            'employee_num' => '113',
            'phone_num' => '9123456789',
            'unit_id' => 5,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'احسان',
            'family_name' => 'مرادیان',
            'employee_num' => '114',
            'phone_num' => '9123456789',
            'unit_id' => 5,
            'password' => Hash::make('1'),
        ]);

        //Transportation
        DB::table('users')->insert([
            'name' => 'ابوالفضل',
            'family_name' => 'حسینی کیا',
            'employee_num' => '115',
            'phone_num' => '9123456789',
            'unit_id' => 6,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'شاپور',
            'family_name' => 'پورغلام',
            'employee_num' => '116',
            'phone_num' => '9123456789',
            'unit_id' => 6,
            'password' => Hash::make('1'),
        ]);

        //Protection
        DB::table('users')->insert([
            'name' => 'کورش',
            'family_name' => 'موسوی',
            'employee_num' => '117',
            'phone_num' => '9123456789',
            'unit_id' => 7,
            'password' => Hash::make('1'),
        ]);

        DB::table('users')->insert([
            'name' => 'حمید',
            'family_name' => 'شیروزاده',
            'employee_num' => '118',
            'phone_num' => '9123456789',
            'unit_id' => 7,
            'password' => Hash::make('1'),
        ]);
    }
}
