<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Technical department
        DB::table('roles')->insert([
            'name' => 'مدیر فناوری اطلاعات',
            'unit_id' => 1,
            'slug' => 'it-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'مدیر پشتیبانی فنی',
            'unit_id' => 2,
            'slug' => 'technical-support-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'جانشین پشتیبانی فنی',
            'unit_id' => 2,
            'slug' => 'technical-support-successor',
        ]);

        //Wares department
        DB::table('roles')->insert([
            'name' => 'انباردار پشتیبانی فنی',
            'unit_id' => 3,
            'slug' => 'technical-support-warehouse-keeper',
        ]);

        //TV and Radio
        DB::table('roles')->insert([
            'name' => 'مدیر تولید سیما',
            'unit_id' => 4,
            'slug' => 'tv-production-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'مدیر پخش سیما',
            'unit_id' => 4,
            'slug' => 'tv-onair-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'مدیر تولید رادیو',
            'unit_id' => 4,
            'slug' => 'radio-production-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'مدیر پخش رادیو',
            'unit_id' => 4,
            'slug' => 'radio-onair-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'هماهنگی سیما',
            'unit_id' => 4,
            'slug' => 'tv-coordinator',
        ]);

        DB::table('roles')->insert([
            'name' => 'هماهنگی رادیو',
            'unit_id' => 4,
            'slug' => 'radio-coordinator',
        ]);

        DB::table('roles')->insert([
            'name' => 'تهیه کننده سیما',
            'unit_id' => 4,
            'slug' => 'tv-producer',
        ]);

        DB::table('roles')->insert([
            'name' => 'تهیه کننده رادیو',
            'unit_id' => 4,
            'slug' => 'radio-producer',
        ]);

        DB::table('roles')->insert([
            'name' => 'دستیار تهیه کننده تلویزیون',
            'unit_id' => 4,
            'slug' => 'tv-producer-assistant',
        ]);

        DB::table('roles')->insert([
            'name' => 'دستیار تهیه کننده رادیو',
            'unit_id' => 4,
            'slug' => 'radio-producer-assistant',
        ]);

        DB::table('roles')->insert([
            'name' => 'کارگردان',
            'unit_id' => 4,
            'slug' => 'director',
        ]);

        DB::table('roles')->insert([
            'name' => 'دستیار کارگردان',
            'unit_id' => 4,
            'slug' => 'director-assistant',
        ]);

        DB::table('roles')->insert([
            'name' => 'صدابردار',
            'unit_id' => 4,
            'slug' => 'sound-recordist',
        ]);

        DB::table('roles')->insert([
            'name' => 'دستیار صدابردار',
            'unit_id' => 4,
            'slug' => 'sound-recordist-assistant',
        ]);

        DB::table('roles')->insert([
            'name' => 'تصویربردار',
            'unit_id' => 4,
            'slug' => 'cameraman',
        ]);

        DB::table('roles')->insert([
            'name' => 'دستیار فنی',
            'unit_id' => 4,
            'slug' => 'tech-assistant',
        ]);

        //Microwave
        DB::table('roles')->insert([
            'name' => 'مدیر مایکروویو',
            'unit_id' => 5,
            'slug' => 'microwave-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'جانشین مایکروویو',
            'unit_id' => 5,
            'slug' => 'microwave-successor',
        ]);

        //Transportation
        DB::table('roles')->insert([
            'name' => 'مدیر ترابری',
            'unit_id' => 6,
            'slug' => 'transportation-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'جانشین مدیر ترابری',
            'unit_id' => 6,
            'slug' => 'transportation-successor',
        ]);

        DB::table('roles')->insert([
            'name' => 'راننده',
            'unit_id' => 6,
            'slug' => 'driver',
        ]);


        //Security department
        DB::table('roles')->insert([
            'name' => 'مدیر حراست',
            'unit_id' => 7,
            'slug' => 'protection-manager',
        ]);

        DB::table('roles')->insert([
            'name' => 'حفاظت درب خودرویی',
            'unit_id' => 7,
            'slug' => 'car-gate-protection',
        ]);

        DB::table('roles')->insert([
            'name' => 'حفاظت درب پذیرش',
            'unit_id' => 7,
            'slug' => 'reception-gate-protection',
        ]);
    }
}
