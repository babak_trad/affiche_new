<?php

use Illuminate\Database\Seeder;

class LinkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('links')->insert([
            'name' => 'سامانه حقوق',
            'url' => 'http://iriboffice.ir',
            'status' => true,
        ]);
        DB::table('links')->insert([
            'name' => 'سامانه حضور و غیاب',
            'url' => 'http://172.16.1.1',
            'status' => true,
        ]);
    }
}
