<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UnitTableSeeder::class,
            RoleTableSeeder::class,
            UserTableSeeder::class,
            // LinkTableSeeder::class,
            // MessageTableSeeder::class,
            CategoryTableSeeder::class,
            WareTableSeeder::class
        ]);
    }
}
