<?php

use Illuminate\Database\Seeder;

class WareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wares')->insert([
            'device_name' => 'میکروفن',
            'part_number' => '11',
            'serial_number' => 'A11',
            'transferee' => 3,
        ]);

        DB::table('wares')->insert([
            'device_name' => 'دوربین',
            'part_number' => '22',
            'serial_number' => 'V11',
            'transferee' => 3,
        ]);

        DB::table('wares')->insert([
            'device_name' => 'نور سافت',
            'part_number' => '33',
            'serial_number' => 'L11',
            'transferee' => 3,
        ]);

        DB::table('wares')->insert([
            'device_name' => 'تراولینگ',
            'part_number' => '44',
            'serial_number' => 'M11',
            'transferee' => 3,
        ]);

        DB::table('wares')->insert([
            'device_name' => 'میکسر صدای پرتابل',
            'part_number' => '55',
            'serial_number' => 'A22',
            'transferee' => 3,
        ]);
    }
}
