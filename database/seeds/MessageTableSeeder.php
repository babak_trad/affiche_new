<?php

use Illuminate\Database\Seeder;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            'user_id' => 7,
            'subject' => 'موضوع 1',
            'message' => 'متن شماره یک جهت تست نرم افزار نوشته شده است',
            'status' => 1,
        ]);

        DB::table('messages')->insert([
            'user_id' => 2,
            'subject' => 'موضوع 2',
            'message' => 'متن شماره دو جهت تست نرم افزار نوشته شده است',
            'status' => 1,
        ]);

        DB::table('messages')->insert([
            'user_id' => 3,
            'subject' => 'موضوع 3',
            'message' => 'متن شماره سه جهت تست نرم افزار نوشته شده است',
            'status' => 1,
        ]);

        DB::table('messages')->insert([
            'user_id' => 4,
            'subject' => 'موضوع 4',
            'message' => 'متن شماره چهار جهت تست نرم افزار نوشته شده است',
            'status' => 1,
        ]);

        DB::table('messages')->insert([
            'user_id' => 5,
            'subject' => 'موضوع 5',
            'message' => 'متن شماره پنج جهت تست نرم افزار نوشته شده است',
            'status' => 1,
        ]);

        DB::table('messages')->insert([
            'user_id' => 6,
            'subject' => 'موضوع 6',
            'message' => 'متن شماره شش جهت تست نرم افزار نوشته شده است',
            'status' => 1,
        ]);
    }
}
