<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'صدا',
            'slug' => 'audio',
        ]);

        DB::table('categories')->insert([
            'name' => 'تصویر',
            'slug' => 'video',
        ]);

        DB::table('categories')->insert([
            'name' => 'نور',
            'slug' => 'light',
        ]);

        DB::table('categories')->insert([
            'name' => 'حرکتی',
            'slug' => 'mechanical',
        ]);

        DB::table('categories')->insert([
            'name' => 'سایر',
            'slug' => 'other',
        ]);
    }
}
