@if (session('registerSuccess'))
<div class="alert alert-success">
    {{ __('AuthFA.ok_register') }}
</div>
@endif

@if (session('badUserPass'))
<div style="text-align:right;" class="alert alert-danger">
    {{ session('badUserPass') }}
</div>
@endif

@if(session('success'))
<div class="alert alert-success alert-dismissible fade show">
    <button type="button" class="close float-left" data-dismiss="alert">&times;</button>
    <p>
        {{ session('success') }}
    </p>
</div>
@endif

@if(session('error'))
<div class="alert alert-danger alert-dismissible fade show">
    <button type="button" class="close float-left" data-dismiss="alert">&times;</button>
    <p>
        {{ session('error') }}
    </p>
</div>
@endif

@if(session('pendingWaresError'))
<div class="alert alert-danger alert-dismissible fade show my-3">
    <button type="button" class="close float-left" data-dismiss="alert">&times;</button>
    <span>وضعیت</span>
    <span class="mx-1">{{ session('pendingWaresError') }}</span>
    <span>وسیله تعیین نشده است!</span>
</div>
@endif

@if(session('wareAssociating'))
<div class="alert alert-danger alert-dismissible fade show">
    <strong>خطا: وسایل زیر در اختیار آفیش دیگری است!</strong>
    <button type="button" class="close float-left" data-dismiss="alert">&times;</button>
    <br>
    @foreach(session('wareAssociating') as $ware)
    <span class="ml-1">{{ $ware->device_name }}</span>
    <span class="ml-1">با شماره اموال:</span>
    <span class="badge badge-info">{{ $ware->serial_number }}</span>
    <br>
    @endforeach
</div>
@endif