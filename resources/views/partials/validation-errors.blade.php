@if($errors->any())
<div class="alert alert-danger alert-dismissible fade show" style="font-size: 12px; direction: rtl;">
    <strong>خطا:</strong>
    <button type="button" class="close float-left" data-dismiss="alert">&times;</button>
    <br>
    @foreach($errors->all() as $error)
    {{ $error }}<br>
    @endforeach
</div>
@endif