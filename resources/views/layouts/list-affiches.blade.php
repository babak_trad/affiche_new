@extends('layouts.tables')

@section('table-content')

<div class="fadeIn my-5">
    <div class="row list-affiche">
        <div class="d-none d-md-block">
            <ul class="affiche-type">
                @foreach($afficheTypes as $type => $meta)
                <li>
                    <a href="{{ route('affiches.list', ['type' => $type]) }}" class="shadow-sm">
                        <span>{{ $meta['name'] }}</span>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="d-md-none">
            <div class="dropdown">
                <button class="btn btn-light dropdown-toggle" type="button" id="afficheTypesDropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ __('labels.affiches.generals.list_title') }}
                </button>
                <div class="dropdown-menu" aria-labelledby="afficheTypesDropdown">
                    @foreach($afficheTypes as $type => $meta)
                    <a class="dropdown-item" href="{{ route('affiches.list', ['type' => $type]) }}">{{ $meta['name']
                        }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@yield('table')

@endsection