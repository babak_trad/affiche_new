@extends('layouts.main-layout')

@section('main-content')
<div class="container mt-3">
    <div class="row animated fadeIn justify-content-center">
        <div class="col-12 col-md-8 card">

            @yield('header')

            <div class="card-body card-block" style="padding-top: 0;">

                @include('partials.validation-errors')

                @include('partials.alerts')

                @yield('form')

                @yield('steps')

            </div>
        </div>

        <div class="d-none">
            @yield('affiche-preview')
        </div>

    </div>
</div>

@push('special-scripts')
<script src="{{ asset('/vendor/js/jquery.md.bootstrap.datetimepicker.js') }}"></script>

<script type="text/javascript">
    $(document).on('ready', () => {
        $('#date1').MdPersianDateTimePicker({
            targetTextSelector: '#start_date',
            englishNumber: true,
            textFormat: 'yyyy-MM-dd HH:mm:ss',
            isGregorian: false,
            enableTimePicker: true,
            disableBeforeToday: true,
        });

        $('#date2').MdPersianDateTimePicker({
            targetTextSelector: '#end_date',
            // targetDateSelector: '#inputDate1-1',
            englishNumber: true,
            // dateFormat: 'yyyy-MM-dd HH:mm:ss',
            textFormat: 'yyyy-MM-dd HH:mm:ss',
            isGregorian: false,
            enableTimePicker: true,
            disableBeforeToday: true,
        });
    })

    function selectResults() {
        this.select2 = $(this.$refs.select).select2({ allowClear: true })

        this.select2.on('select2:opening', async () => {
            if (this.requested) return;

            this.loader = true

            const options = {
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content'),
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(this.body)
            }

            const success = (data) => {
                this.values = data
                this.select2.select2('close')
                setTimeout(() => {
                    this.select2.select2({ allowClear: true }).select2('open')
                }, 200);
                this.loader = false
                this.requested = true
                return
            }

            const error = (errors) => {
                let msg = ``

                errors.attached_type.forEach((err) => {
                    msg += `<li>${err}</li>`
                })

                showToast(`<ul>${msg}</ul>`, 'error')

                return
            }

            try {
                const res = await fetch(this.url, options)

                const data = await res.json()

                if (!res.ok) {
                    this.loader = false

                    if (data.errors) return error(data.errors)

                    return
                }

                return success(data)

            } catch (error) {
                this.loader = false
            }
        })
    }

</script>

@stack('form-scripts')

@endpush
@endsection