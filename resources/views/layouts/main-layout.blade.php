<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>سامانه آفیش شبکه دنا</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="{{ asset('/vendor/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/themify-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/flag-icon.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/fa-all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/cs-skin-elastic.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/buttons.bootstrap4.css') }}" />
    <link rel="stylesheet" href="{{
                asset(
                    '/vendor/css/jquery.md.bootstrap.datetimepicker.style.css'
                )
            }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/sweetalert2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/css/sufee-main.css') }}" />

    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}" />

    <script src="{{ asset('/vendor/js/jquery.min.js') }}"></script>
</head>

<body dir="rtl" style="margin: 0; padding: 0;">
    <!-- Right Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./">
                    <img src="{{ asset('/images/Yasuj_DENA.svg') }}" class="logo" alt="سامانه آفیش دنا" />
                    <span class="mx-2">سامانه آفیش دنا</span>
                </a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{ route('dashboard') }}">
                            <i class="menu-icon fas fa-tachometer-alt"></i>
                            <span class="float-right">داشبورد</span>
                        </a>
                    </li>
                    <h3 class="menu-title">دسترسی ها</h3>
                    <!-- /.menu-title -->
                    @admin
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="menu-icon fa fa-users"></i>کاربران</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li>
                                <i class="fas fa-user-friends"></i><a href="{{ route('admin.users.list') }}">لیست
                                    کاربران</a>
                            </li>
                            <li>
                                <i class="fas fa-id-card"></i><a href="{{ route('admin.roles.list') }}">نقش ها</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.units.list') }}" class="">
                            <i class="menu-icon fas fa-landmark"></i>
                            واحدها
                        </a>
                    </li>
                    @endadmin
                    <li>
                        <a href="{{
                                    route('affiches.list', ['portable'])
                                }}" class="">
                            <i class="menu-icon fas fa-file-signature"></i>
                            آفیش
                        </a>
                    </li>

                    @can('wares.create')
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="menu-icon fa fa-users"></i>انبار</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li>
                                <i class="menu-icon fas fa-warehouse"></i>
                                <a href="{{ route('wares.list', ['type' => 'uncountable']) }}">اموال</a>
                            </li>
                            <li>
                                <i class="menu-icon fas fa-project-diagram"></i>
                                <a href="{{ route('categories.list') }}">دسته بندی اموال</a>
                            </li>
                        </ul>
                    </li>
                    @endcan @can('create-car')
                    <li>
                        <a href="{{ route('cars.list') }}" class="">
                            <i class="menu-icon fas fa-car"></i>
                            خودروها
                        </a>
                    </li>
                    @endcan

                    <h3 class="menu-title">سایر</h3>
                    <!-- /.menu-title -->

                    <li>
                        <a href="{{ route('messages.list') }}" class="">
                            <i class="menu-icon fas fa-ticket-alt"></i>
                            تیکت ها
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('links.list') }}" class="">
                            <i class="menu-icon fas fa-link"></i>
                            پیوندها
                        </a>
                    </li>

                    <li>
                        <a href="#" class="">
                            <i class="menu-icon fas fa-cogs"></i>
                            تنظیمات
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    </aside>

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="header-menu">
                <div class="col-sm-7">
                    <div class="header-left">
                        <!-- Notifications -->
                        <div class="dropdown for-notification" x-data="notifications()" x-init="getNotifications"
                            id="notif-dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="notification-bell-btn"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell"></i>
                                <span class="count bg-danger" x-text="count"></span>
                            </button>
                            <div class="dropdown-menu shadow-sm fnt-shabnam-12" x-ref="notifDropdownMenu">
                                <p class="" x-show="count < 1">
                                    <i class="far fa-bell"></i>
                                    <span class="mx-1 text-canter">
                                        @lang('labels.generals.no-notifs')
                                    </span>
                                </p>
                                <template x-show="count > 0" x-for="notification in notifications"
                                    :key="notification.id">
                                    <div class="dropdown-item" x-html="notification.text"></div>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <a id="menuToggle" class="menutoggle float-right"><i class="fas fa-bars"></i></a>

                    <div class="user-area dropdown float-right">
                        <span id="userId" hidden>{{ $userId }}</span>
                        <span id="unitSlug" hidden>{{ $unitSlug }}</span>
                        <div class="d-flex flex-row">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" style="margin-top: 10px;">
                                <img class="user-avatar rounded-circle" src="{{ asset('/images/me1.png') }}"
                                    alt="User Avatar" />
                            </a>
                            <div class="mx-2 p-2">
                                <p class="my-0"
                                    style="font-size: 10px; font-family: Shabnam, Arial, Helvetica, sans-serif; margin-right: 12px;">
                                    {{ $username }}
                                </p>
                                <p class="my-0 badge badge-info"
                                    style="font-size: 10px; font-family: Shabnam, Arial, Helvetica, sans-serif;">
                                    <span>{{ $role }}</span>
                                </p>
                            </div>
                            <div class="user-menu dropdown-menu text-right border shadow-sm">
                                <a class="nav-link" href="#">
                                    <i class="fa fa-user"></i>
                                    <span class="mx-2">حساب کاربری</span>
                                </a>
                                <a class="nav-link" href="#"
                                    onclick="event.preventDefault(); document.getElementById('logoutForm').submit();">
                                    <i class="fa fa-power-off"></i>
                                    <span class="mx-2">خروج</span>
                                </a>
                                <form class="d-none" id="logoutForm"
                                    action="{{ auth()->guard('web_admin')->check() ? route('admin.logout') : route('logout') }}"
                                    method="POST" enctype="multipart/form-data">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- /header -->
        <!-- Header-->

        <div class="content mt-3">
            @yield('main-content')
        </div>

        <div hidden="hidden">
            <audio id="notificationSound" muted>
                <source src="{{ asset('/sounds/notification-sound-1.mp3') }}" type="audio/mpeg" />
            </audio>
        </div>
        <!-- .content -->
    </div>
    <!-- /#right-panel -->

    @yield('include-blades')
    <script src="{{ asset('/vendor/js/popper.min.js') }}"></script>
    <script src="{{ asset('/vendor/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/vendor/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/vendor/js/select2.min.js') }}"></script>
    <script defer src="{{ asset('/vendor/js/alpine.min.js') }}"></script>

    @stack('special-scripts')

    <script>
        // -------------------- Notifications -------------------------------------
        const notifications = function () {
            return {
                count: 0,
                notifications: [],
                fetchOptions: {
                    method: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr(
                            "content"
                        ),
                        "Content-Type": "application/json",
                        Accept: "application/json"
                    },
                    body: JSON.stringify({
                        user: parseInt($("#userId").text())
                    })
                },
                appendBroadcastedNotification(notification) {
                    this.notifications.push({
                        id: randomId(10),
                        text: notification
                    });

                    this.count++;
                },
                clear() {
                    $(this.$el).on("hidden.bs.dropdown", e => {
                        if (this.count > 0) {
                            let result = clearNotifications(
                                this.fetchOptions
                            );

                            if (result) {
                                this.notifications = [];
                                this.count = 0;
                            }
                        }
                    });
                }
            };
        };

        const clearNotifications = async function (options) {
            try {
                const res = await fetch("/notifications/clear", options);

                const data = await res.json();

                if (data.status) {
                    return true;
                }
            } catch (err) {
                console.log(err);
            }
        };

        const getNotifications = async function () {
            this.clear();

            this.$el.addEventListener('broadcastedNotif', (e) => {
                this.appendBroadcastedNotification(e.detail)
            })

            this.$refs.notifDropdownMenu.addEventListener('click', e => e.stopPropagation())

            const success = data => {
                this.notifications = data.map((notification, index) => {
                    return {
                        id: notification.id,
                        text: notification.data.html
                    };
                });

                this.count = this.notifications.length;
                return;
            };

            try {
                const res = await fetch(
                    "/notifications/list",
                    this.fetchOptions
                );

                const data = await res.json();

                if (!res.ok) {
                    console.log("Failed to get notifications!");
                }

                return success(data);
            } catch (err) {
                console.log(err);
            }
        };

        const randomId = length => {
            var result = "";
            var characters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#!$%^";
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(
                    Math.floor(Math.random() * charactersLength)
                );
            }
            return result;
        };
    </script>

    <script src="{{ asset('/js/main-script.js') }}"></script>
</body>

</html>