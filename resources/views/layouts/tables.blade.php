@extends('layouts.main-layout')

@section('include-blades')
@include('cp.affiche.contributor-list')
@endsection

@section('main-content')

@yield('table-content')

@push('special-scripts')
<script src="{{ asset('/vendor/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendor/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/js/datatables-init.js') }}"></script>

@stack('other-scripts')

@endpush


@endsection