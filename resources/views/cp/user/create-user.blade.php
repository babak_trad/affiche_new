@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.create_user') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('admin.users.store') }}" class="multi-step">
    @csrf

    <div class="form-tab">
        <div class="form-group">
            <label>{{ __('AuthFA.name') }}</label>
            <input name="name" type="text" class="form-control">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.family_name') }}</label>
            <input name="family_name" type="text" class="form-control">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.employee_num') }}</label>
            <input name="employee_num" type="text" class="form-control">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.phone_num') }}</label>
            <input name="phone_num" type="text" class="form-control">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.password') }}</label>
            <input name="password" type="password" class="form-control">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.confirm_password') }}</label>
            <input name="password_confirmation" type="password" class="form-control">
        </div>
    </div>

    <div class="form-tab" x-data="unitAndRoles()" x-init="initSelectTags">
        <div class="form-group">
            <label>{{ __('AuthFA.unit_assign') }}</label>
            <select name="unit_id" class="form-control select2-tag" x-ref="unitSelect"
                data-placeholder="{{ __('AuthFA.unit_modal_reg_message') }}">
                @foreach($units as $unit)
                <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group with-loader">
            <label>{{ __('AuthFA.role_assign') }}</label>
            <select name="role_id[]" data-placeholder="{{ __('AuthFA.role_modal_reg_message') }}" multiple="multiple"
                class="form-control select2-tag" tabindex="5" x-ref="roleSelect" x-init="fetchRoles">
                <template x-for="role in roles" :key="role.id">
                    <option :value="role.id" x-text="role.name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_user') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>
@endsection

@section('steps')
<div class="mt-2 text-center rtl">
    <span class="form-step" style="width: 15px; height: 15px;"></span>
    <span class="form-step" style="width: 15px; height: 15px;"></span>
</div>
@endsection

@push('form-scripts')
<script>
    const unitAndRoles = function () {
        return {
            unit: 1,
            roles: [],
            loader: false,
            url: '/admin/units/roles',
        }
    }

    const initSelectTags = function () {
        $(this.$refs.roleSelect).select2({ allowClear: true })

        this.unitTag = $(this.$refs.unitSelect).select2({ allowClear: true })

        this.unit = this.unitTag.val()

        this.unitTag.on('select2:select', () => {
            this.unit = this.unitTag.val()
            this.fetchRoles()
        })

        this.fetchRoles = async () => {
            this.loader = true

            const options = {
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content'),
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ unit: this.unit })
            }

            const error = (errors) => {
                let msg = ``

                errors.unit.forEach((err) => {
                    msg += `<li style="list-style-type: none;">${err}</li>`
                })

                showToast(`<ul style="padding: 0; margin-right:5px;">${msg}</ul>`, 'error')

                return
            }

            try {
                const res = await fetch(this.url, options)

                const data = await res.json()

                if (!res.ok) {
                    this.loader = false

                    if (data.errors) return error(data.errors)

                    return
                }

                this.roles = data
                this.loader = false

            } catch (error) {
                this.loader = false
            }
        }
    }
</script>
@endpush