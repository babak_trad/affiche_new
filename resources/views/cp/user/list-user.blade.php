@extends('layouts.tables')
@section('table-content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">

                    @include('partials.alerts')

                    <div class="card-header">
                        <strong class="card-title">{{ __('AuthFA.users_data') }}</strong>
                        <a href="{{ route('admin.users.create') }}" class="btn btn-success pull-left">
                            {{ __('AuthFA.create_user') }}
                        </a>
                    </div>
                    <div class="card-body" style="overflow-x: auto">
                        <div class="row">
                            <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                <div class="input-group">
                                    <input type="text" class="form-control data-table-search-text"
                                        placeholder="جستجو..." data-table="#table1">
                                </div>
                            </dvi>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <select class="form-control data-table-change-length ltr" data-table="#table1">
                                        <option>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                        <option>40</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <table id="table1" class="table table-striped table-bordered bootstrap-data-table-export">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('AuthFA.row') }}</th>
                                    <th>{{ __('AuthFA.name_and_family') }}</th>
                                    <th>{{ __('AuthFA.employee_num') }}</th>
                                    <th>{{ __('AuthFA.unit') }}</th>
                                    <th>{{ __('AuthFA.role') }}</th>
                                    <th>{{ __('AuthFA.operations') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $i => $user)
                                <tr class="text-center">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $user->name. " " .$user->family_name }}</td>
                                    <td>{{ $user->employee_num }}</td>
                                    <td>
                                        <span>{{ $user->unit->name }}</span>
                                        <br>
                                        <span>{{ $user->unit->slug }}</span>
                                    </td>
                                    <td>
                                        @foreach ($user->roles as $role)
                                        <span class="badge badge-info mx-1">
                                            {{$role->name}}
                                        </span>
                                        @endforeach
                                    </td>
                                    <td class="d-flex flex-row justify-content-center">
                                        <a href="{{ route('admin.users.edit', [$user->id]) }}"
                                            class="btn btn-sm btn-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <form action="{{ route('admin.users.delete', [$user->id]) }}" method="POST"
                                            class="mx-1">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-danger btn-sm deleteBtn">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection