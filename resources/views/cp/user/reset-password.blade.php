@extends('cp.layout.master')
    @section('content')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="card">
                {{-- <div style="text-align:right;" class="card-header"> {{ __('AuthFA.create_user') }} </div> --}}
                <strong style="text-align:right;" class="card-header">{{ __('AuthFA.change_password') }}</strong>
                    <div class="card-body card-block">

                        
                        <form method="POST" action="{{ route('user.updatePassword', $user->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                
                                <div class="form-group">
                                    <label style="float: right;">{{ __('AuthFA.password') }}</label>
                                    <input name="password" type="password" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label style="float: right;">{{ __('AuthFA.confirm_password') }}</label>
                                    <input name="password_confirmation" type="password" class="form-control">
                                </div>

                                <div class="pull-right offset-sm-3 mt-sm-3">
                                    @include('partials.validation-errors')
                                </div>
                                <div class="form-actions form-group"><button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('AuthFA.submit_changes') }}</button></div>
                                <div class="form-actions form-group"><a href="{{ url('/auth/users') }}" class="btn btn-danger btn-lg btn-block">{{ __('AuthFA.cancel') }}</a></div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
