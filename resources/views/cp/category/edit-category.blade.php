@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.categories.edit.title') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('categories.update', [$category->id]) }}" class="multi-step">
    @csrf
    @method('PUT')

    <div class="form-tab">
        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.categories.properties.name') }}</label>
            <input name="name" style="text-align:right;" type="text" class="form-control" value="{{ $category->name }}">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.categories.properties.slug') }}</label>
            <input name="slug" style="text-align:right;" type="text" class="form-control" value="{{ $category->slug }}">
        </div>
    </div>
    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.categories.edit.edit_btn') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>
@endsection