@extends('layouts.tables')
@section('table-content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">

                    @include('partials.alerts')

                    <div class="card-header">
                        <strong class="card-title pull-right">{{ __('AuthFA.categories.list.title') }}</strong>
                        <a href="{{ route('categories.create') }}" class="btn btn-success pull-left">{{
                            __('AuthFA.categories.create.create_btn') }}</a>
                    </div>
                    <div class="card-body" style="overflow-x: auto">
                        <div class="row">
                            <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                <div class="input-group">
                                    <input type="text" class="form-control data-table-search-text"
                                        placeholder="جستجو..." data-table="#table1">
                                </div>
                            </dvi>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <select class="form-control data-table-change-length ltr" data-table="#table1">
                                        <option selected>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                        <option>40</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <table id="table1" class="table table-striped table-bordered bootstrap-data-table-export">
                            <thead>
                                <tr>
                                    <th>{{ __('AuthFA.row') }}</th>
                                    <th>{{ __('AuthFA.categories.properties.name') }}</th>
                                    <th>{{ __('AuthFA.categories.properties.slug') }}</th>
                                    <th>{{ __('AuthFA.operations') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $i => $category)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td class="d-flex flex-row">
                                        <a href="{{ route('categories.edit', [$category->id]) }}"
                                            class="btn btn-sm rounded-circle btn-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <form action="{{ route('categories.delete', [$category->id]) }}" method="POST"
                                            class="mx-1">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit"
                                                class="btn btn-danger btn-sm rounded-circle deleteBtn">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection