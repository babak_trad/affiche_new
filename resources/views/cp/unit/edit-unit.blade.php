@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.unit_edit') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('admin.units.update', [$unit->id]) }}" class="multi-step">
    @method('PUT')
    @csrf

    <div class="form-tab">
        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.unit_name') }}</label>
            <input name="name" style="text-align:right;" type="text" class="form-control" value="{{ $unit->name }}">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.department_name') }}</label>
            <input name="department" style="text-align:right;" type="text" class="form-control"
                value="{{ $unit->department }}">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.unit_slug') }}</label>
            <input name="slug" style="text-align:right;" type="text" class="form-control" value="{{ $unit->slug }}">
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.unit_edit') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>
@endsection