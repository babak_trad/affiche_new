@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.create_unit') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('admin.units.store') }}" class="multi-step">
    @csrf

    <div class="form-tab">
        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.unit_name') }}</label>
            <input name="name" style="text-align:right;" type="text" class="form-control" data-tag="input">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.department_name') }}</label>
            <input name="department" style="text-align:right;" type="text" class="form-control" data-tag="input">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.unit_slug') }}</label>
            <input name="slug" style="text-align:right;" type="text" class="form-control required" data-tag="input">
        </div>
    </div>
    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_unit') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>
@endsection