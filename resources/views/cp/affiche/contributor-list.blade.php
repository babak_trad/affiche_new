<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex flex-row justify-content-between">
                <h5 class="modal-title" id="mediumModalLabel">کاربران</h5>
                <p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </p>
            </div>
            <div class="modal-body text-center">
                <img src="{{ asset('/GIFs/Spinner-1s-80px.gif') }}" alt="Loading...">
                <div class="alert alert-warning">رکوردی یافت نشد!</div>
                <div id="contributorsData"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">
                    {{ __('AuthFA.confirm') }}
                </button>
            </div>
        </div>
    </div>
</div>