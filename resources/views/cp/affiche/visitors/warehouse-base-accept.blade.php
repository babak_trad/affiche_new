@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">فرم تأیید آفیش</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('affiches.accept', [$affiche->id]) }}" class="multi-step">
    @csrf

    <input type="hidden" name="affiche_type" value="{{ $affiche->present()->type }}">

    <div class="form-tab">
        @if($affiche->flow === 0)
        <div class="form-group">
            <label style="float: right;">تاریخ واگذاری وسایل</label>
            <div class="input-group" dir="ltr">
                <input type="text" dir="rtl" name="date1" class="form-control required" aria-label="date1"
                    aria-describedby="date1" data-tag="input" id="start_date"
                    value="{{ $affiche->warehouse_exit_date }}">
                <div class="input-group-append">
                    <span class="input-group-text cursor-pointer fa fa-calendar-alt" id="date1"></span>
                </div>
            </div>
        </div>
        @elseif($affiche->flow === 1)
        <div class="form-group">
            <label style="float: right;">تاریخ تحویل وسایل</label>
            <div class="input-group" dir="ltr">
                <input type="text" dir="rtl" name="date2" class="form-control required" aria-label="date2"
                    aria-describedby="date1" data-tag="input" id="end_date"
                    value="{{ $affiche->warehouse_enter_date }}">
                <div class="input-group-append">
                    <span class="input-group-text cursor-pointer fa fa-calendar-alt" id="date2"></span>
                </div>
            </div>
        </div>
        @endif

        <div class="form-group">
            <label for="accepting">کامنت</label>
            <textarea id="accepting" name="accepted-comment" rows="3" class="form-control required"
                data-tag="input">{{ $action['comment'] ?? '' }}</textarea>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_affiche') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@endsection