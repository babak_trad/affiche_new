@extends('layouts.tables')
@section('table-content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">

                    @include('partials.alerts')

                    <div class="card-header">
                        <strong class="card-title pull-right">لیست تجهیزات آفیش</strong>
                        @can('wares.edit-affiche', $affiche)
                        <a href="{{ route('affiches.wares.edit', [$affiche->present()->type, $affiche->id]) }}"
                            class="btn btn-primary pull-left rounded">
                            ویرایش
                        </a>
                        @endcan
                    </div>

                    <div class="card-body" style="overflow-x: auto">
                        <div class="row">
                            <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                <div class="input-group">
                                    <input type="text" class="form-control data-table-search-text"
                                        placeholder="جستجو..." data-table="#table1">
                                </div>
                            </dvi>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <select class="form-control data-table-change-length ltr" data-table="#table1">
                                        <option selected>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                        <option>40</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <table id="table1" class="table table-striped table-bordered bootstrap-data-table-export">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('AuthFA.row') }}</th>
                                    <th>{{ __('AuthFA.device_name') }}</th>
                                    <th>{{ __('AuthFA.device_category') }}</th>
                                    <th>{{ __('AuthFA.part_number') }}</th>
                                    <th>{{ __('AuthFA.serial_number') }}</th>
                                    <th>موجودی</th>
                                    <th>درخواستی</th>
                                    <th>وضعیت</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($afficheWares as $i => $ware)
                                <tr class="text-center">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $ware->device_name }}</td>
                                    <td>
                                        @foreach($ware->categories as $category)
                                        <span class="badge badge-info rounded">
                                            {{ $category->name }}
                                        </span>
                                        @endforeach
                                    </td>
                                    <td>{{ $ware->part_number }}</td>
                                    <td>{{ $ware->serial_number }}</td>
                                    <td>{{ $ware->stock }}</td>
                                    <td>{{ $ware->pivot->ware_quantity }}</td>
                                    <td>{{ $ware->present()->ware_status }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection