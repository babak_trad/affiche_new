<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">{{ __('AuthFA.row') }}</th>
            <th scope="col">{{ __('AuthFA.name_and_family') }}</th>
            <th scope="col">{{ __('AuthFA.role') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($contributors as $i => $contributor)
            <tr>
                <td scope="row">{{ $i+1 }}</td>
                <td>{{ $contributor->name . " " . $contributor->family_name }}</td>
                <td>@foreach($contributor->getRelations()['roles'] as $role)
                        <span class="badge badge-info">{{ $role->name }}</span>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </tbody>
</table>