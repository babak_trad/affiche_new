@extends('layouts.multi-step-form')
@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('labels.affiches.generals.update') }}</strong>
</div>
@endsection
@section('form')
<form method="POST" action="{{ route('affiches.creator.update', [$affiche->id]) }}" class="multi-step">
    @method('PUT')
    @csrf

    <input type="hidden" name="affiche_type" value="{{ $affiche->present()->type }}">

    <!-----------------------------------------  Name, Date & Time -->
    <div class="form-tab">
        <div class="form-group">
            <label>{{ __('AuthFA.affiche_name') }}</label>
            <input name="name" type="text" class="form-control required" data-tag="input" value="{{ $affiche->name }}">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.start_datetime') }}</label>
            <div class="input-group" dir="ltr">
                <input type="text" dir="rtl" name="start_date" id="start_date" class="form-control required"
                    aria-label="date1" aria-describedby="date1" data-tag="input" value="{{ $affiche->start_date }}">
                <div class="input-group-append">
                    <span class="input-group-text cursor-pointer fa fa-calendar" id="date1"></span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.end_datetime') }}</label>
            <div class="input-group" dir="ltr">
                <input type="text" dir="rtl" name="end_date" id="end_date" class="form-control required"
                    aria-label="date2" aria-describedby="date2" data-tag="input" value="{{ $affiche->end_date }}">
                <div class="input-group-append">
                    <span class="input-group-text cursor-pointer fa fa-calendar" id="date2"></span>
                </div>
            </div>
        </div>
    </div>

    <!------------------------------------------------------ Contributors -->
    <div class="form-tab">
        <div class="form-group with-loader" x-data="role('tv-producer')">
            <label>{{ __('AuthFA.producer') }}</label>
            <select x-ref="select" x-init="selectResults" name="producer"
                data-placeholder="{{ __('AuthFA.select_producer') }}" id="producerName" class="form-control required"
                tabindex="5" data-tag="select2">
                <option></option>
                @foreach($afficheContributors['tv-producer'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="role('tv-producer-assistant')">
            <label>{{ __('AuthFA.producer_assistant') }}</label>
            <select x-ref="select" x-init="selectResults" name="producer_assistant[]"
                data-placeholder="{{ __('AuthFA.select_producer_assistant') }}" multiple class="form-control"
                tabindex="5" data-tag="select2">
                <option></option>
                @foreach($afficheContributors['tv-producer-assistant'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="role('director')">
            <label>{{ __('AuthFA.director') }}</label>
            <select x-ref="select" x-init="selectResults" name="director"
                data-placeholder="{{ __('AuthFA.select_director') }}" class="form-control required" tabindex="5"
                data-tag="select2">
                <option></option>
                @foreach($afficheContributors['director'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="role('director-assistant')">
            <label>{{ __('AuthFA.director_assistant') }}</label>
            <select x-ref="select" x-init="selectResults" name="director_assistant[]"
                data-placeholder="{{ __('AuthFA.select_director_assistant') }}" multiple class="form-control"
                tabindex="5" data-tag="select2">
                <option></option>
                @foreach($afficheContributors['director-assistant'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="role('sound-recordist')">
            <label>{{ __('AuthFA.sound_recordist') }}</label>
            <select x-ref="select" x-init="selectResults" name="sound_recordist[]"
                data-placeholder="{{ __('AuthFA.select_sound_recordist') }}" multiple class="form-control required"
                tabindex="5" data-tag="select2">
                <option></option>
                @foreach($afficheContributors['sound-recordist'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="role('sound-recordist-assistant')">
            <label>{{ __('AuthFA.sound_recordist_assistant') }}</label>
            <select x-ref="select" x-init="selectResults" name="sound_recordist_assistant[]"
                data-placeholder="{{ __('AuthFA.select_sound_recordist_assistant') }}" multiple class="form-control"
                tabindex="5" data-tag="select2">
                <option></option>
                @foreach($afficheContributors['sound-recordist-assistant'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="role('cameraman')">
            <label>{{ __('AuthFA.cameraman') }}</label>
            <select x-ref="select" x-init="selectResults" name="cameraman[]"
                data-placeholder="{{ __('AuthFA.select_cameraman') }}" multiple class="form-control required"
                tabindex="5" data-tag="select2">
                <option></option>
                @foreach($afficheContributors['cameraman'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="role('tech-assistant')">
            <label>{{ __('AuthFA.tech_assistant') }}</label>
            <select x-ref="select" x-init="selectResults" name="tech_assistant[]"
                data-placeholder="{{ __('AuthFA.select_tech_assistant') }}" multiple class="form-control" tabindex="5"
                data-tag="select2">
                <option></option>
                @foreach($afficheContributors['tech-assistant'] ?? [] as $user)
                <option value="{{ $user->id ?? null }}" selected>
                    {{ $user ? ($user->name . ' ' . $user->family_name) : '' }}
                </option>
                @endforeach
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>
    </div>

    <!-------------------------------------------------------- Wares -->
    <div class="form-tab">
        <div class="col-12">
            <div class="default-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-uncountable-tab" data-toggle="tab"
                            href="#nav-uncountable" role="tab" aria-controls="nav-uncountable" aria-selected="true">
                            <span>غیرمصرفی</span>
                        </a>
                        <a class="nav-item nav-link" id="nav-countable-tab" data-toggle="tab" href="#nav-countable"
                            role="tab" aria-controls="nav-countable" aria-selected="false">
                            <span>مصرفی</span>
                        </a>
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-uncountable" role="tabpanel"
                        aria-labelledby="nav-uncountable-tab">
                        <div class="card-body px-3" style="overflow-x: auto">
                            <div class="row">
                                <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control data-table-search-text"
                                            placeholder="جستجو..." data-table="#uncountable-table">
                                    </div>
                                </dvi>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control data-table-change-length ltr"
                                            data-table="#uncountable-table">
                                            <option selected>10</option>
                                            <option>20</option>
                                            <option>30</option>
                                            <option>40</option>
                                            <option>50</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <table id="uncountable-table"
                                class="table table-striped table-bordered bootstrap-data-table-export">
                                <thead>
                                    <tr>
                                        <th>{{ __('AuthFA.row') }}</th>
                                        <th>نام تجهیز</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($wares['uncountable'] as $i => $ware)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>
                                            <span>{{ $ware->device_name }}</span>
                                            <input type="checkbox" name="uncountable_wares[]" class="form-check-input"
                                                value="{{ $ware->id }}" {{ isset($afficheWares['uncountable']) &&
                                                $afficheWares['uncountable']->contains($ware->id) ? 'checked' : '' }}>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-countable" role="tabpanel" aria-labelledby="nav-countable-tab">
                        <div class="card-body px-3" style="overflow-x: auto">
                            <div class="row">
                                <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control data-table-search-text"
                                            placeholder="جستجو..." data-table="#countable-table">
                                    </div>
                                </dvi>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control data-table-change-length ltr"
                                            data-table="#countable-table">
                                            <option selected>10</option>
                                            <option>20</option>
                                            <option>30</option>
                                            <option>40</option>
                                            <option>50</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <table id="countable-table"
                                class="table table-striped table-bordered bootstrap-data-table-export">
                                <thead>
                                    <tr>
                                        <th>{{ __('AuthFA.row') }}</th>
                                        <th>نام تجهیز</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($wares['countable'] as $ware)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>
                                            <span>{{ $ware->device_name }}</span>
                                            <input type="checkbox" name="countable_wares[{{ $ware->id }}]"
                                                class="form-check-input"
                                                value="{{ isset($afficheWares['countable']) ? $afficheWares['countable']->find($ware->id)->pivot->ware_quantity : 1 }}"
                                                {{ (isset($afficheWares['countable']) &&
                                                $afficheWares['countable']->contains($ware->id)) ? 'checked' : '' }}>
                                            <div class="my-3">
                                                <span>تعداد:</span>
                                                <span class="mx-1 float-left slider-changer slider-minus">
                                                    <i class="fas fa-minus-circle"></i>
                                                </span>
                                                <input type="number" min="1"
                                                    class="p-1 mx-1 float-left text-center rounded ware-count"
                                                    value="{{ isset($afficheWares['countable']) ? $afficheWares['countable']->find($ware->id)->pivot->ware_quantity : 1 }}">
                                                <span class="mx-1 float-left slider-changer slider-plus">
                                                    <i class="fas fa-plus-circle"></i>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_affiche') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@endsection

@section('steps')
<div class="mt-2 text-center rtl" style="padding-top: 10px;">
    <span class="form-step" style="width: 15px; height: 15px;"></span>
    <span class="form-step" style="width: 15px; height: 15px;"></span>
    <span class="form-step" style="width: 15px; height: 15px;"></span>
</div>
@endsection

@section('affiche-preview')
<div class="col-12 col-md-8 card">
    <div class="card-header">
        <strong style="text-align:right;" class="card-header">{{ __('labels.affiches.generals.preview')
            }}</strong>
    </div>

    <div class="card-body card-block" style="padding-top: 0;">
        <!-- make preview -->
    </div>
</div>
@endsection

@push('form-scripts')
<script src="{{ asset('/vendor/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendor/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/js/datatables-init.js') }}"></script>
<script>
    $('.slider-plus').on('click', function () {
        let newValue = $(this).siblings('.ware-count').val((ind, val) => {
            return ++val;
        });

        changeQuantity($(this), parseInt(newValue.val()));
    });

    $('.slider-minus').on('click', function () {
        let newValue = $(this).siblings('.ware-count').val((ind, val) => {
            if (parseInt(val) <= 1) {
                return 1;
            }

            return --val;
        });

        changeQuantity($(this), parseInt(newValue.val()));
    });

    $('.ware-count').on('change', function () {
        let newVal = parseInt($(this).val());

        if (newVal < 1) {
            $(this).val(1);
        }

        changeQuantity($(this), newVal);
    })

    const changeQuantity = (thisBtn, newVal) => {
        if (newVal < 1) {
            newVal = 1;
        }

        let check = thisBtn.parent().siblings('input[type="checkbox"]').eq(0);

        check.val(newVal);
    }

    function role(role = '') {
        if (!role) return

        return {
            values: [],
            body: { role: role },
            url: '/users/role',
            loader: false,
            requested: false
        }
    }
</script>
@endpush