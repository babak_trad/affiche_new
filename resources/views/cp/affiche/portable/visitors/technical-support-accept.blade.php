@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">فرم تأیید آفیش</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('affiches.accept', [$affiche->id]) }}" class="multi-step">
    @csrf

    <input type="hidden" name="affiche_type" value="{{ $affiche->present()->type }}">

    <div class="form-tab">
        <div class="form-group">
            <label for="accepting">کامنت</label>
            <textarea id="accepting" name="accepted-comment" rows="3" class="form-control required"
                data-tag="input">{{ $action['comment'] ?? '' }}</textarea>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_affiche') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

<script>

</script>

@endsection