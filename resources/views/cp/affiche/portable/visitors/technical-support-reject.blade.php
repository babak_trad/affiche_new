@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">فرم رد آفیش</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('affiches.reject', [$affiche->id]) }}" class="multi-step">
    @csrf

    <input type="hidden" name="affiche_type" value="{{ $affiche->present()->type }}">

    <div class="form-tab">
        <div class="form-group">
            <label for="rejecting">
                <span>کامنت</span>
                <i class="far fa-hand-point-down" style="vertical-align: -5px;"></i>
            </label>
            @if(isset($action) && $action->action !== 1)
            <textarea id="rejecting" name="rejected-comment" rows="3" class="form-control required" data-tag="input">
                {{ $action['comment'] ?? '' }}
            </textarea>
            @else
            <textarea id="rejecting" name="rejected-comment" rows="3" class="form-control required"
                data-tag="input"></textarea>
            @endif
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_affiche') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@endsection