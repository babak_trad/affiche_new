@extends('layouts.list-affiches')

@section('table')
<div class="animated fadeIn">

    <div class="row">
        <div class="col-md-12">
            <div class="card">

                @include('partials.alerts')

                <div class="card-header">
                    <strong class="card-title pull-right">{{ __('labels.affiches.generals.list_title') }}</strong>
                </div>

                <div class="card-body" style="overflow-x: auto">
                    <div class="row">
                        <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                            <div class="input-group">
                                <input type="text" class="form-control data-table-search-text" placeholder="جستجو..."
                                    data-table="#table1">
                            </div>
                        </dvi>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <select class="form-control data-table-change-length ltr" data-table="#table1">
                                    <option selected>10</option>
                                    <option>20</option>
                                    <option>30</option>
                                    <option>40</option>
                                    <option>50</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <table id="table1" class="table table-striped table-bordered bootstrap-data-table-export">
                        <thead>
                            <tr>
                                <th>{{ __("labels.generals.row") }}</th>
                                <th>{{ __("labels.affiches.generals.name") }}</th>
                                <th>{{ __("labels.affiches.car.issuer") }}</th>
                                <th>{{ __("labels.affiches.car.execution_period") }}</th>
                                <th>{{ __("labels.affiches.car.execution_date") }}</th>
                                <th>{{ __("labels.affiches.generals.current_state") }}</th>
                                <th>{{ __("labels.generals.status") }}</th>
                                <th>{{ __("labels.generals.operations") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($affiches as $i => $affiche)
                            @if($affiche->status !== 3)
                            @canany(['affiches.view', 'affiches.visit'], $affiche)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><a href="#" class="affiche-name-link">{{ $affiche->name }}</a></td>
                                <td>{{ $affiche->user->name. ' ' . $affiche->user->family_name }}</td>
                                <td>{{ $affiche->present()->start_date }} تا {{ $affiche->present()->end_date }}</td>
                                <td>{{ $affiche->present()->execution_date }}</td>
                                <td class="text-center">
                                    <span class="badge badge-info">
                                        {{ $affiche->present()->current_state }}
                                    </span>
                                </td>
                                <td class="text-center">
                                    {{ $affiche->present()->status }}
                                </td>
                                <td class="d-flex flex-row border-0">
                                    @can('affiches.archive', $affiche)
                                    <form action="{{ route('affiches.creator.archive', [$affiche->id]) }}" method="POST"
                                        class="mx-1">
                                        @csrf
                                        <input type="hidden" name="affiche_type" value="car">
                                        <button type="submit" class="btn btn-dark btn-sm">
                                            <i class="fas fa-file-archive"></i>
                                        </button>
                                    </form>
                                    @endcan
                                    @cannot('affiches.create', 'car')
                                    @can('affiches.action', $affiche)
                                    @if($affiche->flow === 0)
                                    <form action="{{ route('affiches.action.form', [$affiche->id, 'accept']) }}"
                                        method="POST" class="mx-1">
                                        @csrf
                                        <input type="hidden" name="affiche_type" value="car">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            <i class="fas fa-check-circle"></i>
                                        </button>
                                    </form>
                                    @elseif($affiche->flow === 1)
                                    <form action="{{ route('affiches.action.form', [$affiche->id, 'accept']) }}"
                                        method="POST" class="mx-1">
                                        @csrf
                                        <input type="hidden" name="affiche_type" value="car">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            <i class="fas fa-check-circle"></i>
                                        </button>
                                    </form>
                                    @endif
                                    @if(Auth::user()->can('affiches.reject', $affiche) && $affiche->flow === 0)
                                    <form action="{{ route('affiches.action.form', [$affiche->id, 'reject']) }}"
                                        method="POST" class="mx-1">
                                        @csrf
                                        <input type="hidden" name="affiche_type" value="car">
                                        <button type="submit" class="btn btn-sm btn-danger">
                                            <i class="fas fa-ban"></i>
                                        </button>
                                    </form>
                                    @endif
                                    @endcan
                                    @if(!Auth::user()->can('affiches.reject', $affiche) && $affiche->status === 1)
                                    <form action="{{ route('affiches.list', [$affiche->present()->type]) }}"
                                        method="GET" class="mx-1">
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-secondary">
                                            <i class="fas fa-comment-medical"></i>
                                        </button>
                                    </form>
                                    @endif
                                    @endcannot
                                </td>
                            </tr>
                            @endcanany
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @can('affiches.create')
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    <strong class="card-title pull-right">
                        {{ __('labels.affiches.generals.draft_title') }}
                    </strong>

                    <div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" style="float: left;" type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ __('labels.affiches.generals.create') }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" style="text-align: right;"
                                href="{{ route('affiches.creator.create', ['type' => 'portable']) }}">
                                {{ __('labels.affiches.portable.type') }}
                            </a>
                            <a class="dropdown-item" style="text-align: right;"
                                href="{{ route('affiches.creator.create', ['type' => 'car']) }}">
                                {{ __('labels.affiches.car.type') }}
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-body" style="overflow-x: auto">
                    <div class="row">
                        <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                            <div class="input-group">
                                <input type="text" class="form-control data-table-search-text" placeholder="جستجو..."
                                    data-table="#table2">
                            </div>
                        </dvi>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <select class="form-control data-table-change-length ltr" data-table="#table2">
                                    <option selected>10</option>
                                    <option>20</option>
                                    <option>30</option>
                                    <option>40</option>
                                    <option>50</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <table id="table2" class="table table-striped table-bordered bootstrap-data-table-export">
                        <thead>
                            <tr>
                                <th>{{ __('labels.generals.row') }}</th>
                                <th>{{ __('labels.affiches.generals.name') }}</th>
                                <th>{{ __('labels.affiches.car.execution_period') }}</th>
                                <th>{{ __('labels.affiches.car.execution_date') }}</th>
                                <th>{{ __('labels.generals.operations') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($affiches as $i => $affiche)
                            @if(Auth::user()->can('affiches.view', $affiche) && $affiche->status === 3)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><a href="#" class="affiche-name-link">{{ $affiche->name }}</a></td>
                                <td>{{ $affiche->present()->start_date }} تا {{ $affiche->present()->end_date }}</td>
                                <td>{{ $affiche->present()->execution_date }}</td>
                                <td class="d-flex flex-row">
                                    <a href="{{ route('affiches.creator.edit', ['car', $affiche->id]) }}"
                                        class="btn btn-sm btn-success mx-1">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <form action="{{ route('affiches.creator.delete', [$affiche->id]) }}" method="POST"
                                        class="mx-1">
                                        {{ method_field('DELETE') }}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="affiche_type" value="car">
                                        <button type="submit" class="btn btn-danger btn-sm deleteBtn">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </form>
                                    <form action="{{ route('affiches.creator.send', [$affiche->id]) }}" method="POST"
                                        class="mx-1">
                                        @csrf
                                        <input type="hidden" name="affiche_type" value="car">
                                        <button type="submit" class="btn btn-secondary btn-sm">
                                            <i class="fas fa-file-upload"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endcan

</div><!-- .animated -->

@endsection