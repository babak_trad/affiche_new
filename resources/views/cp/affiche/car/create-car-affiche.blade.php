@extends('layouts.multi-step-form')
@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.create_affiche') }}</strong>
</div>
@endsection
@section('form')
<form method="POST" action="{{ route('affiches.creator.store') }}" class="multi-step" id="createAffiche">
    @csrf

    <input type="hidden" name="affiche_type" value="car">

    <!-- date & time step -->
    <div class="form-tab">
        <div class="form-group">
            <label>{{ __('labels.affiches.generals.name') }}</label>
            <input name="name" type="text" class="form-control" data-tag="input" value="{{ old('car_affiche_name') }}">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('labels.affiches.car.start_time') }}</label>
            <input type="time" name="start_time" min="00:00" max="24:00" class="form-control" data-tag="input"
                value="{{ old('car_affiche_start') }}">
        </div>

        <div class="form-group">
            <label>{{ __('labels.affiches.car.end_time') }}</label>
            <input type="time" name="end_time" min="00:00" max="24:00" class="form-control" data-tag="input"
                value="{{ old('car_affiche_end') }}">
        </div>

        <div class="form-group">
            <label>{{ __('labels.affiches.car.execution_date') }}</label>
            <div class="input-group" dir="ltr">
                <input type="text" dir="rtl" name="execution_date" id="execution_date" class="form-control"
                    aria-label="date3" aria-describedby="date3" data-tag="input"
                    value="{{ old('car_affiche_execution') }}">
                <div class="input-group-append">
                    <span class="input-group-text cursor-pointer fa fa-calendar" id="date3"></span>
                </div>
            </div>
        </div>
    </div>

    <!-- contributors step -->
    <div class="form-tab">
        <div class="form-group" x-data="contributors()">
            <label>{{ __('labels.affiches.car.carrier') }}</label>
            <select x-ref="select" x-init="selectResults" name="carrier" class="form-control"
                data-placeholder="{{ __('labels.affiches.car.carrier') }}" tabindex="5" data-tag="select2">
                <option></option>
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <hr>

        <div class="form-group" x-data="contributors()">
            <label>{{ __('labels.affiches.car.contributors') }}</label>
            <select x-ref="select" x-init="selectResults" name="contributors[]" class="form-control"
                data-placeholder="{{ __('labels.affiches.car.contributors') }}" tabindex="5" data-tag="select2"
                multiple>
                <option></option>
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>
    </div>

    <!-- ware adding step -->
    <div class="form-tab">
        <div class="form-group" x-data="attachAffiche()">
            <label>{{ __('labels.affiches.car.attached_type') }}</label>
            <br>
            @foreach($types as $type => $meta)
            <label class="form-check-label shadow-sm rounded">
                <input class="check-input" type="radio" name="attached_type" value="{{ $type }}"
                    x-data="{ thisType: $el.getAttribute('value')}"
                    x-init="$watch('attachedType', () => requested = false)"
                    @click="body = {attached_type: thisType}; attachedType = thisType;"
                    checked="{{ $type == 'portable' ? 'checked' : '' }}">
                <span>{{ $meta['name'] }}</span>
            </label>
            @endforeach

            <br>

            <label>{{ __('labels.affiches.car.attached_affiche') }}</label>
            <select x-ref="select" x-init="selectResults" name="attached_affiche" class="form-control"
                data-placeholder="{{ __('labels.affiches.car.attached_placeholder') }}" tabindex="5" data-tag="select2">
                <option></option>
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name"></option>
                </template>
            </select>

            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="divider text-center">
            <span class="divider-label">
                {{ __('labels.affiches.car.adding_ware_placeholder') }}
            </span>
        </div>

        <div class="form-group" x-data="wares()">
            <div class="d-flex flex-row justify-content-between my-3 p-3 shadow-sm rounded">
                <input type="text" class="form-control ml-4"
                    placeholder="{{ __('labels.affiches.car.added_ware_name') }}" x-model="wareName">
                <input type="number" class="form-control ml-4"
                    placeholder="{{ __('labels.affiches.car.added_ware_serial') }}" x-model="wareSerial">
                <input type="number" class="form-control ml-4"
                    placeholder="{{ __('labels.affiches.car.added_ware_quantity') }}" x-model="wareQuantity">
                <span class="ware-add" @click="addWare()"><i class="fas fa-plus-circle"></i></span>
            </div>
            <div class="ware-container p-3" x-ref="container">
                <span class="shadow-sm rounded p-2 text-center" @ware-remove="$el.remove()" x-transition:leave="popup"
                    x-transition:enter="popup" style="display: none;">
                    <input type="hidden">
                    <span class="border-left px-1">باتری</span>
                    <span class="border-left px-1">1221334</span>
                    <span class="px-1">1</span>
                    <span class="ware-delete"><i class="far fa-times-circle"
                            @click="$dispatch('ware-remove')"></i></span>
                </span>
            </div>
        </div>
    </div>

    <!-- description step -->
    <div class="form-tab">
        <div class="form-group">
            <label>{{ __('labels.affiches.car.to') }}</label>
            <input type="text" name="to_description" class="form-control" value="{{ old('car_affiche_to') }}">
        </div>
        <div class="form-group">
            <label>{{ __('labels.affiches.car.for') }}</label>
            <input type="text" name="for_description" class="form-control" value="{{ old('car_affiche_for') }}">
        </div>
        <div class="form-group">
            <label>{{ __('labels.affiches.car.issuer_note') }}</label>
            <textarea name="issuer_note" class="form-control" value="{{ old('car_affiche_note') }}"></textarea>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_affiche') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@endsection

@section('steps')
<div class="mt-2 text-center rtl" style="padding-top: 10px;">
    <span class="form-step" style="width: 15px; height: 15px;"></span>
    <span class="form-step" style="width: 15px; height: 15px;"></span>
    <span class="form-step" style="width: 15px; height: 15px;"></span>
    <span class="form-step" style="width: 15px; height: 15px;"></span>
</div>
@endsection

@section('affiche-preview')
<div class="col-12 col-md-8 card">
    <div class="card-header">
        <strong style="text-align:right;" class="card-header">{{ __('labels.affiches.generals.preview')
            }}</strong>
    </div>

    <div class="card-body card-block" style="padding-top: 0;">
        <!-- make preview -->
    </div>
</div>
@endsection

@push('form-scripts')
<script src="{{ asset('/vendor/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/vendor/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/js/datatables-init.js') }}"></script>
<script>

    $('#date3').MdPersianDateTimePicker({
        targetTextSelector: '#execution_date',
        englishNumber: true,
        textFormat: 'yyyy-MM-dd HH:mm:ss',
        isGregorian: false,
        enableTimePicker: false,
        disableBeforeToday: true,
    });

    const contributors = function () {
        return {
            values: [],
            loader: false,
            requested: false,
            url: '/users/unit',
            body: { unit: 'tv-production' }
        }
    }

    const attachAffiche = function () {
        return {
            values: [],
            attachedType: '',
            loader: false,
            requested: false,
            url: '/affiches/user/affiches',
            body: {}
        }
    }

    const wares = function () {
        return {
            wareName: '',
            wareSerial: '',
            wareQuantity: 1,
            addWare() {
                if (!this.wareName || this.wareName == '' || !this.wareSerial || this.wareSerial == '') return;

                let ware = this.$refs.container.children[0].cloneNode(true)

                ware.children[0].setAttribute('name', 'wares[]')
                ware.children[0].setAttribute(
                    'value',
                    `{
                        "name": "${this.wareName}",
                        "serial_number": "${this.wareSerial}",
                        "quantity": ${this.wareQuantity}
                    }`
                )
                ware.children[1].textContent = this.wareName
                ware.children[2].textContent = this.wareSerial
                ware.children[3].textContent = this.wareQuantity

                ware.style.display = 'inline-block'

                this.$refs.container.appendChild(ware)

                this.wareName = this.wareSerial = ''
                this.wareQuantity = 1
            }
        }
    }
</script>
@endpush