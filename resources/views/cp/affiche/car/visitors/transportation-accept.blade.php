@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('labels.affiches.generals.accept-form') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('affiches.accept', [$affiche->id]) }}" class="multi-step">
    @csrf

    <input type="hidden" name="affiche_type" value="{{ $affiche->present()->type }}">

    <div class="form-tab">
        <div class="form-group with-loader" x-data="car()">
            <label style="float: right;">{{ __('labels.car.car') }}</label>
            <select x-ref="select" x-init="getCars()" name="car" class="form-control"
                data-placeholder="{{ __('labels.car.car') }}" tabindex="5" data-tag="select2">
                <option></option>
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' @ ' + value.plate_number"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>

        <div class="form-group with-loader" x-data="driver()">
            <label style="float: right;">{{ __('labels.car.driver') }}</label>
            <select x-ref="select" x-init="selectResults" name="driver" class="form-control"
                data-placeholder="{{ __('labels.car.driver') }}" tabindex="5" data-tag="select2">
                <option></option>
                <template x-for="value in values" :key="value.id">
                    <option :value="value.id" x-text="value.name + ' ' + value.family_name"></option>
                </template>
            </select>
            <img src="{{ asset('/GIFs/Spinner-1s-40px.gif') }}" x-show="loader">
        </div>
    </div>

    <div class="form-tab">
        <div class="form-group">
            <label for="accepting">{{ __('labels.affiches.generals.comment') }}</label>
            <textarea id="accepting" name="accepted-comment" rows="3" class="form-control"
                data-tag="input">{{ $action['comment'] ?? '' }}</textarea>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_affiche') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@push('form-scripts')
<script>
    const car = function () {
        return {
            values: [],
            loader: false,
            url: '/cars/search',
            getCars() {
                this.select2 = $(this.$refs.select).select2({ allowClear: true })

                $(document).on('keyup', '.select2-search__field', async (e) => {
                    if ($.trim(e.target.value).length < 2) return;

                    this.loader = true
                    this.values = []

                    const options = {
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content'),
                            'Content-Type': 'application/json',
                            'Accept': 'application/json'
                        },
                        body: JSON.stringify({ search: $.trim(e.target.value) })
                    }

                    const success = (data) => {
                        this.values = data
                        this.select2.select2({ allowClear: true }).select2('close')
                        setTimeout(() => {
                            this.select2.select2({ allowClear: true }).select2('open')
                        }, 200);
                        this.loader = false
                        return
                    }

                    try {
                        const res = await fetch(this.url, options)

                        const data = await res.json()

                        if (!res.ok) {
                            this.loader = false

                            return
                        }

                        return success(data)

                    } catch (error) {
                        this.loader = false
                    }
                })
            }
        }
    }

    const driver = function () {
        return {
            values: [],
            loader: false,
            requested: false,
            url: '/users/role',
            body: { role: 'driver' }
        }
    }
</script>
@endpush

@endsection