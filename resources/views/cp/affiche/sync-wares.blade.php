@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">ویرایش تجهیزات آفیش</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('affiches.wares.update', $affiche->id) }}" class="multi-step">
    @method('PUT')
    @csrf

    <input type="hidden" name="affiche_type" value="{{ $affiche->present()->type }}">

    <div class="form-tab">
        <div class="col-12">
            <div class="default-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-uncountable-tab" data-toggle="tab"
                            href="#nav-uncountable" role="tab" aria-controls="nav-uncountable" aria-selected="true">
                            <span>غیرمصرفی</span>
                        </a>
                        <a class="nav-item nav-link" id="nav-countable-tab" data-toggle="tab" href="#nav-countable"
                            role="tab" aria-controls="nav-countable" aria-selected="false">
                            <span>مصرفی</span>
                        </a>
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-uncountable" role="tabpanel"
                        aria-labelledby="nav-uncountable-tab">
                        <div class="card-body px-3" style="overflow-x: auto">
                            <div class="row">
                                <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control data-table-search-text"
                                            placeholder="جستجو..." data-table="#uncountable-table">
                                    </div>
                                </dvi>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control data-table-change-length ltr"
                                            data-table="#uncountable-table">
                                            <option selected>10</option>
                                            <option>20</option>
                                            <option>30</option>
                                            <option>40</option>
                                            <option>50</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <table id="uncountable-table"
                                class="table table-striped table-bordered bootstrap-data-table-export">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc">{{ __('AuthFA.row') }}</th>
                                        <th>نام تجهیز</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($wares['uncountable'] as $i => $ware)
                                    <tr>
                                        <td class="sorting">{{ ++$i }}</td>
                                        <td>
                                            <span>{{ $ware->device_name }}</span>
                                            <input type="checkbox" name="uncountable_wares[]" class="form-check-input"
                                                value="{{ $ware->id }}" {{ (isset($ware->ware_status) &&
                                            $ware->ware_status
                                            !== 2) ? 'checked'
                                            : ''}}>

                                            @if(isset($ware->ware_status))
                                            {{ $ware->present()->ware_status }}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-countable" role="tabpanel" aria-labelledby="nav-countable-tab">
                        <div class="card-body px-3" style="overflow-x: auto">
                            <div class="row">
                                <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control data-table-search-text"
                                            placeholder="جستجو..." data-table="#countable-table">
                                    </div>
                                </dvi>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control data-table-change-length ltr"
                                            data-table="#countable-table">
                                            <option selected>10</option>
                                            <option>20</option>
                                            <option>30</option>
                                            <option>40</option>
                                            <option>50</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <table id="countable-table"
                                class="table table-striped table-bordered bootstrap-data-table-export">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc">{{ __('AuthFA.row') }}</th>
                                        <th>نام تجهیز</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($wares['countable'] as $i => $ware)
                                    <tr>
                                        <td class="sorting">{{ ++$i }}</td>
                                        <td>
                                            <span>{{ $ware->device_name }}</span>
                                            <input type="checkbox" name="countable_wares[{{ $ware->id }}]"
                                                class="form-check-input"
                                                value='{{ isset($ware->ware_status) ? $ware->ware_quantity : 1 }}' {{
                                                (isset($ware->ware_status) && $ware->ware_status !== 2) ? 'checked' :
                                            ''}}>

                                            @if(isset($ware->ware_status))
                                            {{ $ware->present()->ware_status }}
                                            @endif

                                            <div class="my-3">
                                                <span>تعداد:</span>
                                                <span class="mx-1 float-left slider-changer slider-minus">
                                                    <i class="fas fa-minus-circle"></i>
                                                </span>
                                                <input type="number" min="1"
                                                    class="p-1 mx-1 float-left text-center rounded ware-count"
                                                    value="{{ isset($ware->ware_status) ? $ware->ware_quantity : 1 }}">
                                                <span class="mx-1 float-left slider-changer slider-plus">
                                                    <i class="fas fa-plus-circle"></i>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.affiche_update') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@endsection

@section('steps')
<div class="mt-2 text-center rtl">
    <span class="form-step" style="width: 15px; height: 15px;"></span>
</div>
@endsection

@push('form-scripts')
<script>
    $('.slider-plus').on('click', function () {
        let newValue = $(this).siblings('.ware-count').val((ind, val) => {
            return ++val;
        });

        changeQuantity($(this), parseInt(newValue.val()));
    });

    $('.slider-minus').on('click', function () {
        let newValue = $(this).siblings('.ware-count').val((ind, val) => {
            if (parseInt(val) <= 1) {
                return 1;
            }

            return --val;
        });

        changeQuantity($(this), parseInt(newValue.val()));
    });

    $('.ware-count').on('change', function () {
        let newVal = parseInt($(this).val());

        if (newVal < 1) {
            $(this).val(1);
        }

        changeQuantity($(this), newVal);
    })

    const changeQuantity = (thisBtn, newVal) => {
        if (newVal < 1) {
            newVal = 1;
        }

        let check = thisBtn.parent().siblings('input[type="checkbox"]').eq(0);

        check.val(newVal);
    }
</script>
@endpush