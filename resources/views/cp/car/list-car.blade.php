@extends('layouts.tables')

@section('table-content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">

                    @include('partials.alerts')

                    <div class="card-header">
                        <strong class="card-title pull-right">{{ __('labels.car.list_title') }}</strong>
                        <a href="{{ route('cars.create') }}" class="btn btn-success pull-left">{{
                            __('labels.car.create_title') }}</a>
                    </div>
                    <div class="card-body" style="overflow-x: auto">
                        <div class="row">
                            <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                <div class="input-group">
                                    <input type="text" class="form-control data-table-search-text"
                                        placeholder="جستجو..." data-table="#table1">
                                </div>
                            </dvi>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <select class="form-control data-table-change-length ltr" data-table="#table1">
                                        <option selected>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                        <option>40</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <table id="table1" class="table table-striped table-bordered bootstrap-data-table-export">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.generals.row') }}</th>
                                    <th>{{ __('labels.car.name') }}</th>
                                    <th>{{ __('labels.car.plate') }}</th>
                                    <th>{{ __('labels.car.driver') }}</th>
                                    <th>{{ __('labels.car.affiche') }}</th>
                                    <th>{{ __('labels.generals.operations') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cars as $i => $car)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $car->name }}</td>
                                    <td>{{ $car->plate_number }}</td>
                                    <td>{{ ($car['driver']->name ?? '-') . ($car['driver']->family_name ?? '-') }}</td>
                                    <td>{{ $car['affiche']->name ?? '--' }}</td>
                                    <td class="d-flex flex-row">
                                        <a href="{{ route('cars.edit', [$car->id]) }}"
                                            class="btn btn-sm rounded-circle btn-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <form action="{{ route('cars.delete', [$car->id]) }}" method="POST"
                                            class="mx-1">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit"
                                                class="btn btn-danger btn-sm rounded-circle deleteBtn">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection