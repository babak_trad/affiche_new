@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('labels.car.update_title') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('cars.update', [$car->id]) }}" class="multi-step">
    @csrf
    @method('PUT')

    <div class="form-tab">
        <div class="form-group">
            <label style="float: right;">{{ __('labels.car.plate') }}</label>
            <div class="plate-number rounded" x-data="plateNumber()">
                <input name="plate_state" type="number" min="10" max="99" class="plate-input plate-state"
                    x-model="plateState">
                <input name="plate_right" type="number" min="100" max="999" class="plate-input plate-right"
                    x-model="plateRight">
                <select name="plate_word" class="plate-word" x-model="plateWord">
                    @foreach(__('labels.car.plate_word') as $key => $word)
                    <option {{ $key==$car['plate_number']['word'] ? 'selected' : '' }} value="{{ $key }}">
                        {{$word }}
                    </option>
                    @endforeach
                </select>
                <input type="number" name="plate_left" min="10" max="99" class="plate-input plate-left"
                    x-model="plateLeft">
                <input type="hidden" name="plate" :value="plate()">
            </div>
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('labels.car.name') }}</label>
            <input name="name" type="text" class="form-control required" data-tag="input" value="{{ $car->name }}">
        </div>
        <div class="form-group">
            <label style="float: right;">{{ __('labels.car.description') }}</label>
            <textarea name="description" class="form-control" data-tag="input"
                value="{{ $car->description }}"></textarea>
        </div>
    </div>
    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            ثبت
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            بازگشت
        </a>
    </div>
</form>
@endsection

@push('form-scripts')
<script>
    const plateNumber = () => {
        return {
            plateLeft: "{{ $car['plate_number']['left'] }}",
            plateRight: "{{ $car['plate_number']['right'] }}",
            plateWord: "{{ $car['plate_number']['word'] }}",
            plateState: "{{ $car['plate_number']['state'] }}",

            plate() {
                return `${this.plateLeft}-${this.plateWord}-${this.plateRight}-${this.plateState}`
            }
        }
    }
</script>
@endpush