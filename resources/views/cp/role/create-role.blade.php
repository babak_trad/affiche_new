@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.create_role') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('admin.roles.store') }}" class="multi-step">
    @csrf

    <div class="form-tab">
        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.role') }}</label>
            <input name="name" style="text-align:right;" type="text" class="form-control required" data-tag="input">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.slug') }}</label>
            <input name="slug" style="text-align:right;" type="text" class="form-control required" data-tag="input">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.unit_name') }}</label>
            <select name="unit_id" class="form-control select2-tag required"
                data-placeholder="{{ __('AuthFA.unit_modal_reg_message') }}" data-tag="select2">
                <option></option>
                @foreach($units as $unit)
                <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_role') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>
@endsection