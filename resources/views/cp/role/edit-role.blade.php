@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.edit_role') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('admin.roles.update', [$role->id]) }}" class="multi-step">
    @csrf
    @method('PUT')

    <div class="form-tab">
        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.role') }}</label>
            <input name="name" style="text-align:right;" type="text" class="form-control" value="{{ $role->name }}">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.slug') }}</label>
            <input name="slug" style="text-align:right;" type="text" class="form-control" value="{{ $role->slug }}">
        </div>

        <div class="form-group">
            <label style="float: right;">{{ __('AuthFA.unit_name') }}</label>
            <select name="unit_id" class="form-control" data-placeholder="{{ __('AuthFA.unit_modal_reg_message') }}">
                @foreach($units as $unit)
                <option value="{{ $unit->id }}" {{ $role->unit->id === $unit->id ? 'selected' : '' }}>
                    {{ $unit->name }}
                </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_role') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>
@endsection