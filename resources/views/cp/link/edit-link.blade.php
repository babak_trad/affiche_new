@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.edit_link') }}</strong>
</div>
@endsection

@section('form')

<form method="POST" action="{{ route('admin.links.update', [$link->id]) }}" class="multi-step">
    @csrf
    @method('PUT')

    <div class="form-tab">
        <div class="form-group">
            <label>{{ __('AuthFA.link_name') }}</label>
            <input name="name" type="text" class="form-control required" data-tag="input" value="{{ $link->name }}">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.link_url') }}</label>
            <input name="url" type="text" class="form-control required" data-tag="input" value="{{ $link->url }}">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.status') }}</label>
            <select name="status" class="form-control select2-tag" data-tag="select2"
                data-placeholder="{{ __('AuthFA.status') }}">
                <option></option>
                <option value="1" {{ ((int) $link->status) === 1 ? 'selected' : '' }}>
                    {{ __('AuthFA.active') }}
                </option>
                <option value="0" {{ ((int) $link->status) === 0 ? 'selected' : '' }}>
                    {{ __('AuthFA.deactive') }}
                </option>
            </select>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.edit_link') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@endsection