@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.create_link') }}</strong>
</div>
@endsection

@section('form')

<form method="POST" action="{{ route('admin.links.store') }}" class="multi-step">
    @csrf

    <div class="form-tab">
        <div class="form-group">
            <label>{{ __('AuthFA.link_name') }}</label>
            <input name="name" type="text" class="form-control required" data-tag="input">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.link_url') }}</label>
            <input name="url" type="text" class="form-control required" data-tag="input">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.status') }}</label>
            <select name="status" class="form-control select2-tag" data-tag="select2"
                data-placeholder="{{ __('AuthFA.status') }}">
                <option></option>
                <option value="1">{{ __('AuthFA.active') }}</option>
                <option value="0">{{ __('AuthFA.deactive') }}</option>
            </select>
        </div>
    </div>

    <div class="form-actions form-group ">
        <button type="button" class="btn btn-primary float-left" id="nextBtn">
            {{ __('AuthFA.create_link') }}
        </button>
        <a href="#" class="btn btn-secondary float-left mx-1" id="prevBtn">
            {{ __('AuthFA.back') }}
        </a>
    </div>
</form>

@endsection