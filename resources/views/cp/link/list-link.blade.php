@extends('layouts.tables')
@section('table-content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">

                    @include('partials.alerts')

                    <div class="card-header">
                        <strong class="card-title pull-right">{{ __('AuthFA.links') }}</strong>
                        @admin
                        <a href="{{ route('admin.links.create') }}" class="btn btn-success pull-left">
                            {{ __('AuthFA.create_link') }}
                        </a>
                        @endadmin
                    </div>

                    <div class="card-body" style="overflow-x: auto">
                        <div class="row">
                            <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                <div class="input-group">
                                    <input type="text" class="form-control data-table-search-text"
                                        placeholder="جستجو..." data-table="#table1">
                                </div>
                            </dvi>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <select class="form-control data-table-change-length ltr" data-table="#table1">
                                        <option selected>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                        <option>40</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <table id="table1" class="table table-striped table-bordered bootstrap-data-table-export">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('AuthFA.row') }}</th>
                                    <th>{{ __('AuthFA.link_name') }}</th>
                                    <th>{{ __('AuthFA.link_url') }}</th>
                                    <th>{{ __('AuthFA.status') }}</th>
                                    @admin
                                    <th>{{ __('AuthFA.operations') }}</th>
                                    @endadmin
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($links as $i => $link)
                                <tr class="text-center">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $link->name }}</td>
                                    <td>
                                        @if ($link->status == 1)
                                        <a class="font-weight-bold text-info" href="{{ $link->url }}"
                                            target="{{ $link->url }}">
                                            {{ $link->url }}
                                        </a>
                                        @else
                                        <span>{{ $link->url }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($link->status == 1)
                                        <span class="badge badge-success mx-1">
                                            {{ __('AuthFA.active') }}
                                        </span>
                                        @else
                                        <span class="badge badge-danger mx-1">
                                            {{ __('AuthFA.deactive') }}
                                        </span>
                                        @endif
                                    </td>
                                    @admin
                                    <td class="d-flex flex-row justify-content-center">
                                        <a href="{{ route('admin.links.edit', [$link->id]) }}"
                                            class="btn btn-sm btn-info rounded-circle">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <form action="{{ route('admin.links.delete', [$link->id]) }}" method="POST"
                                            class="mx-1">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit"
                                                class="btn btn-danger btn-sm rounded-circle deleteBtn">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                    @endadmin
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection