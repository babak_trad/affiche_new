@extends('layouts.main-layout')

@section('main-content')
<meta name="page-name" content="dashboard">
<div class="dashboard p-3 row">
    <!-- <div class="row mb-md-4"> -->
    @admin
    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('admin.users.list') }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">کاربران</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6"><i class="fas fa-users" style="color: rgb(142, 149, 234)"></i></div>
            </button>
        </a>
    </div>
    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('admin.units.list') }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">واحدها</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6"><i class="fas fa-landmark" style="color: #a72abd;"></i></div>
            </button>
        </a>
    </div>
    @endadmin
    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('affiches.list', ['type' => 'portable']) }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">آفیش ها</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6">
                    <i class="fas fa-file-signature" style="color: #f9b115;"></i>
                </div>
            </button>
        </a>
    </div>
    @can('wares.create')
    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('wares.list', ['type' => 'uncountable']) }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">انبار</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6"><i class="fas fa-warehouse" style="color: #00685a;"></i></div>
            </button>
        </a>
    </div>
    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('categories.list') }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">دسته بندی اموال</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6"><i class="fas fa-project-diagram" style="color: #1fced4;"></i></div>
            </button>
        </a>
    </div>
    @endcan

    @can('create-car')
    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('cars.list') }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">خودروها</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6">
                    <i class="fas fa-car" style="color: #597677;"></i>
                </div>
            </button>
        </a>
    </div>
    @endcan

    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('messages.list') }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">صندوق پیام</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6"><i class="fas fa-inbox" style="color: #2eb85c;"></i></div>
            </button>
        </a>
    </div>
    <div class="col-12 col-md-4 mb-4">
        <a href="{{ route('links.list') }}">
            <button class="btn">
                <div class="col-6 card-text">
                    <div class="card-topic">پیوندها</div>
                    <div class="card-details">123</div>
                </div>
                <div class="col-6"><i class="fas fa-link" style="color: #ea2c6d;"></i></div>
            </button>
        </a>
    </div>
</div>
@endsection