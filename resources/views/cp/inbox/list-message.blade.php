@extends('cp.layout.master')
    @section('content')

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    {{ $message }}
                                </div>
                            @endif

                            <div class="card-header">
                                <strong class="card-title pull-right">{{ __('AuthFA.messages_box') }}</strong>
                                <a href="{{ url('/message/create') }}" class="btn btn-success pull-left">{{ __('AuthFA.create_message') }}</a> 
                            </div>
                            <div class="card-body" style="overflow-x: auto">
                                {{-- style="direction:rtl;" --}}
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>{{ __('AuthFA.row') }}</th>
                                            <th>{{ __('AuthFA.sender') }}</th>
                                            <th>{{ __('AuthFA.subject') }}</th>
                                            <th>{{ __('AuthFA.message') }}</th>
                                            <th>{{ __('AuthFA.operations') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($messages as $i => $message)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $message->user->name. " ".$message->user->family_name  }}</td>
                                                <td>{{ $message->subject }}</td>
                                                <td>{{ $message->message }}</td>
                                                <td>
                                                    <a href="{{ url('/message/edit/'.$message->id) }}" class="btn btn-info">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/message/destroy/'.$message->id) }}" class="btn btn-danger" onclick="return confirm('آیا مطمئن هستید؟')">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    @endsection