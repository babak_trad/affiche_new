@extends('cp.layout.master')
    @section('content')

    <div class="container mt-3">
        <div class="row animated fadeIn justify-content-center">
            <div class="col-12 col-md-6 card">
                    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.edit_message') }}</strong>
                        <div class="card-body card-block">
                            <form method="POST" action="{{ route('message.insert') }}">
                                @csrf
                                    
                                    <div class="form-group">
                                        <label>{{ __('AuthFA.subject') }}</label>
                                        <input name="subject" value="{{ $message->subject }}" type="text" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ __('AuthFA.message') }}</label>
                                        <textarea name="message" id="textarea-input" rows="9" class="form-control">{{ $message->message }}</textarea>
                                    </div>


                                    <div class="pull-right offset-sm-3 mt-sm-3">
                                        @include('partials.validation-errors')
                                    </div>

                                    <div class="form-actions form-group"><button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('AuthFA.send_message') }}</button></div>
                                    <div class="form-actions form-group"><a href="{{ url('/message') }}" class="btn btn-danger btn-lg btn-block">{{ __('AuthFA.cancel') }}</a></div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    @endsection

