@extends('layouts.tables')
@section('table-content')

<div class="content mt-3">

    <div class="row mb-4 px-1">
        <div class="col-6 text-center">
            <a href="{{ route('wares.list', ['uncountable']) }}"
                class="btn btn-block btn-fade-gray-1 btn-hover-primary br-25">غیرمصرفی</a>
        </div>
        <div class="col-6 text-center">
            <a href="{{ route('wares.list', ['countable']) }}"
                class="btn btn-block btn-fade-gray-1 btn-hover-primary br-25">مصرفی</a>
        </div>
    </div>

    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">

                    @include('partials.alerts')

                    <div class="card-header">
                        <strong class="card-title">{{ __('AuthFA.warehouse_ts') }}</strong>

                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" style="float: left;" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                {{ __('AuthFA.create_device') }}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" style="text-align: right;"
                                    href="{{ route('wares.create', ['type' => 'uncountable']) }}">
                                    غیرمصرفی
                                </a>
                                <a class="dropdown-item" style="text-align: right;"
                                    href="{{ route('wares.create', ['type' => 'countable']) }}">
                                    مصرفی
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body" style="overflow-x: auto">
                        <div class="row">
                            <dvi class="col-12 col-md-6 mb-3 mb-md-0">
                                <div class="input-group">
                                    <input type="text" class="form-control data-table-search-text"
                                        placeholder="جستجو..." data-table="#table1">
                                </div>
                            </dvi>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <select class="form-control data-table-change-length ltr" data-table="#table1">
                                        <option selected>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                        <option>40</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        @yield('wares-table')

                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection