@extends('layouts.multi-step-form')

@section('header')
<div class="card-header">
    <strong style="text-align:right;" class="card-header">{{ __('AuthFA.create_ware') }}</strong>
</div>
@endsection

@section('form')
<form method="POST" action="{{ route('wares.store') }}" class="multi-step">

    @csrf

    <input type="hidden" name="ware_type" value="uncountable">

    <div class="form-tab">
        <div class="form-group">
            <label>{{ __('AuthFA.device_name') }}</label>
            <input name="device_name" type="text" class="form-control required" data-tag="input">
        </div>

        <div class="form-group" x-data>
            <label>{{ __('AuthFA.device_category') }}</label>
            <select x-ref="select" x-init="categories" name="categories[]" class="form-control select2-tag required"
                data-placeholder="{{ __('AuthFA.device_category_select_label') }}" multiple>
                <option></option>
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.part_number') }}</label>
            <input name="part_number" type="text" class="form-control required" data-tag="input">
        </div>

        <div class="form-group">
            <label>{{ __('AuthFA.serial_number') }}</label>
            <input name="serial_number" type="text" class="form-control required" data-tag="input">
        </div>
    </div>

    <div class="form-actions form-group">
        <button type="button" class="btn btn-primary btn-lg btn-block" id="nextBtn">
            {{ __('AuthFA.create_device') }}
        </button>
    </div>
    <div class="form-actions form-group">
        <a href="{{ route('wares.list', ['type' => 'uncountable']) }}" class="btn btn-danger btn-lg btn-block"
            id="prevBtn">
            {{ __('AuthFA.cancel') }}
        </a>
    </div>
</form>
@endsection

@push('form-scripts')
<script>
    const categories = function () {
        $(this.$refs.select).select2({ allowClear: true })
    }
</script>
@endpush