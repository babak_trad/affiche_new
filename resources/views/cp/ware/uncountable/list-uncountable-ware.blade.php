@extends('cp.ware.list-all-wares')
@section('wares-table')

<table id="table1" class="table table-striped table-bordered bootstrap-data-table-export">
    <thead>
        <tr class="text-center">
            <th>{{ __('AuthFA.row') }}</th>
            <th>{{ __('AuthFA.device_name') }}</th>
            <th>{{ __('AuthFA.device_category') }}</th>
            <th>{{ __('AuthFA.part_number') }}</th>
            <th>{{ __('AuthFA.serial_number') }}</th>
            <th>{{ __('AuthFA.transferee') }}</th>
            <th>در اختیار</th>
            <th>{{ __('AuthFA.operations') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($wares as $i => $ware)
        <tr class="text-center">
            <td>{{ ++$i }}</td>
            <td>{{ $ware->device_name }}</td>
            <td>
                @foreach($ware->categories as $category)
                <span class="badge badge-info rounded">
                    {{ $category->name }}
                </span>
                @endforeach
            </td>
            <td>{{ $ware->part_number }}</td>
            <td>{{ $ware->serial_number }}</td>
            <td>{{ $ware->user->name. " ". $ware->user->family_name }}</td>
            <td>{{ $ware->wareable ? $ware->wareable->name : '--' }}</td>
            <td class="d-flex flex-row justify-content-center">
                <a href="{{ route('wares.edit', ['uncountable', $ware->id]) }}"
                    class="btn btn-sm btn-primary mx-1 rounded-circle">
                    <i class="fas fa-edit"></i>
                </a>
                <form action="{{ route('wares.delete', [$ware->id]) }}" method="POST" class="mx-1">
                    {{ method_field('DELETE') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="ware_type" value="uncountable">
                    <button type="submit" class="btn btn-danger btn-sm rounded-circle deleteBtn">
                        <i class="fas fa-trash"></i>
                    </button>
                </form>
            </td>

        </tr>
        @endforeach
    </tbody>
</table>

@endsection