<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ __('AuthFA.login_title') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('/vendor/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/vendor/css/sufee-main.css') }}">

</head>

<body class="bg-dark">

    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-form">
                    <div class="pull-right mt-sm-3" style="font-size: 0.75em;">
                        @include('partials.alerts')
                        @include('partials.validation-errors')
                    </div>
                    <form method="POST" action="{{ route('admin.login') }}">
                        @csrf
                        <div class="form-group">
                            <label style="float: right;">ایمیل</label>
                            <input name="email" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label style="float: right;">رمز عبور</label>
                            <input name="password" type="password" class="form-control">
                        </div>
                        <div class="checkbox">
                            <label>
                                <a href="#">بازیابی رمز عبور</a>
                            </label>

                            <label class="pull-right">
                                بخاطر بسپار!
                                <input name="remember" type="checkbox">
                            </label>

                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30 mt-sm-3">
                            ورود
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>