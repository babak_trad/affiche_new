<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/errors/405.css') }}">
    <title>405: Method Not Allowed!</title>
</head>

<body>
    <div class="forbidden">
        <img src="{{ asset('/images/forbidden.jpg') }}" alt="">
        <div class="error-405">
            <div class="error-code">405</div>
            <div class="error-msg">درخواست شما نامعتبر است</div>
            <div class="return">
                <a href="{{ route('dashboard') }}">بازگشت</a>
            </div>
        </div>
    </div>
</body>

</html>