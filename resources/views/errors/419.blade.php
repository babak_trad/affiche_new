<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>419! Authentication Timeout</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@1,700&display=swap" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'Gandom';
            src: url('/fonts/gandom.ttf') format('truetype');
            font-weight: normal;
        }

        .status-code {
            font-family: 'Josefin Sans', sans-serif;
            font-size: 50px;
        }

        .error-message {
            font-family: 'Gandom', Arial, sans-serif;
            font-size: 14px;
        }
    </style>
</head>

<body>
    <div>
        <span class="status-code">419</span>
        <hr>
        <p class="error-message">
            به عدم فعالیت شما در این صفحه، صفحه منقضی شده است!
        </p>
        <p class="dashboard-link">
            <a href="{{ route('dashboard') }}">
                رفتن به داشبورد
            </a>
        </p>
    </div>
</body>

</html>