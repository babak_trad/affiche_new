<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/403.css') }}">
    <title>403</title>
</head>

<body>
    <div class='hover'>
        <div class='background'>
            <div class="msg-403">
                <span>شما اجازه دسترسی به این صفحه را ندارید!</span>
                <div>
                    <a href="{{ route('dashboard') }}" class="redirect-back-403">بازگشت به داشبورد</a>
                </div>
            </div>
            <div class='door'>
                <span>403</span>
            </div>
            <div class='rug'></div>
        </div>
        <div class='foreground'>
            <div class='bouncer'>
                <div class='head'>
                    <div class='neck'></div>
                    <div class='eye left'></div>
                    <div class='eye right'></div>
                    <div class='ear'></div>
                </div>
                <div class='body'></div>
                <div class='arm'></div>
            </div>
            <div class='poles'>
                <div class='pole left'></div>
                <div class='pole right'></div>
                <div class='rope'></div>
            </div>
        </div>
    </div>
</body>

</html>