import "./bootstrap";
import MultiStepFormMaker from "./multi-step-form";

$(document).ready(function() {
    $("#menuToggle").on("click", function(event) {
        $("body").toggleClass("open");
    });

    $(".search-trigger").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();
        $(".search-trigger")
            .parent(".header-left")
            .addClass("open");
    });

    $(".search-close").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();
        $(".search-trigger")
            .parent(".header-left")
            .removeClass("open");
    });

    const msfmConfig = [
        {
            tag: "input",
            container: input => null,
            getValue: input => input.val()
        },
        {
            tag: "select2",
            container: input =>
                input
                    .siblings(".select2")
                    .find(".selection span.select2-selection"),
            getValue: input =>
                input.attr("multiple")
                    ? input.select2("data").length
                    : input.select2("data")[0].id
        }
    ];

    const msfmCustomizedTab = {
        formId: "createAffiche",
        tab: 2,
        customize: () =>
            $("form#" + this.formId)
                .find(".form-tab")
                .parents(".card")
                .removeClass("col-md-6"),
        reset: () =>
            $("form#" + this.formId)
                .find(".form-tab")
                .parents(".card")
                .addClass("col-md-6")
    };

    const MSFM = new MultiStepFormMaker(
        $(".multi-step"),
        msfmConfig,
        msfmCustomizedTab
    );

    MSFM.showTab();

    $("#nextBtn").on("click", function() {
        MSFM.nextPrev(1);
    });

    $("#prevBtn").on("click", function() {
        MSFM.nextPrev(-1);
    });

    window.showToast = (message, toastIcon, timeLimit = 4000) => {
        const swalMixin = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: timeLimit,
            timerProgressBar: true,
            onOpen: toast => {
                toast.addEventListener("mouseenter", Swal.stopTimer);
                toast.addEventListener("mouseleave", Swal.resumeTimer);
            }
        });

        swalMixin.fire({
            icon: toastIcon,
            html: message
        });
    };

    // ------------------------------ broadcasting ---------------------------------------
    const playSound = jqElement => {
        jqElement.prop("muted", false);
        jqElement.get()[0].play();
    };

    const userId = $("#userId").text();

    const dispatchNewNotificationEvent = data => {
        const broadcastEvent = new CustomEvent("broadcastedNotif", {
            detail: data,
            bubbles: true
        });

        document.querySelector("#notif-dropdown").dispatchEvent(broadcastEvent);
    };

    Echo.private("users." + userId).notification(notification => {
        showToast(notification.html, "success", 6000);

        // dispatch custom js event
        dispatchNewNotificationEvent(notification.html);

        playSound($("#notificationSound"));
    });

    Echo.private("admins." + userId).notification(notification => {
        showToast(notification.html, "success", 6000);

        // dispatch custom js event
        dispatchNewNotificationEvent(notification.html);

        playSound($("#notificationSound"));
    });
});
