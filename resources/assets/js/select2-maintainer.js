export function selectMaintainer(jqSelectTag, url, type, dataCallback, placeHolder = '') {
    jqSelectTag.select2({
        ajax: {
            url: url,
            type: type,
            delay: 250,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function (params) {
                return {
                    search: params.term
                };
            },
            processResults: function (response) {
                const result = [];
                let item = {};
                $.each(response, function (index, value) {
                    item.id = value[0];
                    item.text = value[1];

                    if (value[2]) {
                        item.relations = [];
                        $.each(value[2], function (ind, val) {
                            let relation = {};
                            relation.id = val[0];
                            relation.text = val[1];
                            item.relations.push(relation);
                        });
                    }

                    result.push(item);
                    item = {};
                });
                return {
                    results: result,
                    pagination: {
                        more: false
                    }
                };
            },
        },
        minimumInputLength: 3,
        placeholder: placeHolder,
    });
}