export default class MultiStepFormMaker {

    constructor(form, config, customize = null) {
        this.tags = config;
        this.currentTab = 0;
        this.form = form;
        this.tabs = form.find('.form-tab');
        this.customizedTab = customize
    }

    getInputSpecs(input) {
        for (const tag of this.tags) {
            if (input.data('tag') && tag.tag === input.data('tag')) {
                return {
                    value: tag.getValue(input),
                    container: tag.container(input)
                };
            }
        }
    }

    validateForm() {
        let inputs = this.tabs.eq(this.currentTab).find('.required');
        let i = 0;

        for (const input of inputs) {
            let inputSpecs = this.getInputSpecs($(input));

            if (!inputSpecs.value) {
                if (inputSpecs.container) {
                    $(inputSpecs.container).addClass('invalid');
                } else {
                    $(input).addClass('invalid');
                }

                i += 1;
            } else {
                if (inputSpecs.container) {
                    $(inputSpecs.container).removeClass('invalid');
                } else {
                    $(input).removeClass('invalid');
                }
            }
        }

        if (!i) {
            $('.form-step').eq(this.currentTab).addClass('form-step-finish');
        }

        return i ? false : true;
    }

    fixStepIndicator() {
        let steps = $('.form-step');

        for (let i = 0; i < steps.length; i++) {
            steps.eq(i).removeClass('form-step-active');
        }

        steps.eq(this.currentTab).addClass('form-step-active');
    }

    showTab(lastBtn = 'ثبت') {
        this.tabs.eq(this.currentTab).show();

        if (this.currentTab === 0) {
            $('#prevBtn').hide();
        } else {
            $('#prevBtn').show();
        }

        if (this.currentTab === this.tabs.length - 1) {
            $('#nextBtn').text(lastBtn);
        } else {
            $('#nextBtn').text('ادامه');
        }

        if (this.customizedTab && this.form.attr('id') === this.customizedTab.formId) {
            if (this.currentTab === this.customizedTab.tab) {
                this.customizedTab.customize();
            } else {
                this.customizedTab.reset();
            }
        }

        this.fixStepIndicator();
    }

    nextPrev(n) {
        let valid = this.validateForm();

        if (n > 0 && !valid) {
            return false
        }

        this.tabs.eq(this.currentTab).hide();

        if (n > 0 && (this.currentTab < this.tabs.length - 1)) {
            this.tabs.eq(this.currentTab).hide();
        }

        this.currentTab += n;

        if (this.currentTab >= this.tabs.length) {
            this.form.submit();

            return false;
        } else {
            this.showTab();
        }
    }
}