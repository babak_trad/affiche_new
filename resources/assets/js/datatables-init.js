$(document).on("ready", function() {
    const swalConfirm = (message, title = "Are you sure?", icon = "info") => {
        return Swal.fire({
            icon: icon,
            title: title,
            text: message,
            showConfirmButton: true,
            confirmButtonText: "اوکی!",
            showCancelButton: true,
            cancelButtonColor: "#d33",
            cancelButtonText: "انصراف"
        });
    };

    const swalToast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: toast => {
            toast.addEventListener("mouseenter", Swal.stopTimer);
            toast.addEventListener("mouseleave", Swal.resumeTimer);
        }
    });

    $(".bootstrap-data-table-export").dataTable({
        dom: "trp",
        lengthMenu: [
            [10, 20, 50, -1],
            [10, 20, 50, "All"]
        ]
    });

    $(".bootstrap-data-table-export").on("click", ".deleteBtn", function(e) {
        e.preventDefault();

        const form = $(e.currentTarget).parent("form");

        let swalModal = swalConfirm(
            "آیا اطمینان دارید؟",
            "عملیات حذف",
            "warning"
        );

        swalModal.then(modal => {
            if (modal.isConfirmed) {
                form.trigger("submit");
            }
        });
    });

    $(".bootstrap-data-table-export").on("click", ".contributors", function(e) {
        e.preventDefault();

        const btn = $(e.currentTarget);

        let modalBody = $("#mediumModal .modal-body");
        const id = $(btn).data("id");
        const type = $(btn).data("type");

        modalBody
            .find("#contributorsData")
            .children()
            .remove();
        modalBody.find("img").show();
        modalBody.find(".alert-warning").hide();

        $.ajax({
            url: "/affiches/contributors/list",
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
            },
            data: {
                id: id,
                affiche_type: type
            }
        })
            .done(function(response) {
                if (response.status) {
                    modalBody.find("img").hide();

                    if (response.length) {
                        modalBody
                            .find("#contributorsData")
                            .append(response.data);
                    } else {
                        modalBody.find(".alert-warning").show();
                    }

                    $("#mediumModal").modal();
                }
            })
            .fail(function(jqXHR) {
                let msg = "";

                Object.values(jqXHR.responseJSON.errors).forEach(item =>
                    item.forEach(err => (msg += err + "<br>"))
                );

                swalToast.fire({
                    icon: "warning",
                    title: "خطا:",
                    html: msg
                });

                setTimeout(() => {
                    $("#mediumModal").modal("hide");
                }, 2000);
            });
    });

    $(".data-table-change-length").on("change", function() {
        let tableId = $(this).data("table");

        let length = $(this).val();

        let dTable = $(tableId).DataTable();

        dTable.page.len(length).draw();
    });

    $(".data-table-search-text").on("keyup", function() {
        let tableId = $(this).data("table");

        let dTable = $(tableId).DataTable();

        let text = $(this).val();

        dTable.search(text).draw();
    });
});
