<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Persian Lines
    |--------------------------------------------------------------------------
    |
    | The following lines contain the Persian error messages used by
    | the validator class for validating form requests.
    |
    */

    'unit' => [
        'get_roles' => [
            'unit#required' => 'تعیین نام واحد الزامی است',
            'unit#exists' => 'واحد مورد نظر یافت نشد'
        ]
    ],

    'affiches' => [
        'generals' => [
            'name#required' => 'تعیین نام آفیش الزامی است',
            'producer#required' => 'تعیین تهیه کننده الزامی است',
            'producer#integer' => 'مقدار انتخابی برای تهیه کننده نامعتبر است',
            'director#required' => 'تعیین کارگردان الزامی است',
            'director#integer' => 'مقدار انتخابی برای کارگردان نامعتبر است',
            'sound_recordist#required' => 'تعیین حداقل یک صدابردار الزامی است',
            'cameraman#required' => 'تعیین حداقل یک فیلمبردار الزامی است'
        ],
        'portable' => [
            'start_date#required' => 'تعیین زمان شروع آفیش الزامی است',
            'start_date#date' => 'فرمت تاریخ انتخابی برای شروع نامعتبر است',
            'end_date#required' => 'تعیین زمان پایان آفیش الزامی است',
            'end_date#date' => 'فرمت تاریخ انتخابی برای پایان نامعتبر است',
            'end_date#after' => 'تاریخ و زمان پایان نمی تواند قبل از تاریخ و زمان شروع آفیش باشد'
        ],
        'car' => [
            'start_time#required' => 'تعیین ساعت شروع آفیش الزامی است',
            'start_time#date_format' => 'فرمت انتخابی برای ساعت شروع نامعتبر است',
            'end_time#required' => 'تعیین ساعت پایان آفیش الزامی است',
            'end_time#date_format' => 'فرمت انتخابی برای ساعت پایان نامعتبر است',
            'end_time#after' => 'ساعت پایان افیش نمی تواند قبل از ساعت آغاز آفیش باشد',
            'execution_date#required' => 'تعیین تازیخ اجرای آفیش الزامی است',
            'execution_date#date' => 'فرمت انتخابی برای تاریج اجرا نامعتبر است',
            'execution_date#after' => 'تاریخ اجرا نمی تواند قبل از امروز باشد',
            'carrier#required' => 'تعیین مراجعه کننده برای آفیش الزامی است',
            'carrier#integer' => 'مقدار انتخابی برای مراجعه کننده نامعتبر است',
            'attached_type#required' => 'تعیین نوع آفیش ضمیمه الزامی است',
            'attached_type#in_array' => 'نوع آفیش ضمیمه موجود نیست',
            'driver#required' => 'تعیین راننده الزامی است',
            'driver#exists' => 'راننده انتخابی وجود ندارد',
            'car#required' => 'تعیین شماره پلاک خودرو الزامی است',
            'car#exists' => 'خودروی انتخابی وجود ندارد'
        ],
    ],

    'car' => [
        'name#required' => 'تعیین نام برای خودرو الزامی است',
        'plate#required' => 'تعیین شماره پلاک خودرو الزامی است',
        'plate#regex' => 'فرمت انتخابی برای شماره پلاک نامعتبر است',
        'plate#unique' => 'این شماره پلاک موجود است'
    ],

    'notification' => [
        'user#required' => 'اطلاعات ارسالی ناقص است',
        'user#exists' => 'کاربر موردنظر یافت نشد'
    ],

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'فیلد :attribute ضروری است',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'ورودی :attribute باید رشته متنی باشد',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
