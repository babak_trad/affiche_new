<?php

return [

    'categories' => [
        'properties' => [
            'name' => 'نام',
            'slug' => 'نام انگلیسی'
        ],
        'list' => [
            'title' => 'لیست دسته بندی ها'
        ],
        'create' => [
            'title' => 'ایجاد دسته بندی',
            'create_btn' => 'ثبت دسته بندی',
            'success' => 'دسته بندی با موفقیت ایجاد شد',
            'error' => 'در ایجاد دسته بندی خطایی رخ داده است!'
        ],
        'edit' => [
            'title' => 'ویرایش دسته بندی',
            'edit_btn' => 'ثبت تغییرات',
            'success' => 'بروزرسانی دسته بندی با موفقیت انجام شد.',
            'error' => 'در ثبت تغییرات دسته بندی خطایی رخ داده است!'
        ],
        'delete' => [
            'success' => 'دسته بندی با موفقیت حذف شد.',
            'error' => 'در حذف دسته بندی خطایی رخ داده است!'
        ],
    ],

    //Login's Constants
    'login_title' => 'سامانه آفیش',
    'employee_num' => 'شماره کارمندی',
    'password' => 'گذرواژه',
    'remember_me' => 'مرا به یاد بیاور',
    'forgot_password' => 'فراموشی گذرواژه؟',
    'login' => 'ورود',
    'slogon' => 'شعار سایت',
    'logout' => 'خروج',

    //Register's Constants
    'users' => 'کاربران',
    'register_title' => 'ثبت کاربر جدید',
    'name' => 'نام',
    'family_name' => 'نام خانوادگی',
    'name_and_family' => 'نام و نام خانوادگی',
    'phone_num' => 'شماره همراه',
    'register' => 'ثبت نام',
    'unit' => 'واحد',
    'units' => 'واحدها',
    'department' => 'معاونت',
    'unit_name' => 'نام واحد',
    'create_unit' => 'ایجاد واحد',
    "department_name" => 'نام معاونت',
    'unit_assign' => 'تعیین واحد',
    'role_assign' => 'تعیین نقش',
    'confirm_password' => 'تکرار گذرواژه',
    'confirm' => 'تایید',
    'cancel' => 'انصراف',
    'unit_modal_reg_message' => 'کاربر در چه واحدی خدمت می کند؟',
    'role_modal_reg_message' => 'وظیفه کاربر در واحد خدمتی چیست؟',

    //Unit's Constants
    'technical_support' => 'پشتیبانی فنی',
    'tv_production' => 'تولید سیما',
    'radio_production' => 'تولید رادیو',
    'it' => 'فناوری اطلاعات',
    'satellite_terrestrial' => ' ارتباطات زمینی و ماهواره ای',
    'transportation' => 'ترابری',
    'security' => 'حراست',
    'technical_warehouse' => 'انبار پشتیبانی فنی',
    'bad_user_or_password' => 'شماره کارمندی یا گذرواژه نادرست است',
    'ok_register' => 'ثبت نام انجام شد',

    //Pages Title
    'users_page_title' => 'مدیریت کاربران',
    'position' => 'عنوان شغلی',
    'operations' => 'عملیات',
    'dashboard' => 'داشبورد مدیریت',
    'users_data' => 'اطلاعات کاربران',

    'dropdown_notification_message_counter' => 'شما 3 پیام دارید',
    'dropdown_notification_message_1' => 'کاربر جدید ایجاد شد',
    'dropdown_notification_message_2' => 'شما یک پیغام دارید',
    'dropdown_notification_message_3' => 'شما یک اخطار دارید',

    //General Labels
    'dena_affiche_system' => 'سامانه آفیش شبکه دنا',
    'roles' => 'نقش ها',
    'role' => 'نقش',
    'table' => 'جدول',
    'general_facilities' => 'امکانات عمومی',
    'personal_facilities' => 'امکانات خصوصی',
    'create_user' => 'ایجاد کاربر جدید',
    'edit_user' => 'ویرایش کاربر',
    'create_role' => 'ایجاد نقش جدید',
    'edit_role' => 'ویرایش نقش',
    'assign_password_label' => 'تعیین گذرواژه',
    'assign_personalies_label' => 'تعیین مشخصات کاربری',
    'create_message' => 'ایجاد پیام',
    'message' => 'متن پیام',
    'subject' => 'موضوع',
    'type' => 'نوع',
    'sender' => 'ارسال کننده',
    'inbox' => 'صندوق پیام',
    'edit_message' => 'ویرایش پیام',
    'back' => 'بازگشت',

    'delete' => 'حذف',
    'edit' => 'ویرایش',
    'add' => 'اضافه',
    'search' => 'جستجو',
    'previous' => 'پیشین',
    'next' => 'بعدی',
    'settings' => 'تنظیمات',
    'profile_settings' => 'تنظیمات کاربری',
    'row' => 'ردیف',
    'status' => 'وضعیت',
    'link_status' => 'انتخاب وضعیت پیوند',
    'active' => 'فعال',
    'deactive' => 'غیرفعال',
    'send_message' => 'ارسال پیام',

    'submit_changes' => 'اعمال تغییرات',

    //Unit
    'unit_name' => 'نام واحد',
    'unit_slug' => 'نام انگلیسی',
    'unit_edit' => 'ویرایش واحد',
    'receiver_unit' => 'واحد دریافت کننده',
    'receiver' => 'دریافت کننده',
    'receiver_role' => 'نقش دریافت کننده',
    'affiche_receiver_role' => 'نقش دریافت کننده آفیش',
    'select_one_or_many_unit' => 'یک یا چند واحد را انتخاب کنید',

    //Affiche
    'affiches' => 'آفیش ها',
    'affiche_name' => 'نام آفیش',
    'affiche_edit' => 'ویرایش آفیش',
    'affiche_update' => 'بروزرسانی آفیش',
    'affiche_status' => 'وضعیت آفیش',
    'start_datetime' => 'تاریخ و زمان شروع',
    'end_datetime' => 'تاریخ و زمان پایان',
    'start_time' => 'ساعت شروع آفیش',
    'end_time' => 'ساعت پایان آفیش',
    'sender_unit' => 'واحد صادرکننده',
    'receiver_units' => 'واحدهای دریافت کننده',
    'create_affiche' => 'صدور آفیش',
    'affiche_details' => 'جزئیات آفیش',
    'show_affiche' => 'نمایش آفیش',
    'delete_affiche' => 'حذف آفیش',
    'issue_date' => 'تاریخ صدور',
    'approval_date' => 'تاریخ تایید',
    'affiche_location' => 'موقعیت کنونی آفیش',
    'affiche_wares_list' => 'لیست تجهیزات',


    //Office Labels
    'attendance_system' => 'سیستم حضور و غیاب',
    'change_password' => 'تغییر گذرواژه',
    'messages_box' => 'صندوق پیام ها',
    'permissions' => 'دسترسی ها',
    'contact_ceo' => 'ارتباط با مدیرکل',

    //Messages
    'user_created' => 'کاربر ایجاد شد',
    'user_deleted' => 'کاربر حذف شد',

    //links
    'link' => 'پیوند',
    'links' => 'پیوندها',
    'link_name' => 'نام پیوند',
    'link_url' => 'آدرس پیوند',
    'create_link' => 'ایجاد پیوند',
    'show_link' => 'نمایش پیوند',
    'delete_link' => 'حذف پیوند',
    'edit_link' => 'ویرایش لینک',

    //warehouse
    'ware' => 'کالا',
    'warehouse' => 'انبار',
    'warehouse_ts' => 'انبار پشتیبانی فنی',
    'device' => 'دستگاه',
    'devices' => 'دستگاه ها',
    'audio_devices' => 'تجهیزات صدا',
    'video_devices' => 'تجهیزات تصویر',
    'light_devices' => 'تجهیزات نور',
    'create_device' => 'ایجاد دستگاه',
    'device_name' => 'نام دستگاه',
    'part_number' => 'شماره امول',
    'serial_number' => 'شماره سریال',
    'device_category' => 'دسته بندی',
    'transferee' => 'تحویل گیرنده',
    'device_category_select_label' => 'انتخاب دسته بندی دستگاه',
    'device_transferee_select_label' => 'انتخاب تحویل گیرنده',

    'create_ware' => 'ایجاد دستگاه',
    'edit_ware' => 'ویرایش دستگاه',

    //Role Lables
    'producer' => 'تهیه کننده',
    'producer_assistant' => 'دستیار تهیه کننده',
    'director' => 'کارگردان',
    'director_assistant' => 'دستیار کارگردان',
    'sound_recordist' => 'صدابردار',
    'sound_recordist_assistant' => 'دستیار صدابردار',
    'cameraman' => 'تصویربردار',
    'tech_assistant' => 'دستیار فنی',

    'select_producer' => 'انتخاب تهیه کننده',
    'select_producer_assistant' => 'انتخاب یک یا چند دستیار تهیه کننده',
    'select_director' => 'انتخاب کارگردان',
    'select_director_assistant' => 'انتخاب دستیار کارگردان',
    'select_sound_recordist' => 'انتخاب صدا بردار',
    'select_sound_recordist_assistant' => 'انتخاب دستیار صدابردار',
    'select_cameraman' => 'انتخاب یک یا چند تصویربردار',
    'select_tech_assistant' => 'انتخاب یک یا چند دستیار فنی',

    'slug' => 'اسم مستعار',
    'role_edit' => 'ویرایش نقش کاربری',

    //Car Affiche
    'applicant' => 'درخواست کننده',



];
