<?php

namespace App\Policies\Affiche;

use App\Models\Affiches\AfficheVisitor;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Auth\Access\HandlesAuthorization;

class AffichePolicy
{
    use HandlesAuthorization;

    public function view($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        return ($user->id === $affiche->user_id) || ($user->id === $affiche->issuer_id);
    }

    public function create($user, string $type = null)
    {
        if (!$user->unit) {
            return false;
        }

        $canCreate = false;

        if (is_null($type)) {
            foreach (config('my-lookup.affiche.types') as $typeConfig) {
                if (!array_key_exists('creators', $typeConfig)) {
                    continue;
                }

                if (in_array($user->unit->slug, $typeConfig['creators'])) {
                    $canCreate = true;

                    break;
                }
            }

            return $canCreate;
        }

        return in_array($user->unit->slug, AfficheConfig::creators($type));
    }

    public function edit($user, $affiche = null)
    {
        if (!$affiche) {
            return false;
        }

        return (($user->id === $affiche->user_id) || ($user->id === $affiche->issuer_id))
            && ($affiche->status === AfficheConfig::statuses('draft'));
    }

    public function delete($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        return (($user->id === $affiche->user_id) || ($user->id === $affiche->issuer_id))
            && ($affiche->status === AfficheConfig::statuses('draft'));
    }

    public function visit($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        $unit = $user->unit->slug;

        if ($affiche->flow === AfficheConfig::flow('foreward')) {
            $topUnit = AfficheConfig::firstUnit($affiche->present()->type);
        } else {
            $topUnit = AfficheConfig::lastUnit($affiche->present()->type);
        }

        $canVisit = AfficheVisitor::canVisit($affiche, $unit)->get()->first();

        return ($unit === $topUnit) || $canVisit;
    }

    public function action($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        $isActive = $affiche->status === AfficheConfig::statuses('active');

        // user can not apply action if he applyed before!
        $isInHisUnit = $user->unit->slug === $affiche->current_state;

        return $isActive && $isInHisUnit;
    }

    public function reject($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        return $user->unit->slug === AfficheConfig::firstUnit($affiche->present()->type);
    }

    public function archive($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        return $affiche->status === AfficheConfig::statuses('active')
            && $affiche->current_state === $affiche->user->unit->slug;
    }

    public function viewWares($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        $canAction = $this->action($user, $affiche);

        $itsOwner = $affiche->user_id === $user->id;

        return $itsOwner || $canAction;
    }

    public function editWares($user, $affiche = null)
    {
        if (!$affiche || !$user->unit) {
            return false;
        }

        $inForewardFlow = $affiche->flow === AfficheConfig::flow('foreward');

        $isActive = $affiche->status === AfficheConfig::statuses('active');

        //TODO: Is there better way to check this rule?!
        $inWarehouseUnit = $affiche->current_state === 'ware-house';

        return $inForewardFlow && $isActive && $inWarehouseUnit;
    }
}
