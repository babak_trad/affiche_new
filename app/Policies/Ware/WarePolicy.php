<?php

namespace App\Policies\Ware;

use Illuminate\Auth\Access\HandlesAuthorization;

class WarePolicy
{
    use HandlesAuthorization;

    public function view($user, $ware = null)
    {
        if (!isset($ware) || !$user->unit) {
            return false;
        }

        return $user->id === $ware->transferee;
    }

    public function create($user)
    {
        if (!$user->unit) {
            return false;
        }

        //TODO: Use config instead of hard coding!
        return $user->unit->slug === 'ware-house';
    }

    public function edit($user, $ware = null)
    {
        if (!isset($ware) || !$user->unit) {
            return false;
        }

        return $user->id === $ware->transferee;
    }

    public function delete($user, $ware = null)
    {
        if (!isset($ware) || !$user->unit) {
            return false;
        }

        return $user->id === $ware->transferee;
    }

    public function editAffiche($user, $affiche = null)
    {
        if (!isset($affiche) || !$user->unit) {
            return false;
        }

        //TODO: Use config instead of hard coding!
        return ($user->unit->slug === 'ware-house') && ($affiche->current_state == 'ware-house');
    }
}
