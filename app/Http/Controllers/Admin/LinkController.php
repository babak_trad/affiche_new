<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Link\LinkCreateRequest;
use App\Models\Link;

class LinkController extends Controller
{

    public function create()
    {
        return view('cp.link.create-link');
    }

    public function store(LinkCreateRequest $request)
    {
        $link = Link::create([
            'name' => $request['name'],
            'url' => $request['url'],
            'status' =>  $request['status'],
        ]);

        if (!($link instanceof Link)) {
            return redirect()->route('links.list')->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('links.list')->with('success', 'پیوند ایجاد شد');
    }

    public function edit($id)
    {
        $link = Link::findOrFail($id);

        return view('cp.link.edit-link', compact('link'));
    }

    public function update(LinkCreateRequest $request, $id)
    {
        $link = Link::findOrFail($id);

        $link->name = $request->get('name');
        $link->url = $request->get('url');
        $link->status = $request->get('status');

        $result = $link->save();

        if (!$result) {
            return redirect()->route('links.list')->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('links.list')->with('success', 'پیوند بروزرسانی شد');
    }

    public function delete(Link $link)
    {
        $result = $link->delete();

        if (!$result) {
            return redirect()->route('links.list')->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('links.list')->with('success', 'پیوند حذف شد');
    }
}
