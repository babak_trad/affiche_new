<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\User\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\UserStoreRequest;

class UsersController extends Controller
{
    private $userService;

    public function __construct()
    {
        $this->userService = resolve(UserService::class);
    }

    public function list()
    {
        $users = User::with(['roles', 'unit'])->orderBy('employee_num')->get();

        return view('cp.user.list-user', compact('users'));
    }

    public function create()
    {
        $units = Unit::get(['id', 'name']);
        $roles = Role::get(['id', 'name']);
        return view('cp.user.create-user', compact('units', 'roles'));
    }

    protected function store(UserStoreRequest $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'family_name' => $request['family_name'],
            'employee_num' => $request['employee_num'],
            'phone_num' => $request['phone_num'],
            'unit_id' => $request['unit_id'],
            'password' => Hash::make($request['password']),
        ]);

        foreach ($request['role_id'] as $role_id) {
            $user->roles()->attach($role_id);
        }

        return redirect()->route('admin.users.list')->with('success', 'کاربر ایجاد شد');
    }

    public function edit(int $id)
    {
        $user = User::with(['unit', 'roles'])->find($id);

        $units = Unit::all();

        return view('cp.user.edit-user', compact('user', 'units'));
    }

    public function update(Request $request, int $id)
    {
        $user = User::findOrFail($id);

        $user->name = $request->get('name');
        $user->family_name = $request->get('family_name');
        $user->employee_num = $request->get('employee_num');
        $user->phone_num = $request->get('phone_num');
        $user->unit_id = $request->get('unit_id');
        $user->save();

        $user->roles()->sync($request->get('role_id'));

        return redirect()->route('admin.users.list')->with('success', 'اطلاعات کاربر بروزرسانی شد');
    }

    public function delete(int $id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.list')->with('success', 'کاربر حذف شد');
    }

    public function findByRole(Request $request)
    {
        $users = $request['role'] ?
            $this->userService->findByRole($request->role)->map->only(['id', 'name', 'family_name'])
            : collect([]);

        return response()->json($users);
    }

    public function findByUnit(Request $request)
    {
        $users = $request['unit'] ?
            $this->userService->findByUnit($request->unit)->map->only(['id', 'name', 'family_name'])
            : collect([]);

        return response()->json($users);
    }
}
