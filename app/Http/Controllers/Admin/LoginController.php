<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.admin_login');
    }

    public function login(LoginRequest $request)
    {
        if ($this->attempLogin($request)) {
            return $this->sendLoginSuccessResponse();
        } else {
            return $this->sendLoginFailResponse();
        }
    }

    protected function attempLogin(Request $request)
    {
        return Auth::guard('web_admin')->attempt([
            'email' => $request['email'],
            'password' => $request['password']
        ]);
    }

    protected function sendLoginSuccessResponse()
    {
        session()->regenerate();
        return redirect()->route('dashboard');
    }

    protected function sendLoginFailResponse()
    {
        return back()->with('badUserPass', 'ایمیل یا رمز عبور نادرست است');
    }

    public function logout(Request $request)
    {
        Auth::guard('web_admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->route('admin.login');
    }
}
