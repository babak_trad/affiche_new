<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Unit;

class RoleController extends Controller
{
    public function list()
    {
        $roles = Role::with('unit')->get();
        return view('cp.role.list-role', compact('roles'));
    }

    public function create()
    {
        $units = Unit::all();

        if ($units->isEmpty()) {
            return back()->with('error', 'هیچ واحد خدمتی وجود ندارد! ابتدا یک واحد خدمتی ایجاد کنید.');
        }

        return view('cp.role.create-role', compact('units'));
    }

    public function store(Request $request)
    {
        $this->validateForm($request);

        $role = Role::create([
            'unit_id' => (int) $request['unit_id'],
            'name' => $request['name'],
            'slug' => $request['slug'],
        ]);

        if (!$role) {
            return redirect()->route('admin.roles.list')->with('error', 'در ایجاد نقش خطایی رخ داده است');
        }

        return redirect()->route('admin.roles.list')->with('success', 'نقش کاربری ایجاد شد');
    }

    public function edit($id)
    {
        $role = Role::with('unit')->find($id);

        $units = Unit::all();

        return view('cp.role.edit-role', compact('role', 'units'));
    }

    public function update(Request $request, $id)
    {
        $this->validateForm($request);

        $role = Role::find($id);

        $role->unit_id = $request->get('unit_id');
        $role->name = $request->get('name');
        $role->slug = $request->get('slug');
        $role->save();

        return redirect()->route('admin.roles.list')->with('success', 'نقش کاربری بروزرسانی شد');
    }

    public function delete($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('admin.roles.list')->with('success', 'نقش کاربری حذف شد');
    }

    private function validateForm(Request $request)
    {
        $request->validate([
            'unit_id' => 'required',
            'name' => 'required|string|max:255',
            'slug' => 'required|string',
        ]);
    }
}
