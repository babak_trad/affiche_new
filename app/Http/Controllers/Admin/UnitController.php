<?php

namespace App\Http\Controllers\Admin;

use App\Models\Unit;
use Illuminate\Http\Request;
use App\Services\Unit\UnitService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Unit\RolesOfUnitRequest;
use App\Http\Requests\Unit\UnitCreateRequest;
use App\Http\Requests\Unit\UnitUpdateRequest;

class UnitController extends Controller
{
    private $unitService;

    public function __construct()
    {
        $this->unitService = resolve(UnitService::class);
    }

    public function list()
    {
        $units = Unit::all();
        return view('cp.unit.list-unit', compact('units'));
    }

    public function create()
    {
        return view('cp.unit.create-unit');
    }

    public function store(UnitCreateRequest $request)
    {
        Unit::create([
            'name' => $request['name'],
            'department' => $request['department'],
            'slug' => $request['slug']
        ]);

        return redirect()->route('admin.units.list')->with('success', 'واحد ایجاد شد');
    }

    public function edit($id)
    {
        $unit = Unit::findOrFail($id);

        return view('cp.unit.edit-unit', compact('unit'));
    }

    public function update(UnitUpdateRequest $request, $id)
    {
        $unit = Unit::findOrFail($id);

        $unit->name = $request->get('name');
        $unit->department = $request->get('department');
        $unit->slug = $request['slug'];
        $unit->save();

        return redirect()->route('admin.units.list')->with('success', 'واحد بروزرسانی شد');
    }

    public function delete($id)
    {
        $unit = Unit::findOrFail($id);
        $unit->delete();

        return redirect()->route('admin.units.list')->with('success', 'واحد حذف شد');
    }

    public function rolesOfUnit(RolesOfUnitRequest $request)
    {
        $roles = $this->unitService->getRoles($request->unit);

        $roles = $roles->map->only(['id', 'name']);

        sleep(3);

        return response()->json($roles);
    }
}
