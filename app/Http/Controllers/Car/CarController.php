<?php

namespace App\Http\Controllers\Car;

use App\Models\Car;
use Illuminate\Http\Request;
use App\Services\Car\CarService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Car\CarRequest;

class CarController extends Controller
{

    private $carService;

    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function list()
    {
        $cars = $this->carService->all();

        return view('cp.car.list-car', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cp.car.create-car');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CarRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        $category = Car::create([
            'plate_number' => $request->input('plate'),
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);

        if (!($category instanceof Car)) {
            return redirect()->route('cars.list')->with('error', 'در ایجاد خودرو خطایی رخ داده است.');
        }

        return redirect()->route('cars.list')->with('success', 'خودرو با موفقیت ایجاد شد.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Car  $car
     * @return \Illuminate\View\View
     */
    public function edit(Car $car)
    {
        list($left, $word, $right, $state) = explode('-', $car->plate_number);

        $car->plate_number = [
            'left' => $left,
            'right' => $right,
            'word' => $word,
            'state' => $state
        ];

        return view('cp.car.edit-car', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CarRequest  $request
     * @param  Car  $car
     * @return
     */
    public function update(Car $car, CarRequest $request)
    {
        $result = $car->update([
            'plate_number' => $request->input('plate'),
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);

        if (!$result) {
            return redirect()->route('cars.list')->with('error', 'در بروزرسانی خودرو خطایی رخ داده است.');
        }

        return redirect()->route('cars.list')->with('success', 'خودرو با موفقیت بروزرسانی شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Car  $car
     * @return
     */
    public function delete(Car $car)
    {
        $result = $car->delete();

        if (!$result) {
            return redirect()->route('cars.list')->with('error', 'در حذف خودرو خطایی رخ داده است.');
        }

        return redirect()->route('cars.list')->with('success', 'خودرو با موفقیت حذف شد.');
    }

    public function search(Request $request)
    {
        $search = $request['search'] ?? null;

        $cars = $this->carService->find($search, true, ['id', 'name', 'plate_number']);

        return response()->json($cars);
    }
}
