<?php

namespace App\Http\Controllers\Ware;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Ware\Implementations\NullWareService;
use App\Services\Ware\WareServiceFactory;

class WaresController extends Controller
{
    use WareServiceFactory;

    public function list(string $type)
    {
        $this->authorize('wares.create');

        $service = self::wareService($type);

        return $service->list($type);
    }

    public function create(string $type)
    {
        $this->authorize('wares.create');

        $service = self::wareService($type);

        return $service->create();
    }

    public function store(Request $request)
    {
        $wareType = $request->input('ware_type');

        $service = self::wareService($wareType);

        if ($service instanceof NullWareService) {
            return $service->back();
        }

        $ware = $service->store($request);

        if (!$ware) {
            return redirect()->route('wares.list', ['type' => $wareType])
                ->with('error', 'در ایجاد دستگاه مشکلی رخ داده است.');
        }

        return redirect()->route('wares.list', ['type' => $wareType])
            ->with('success', 'دستگاه ایجاد شد');
    }

    public function edit(string $wareType, int $wareId)
    {
        $service = self::wareService($wareType);

        if ($service instanceof NullWareService) {
            return $service->back();
        }

        $ware = $service->find($wareId);

        $this->authorize('wares.create');

        $this->authorize('wares.edit', $ware);

        return $service->edit($wareId);
    }

    public function update(int $wareId, Request $request)
    {
        $wareType = $request->input('ware_type');

        $service = self::wareService($wareType);

        if ($service instanceof NullWareService) {
            return $service->back();
        }

        $result = $service->update($wareId, $request);

        if (!$result) {
            return redirect()->route('wares.list', ['type' => $wareType])
                ->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('wares.list', ['type' => $wareType])
            ->with('success', 'دستگاه با موفقیت بروزرسانی شد');
    }

    public function delete($wareId, Request $request)
    {
        $wareType = $request->input('ware_type');

        $service = self::wareService($wareType);

        if ($service instanceof NullWareService) {
            return $service->back();
        }

        $ware = $service->find($wareId);

        $this->authorize('wares.create');

        $this->authorize('wares.delete', $ware);

        $result = $service->delete($wareId);

        if (!$result) {
            return redirect()->route('wares.list', ['type' => $wareType])->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('wares.list', ['type' => $wareType])->with('success', 'دستگاه حذف شد');
    }
}
