<?php

namespace App\Http\Controllers\Affiche;

use App\Http\Controllers\Controller;
use App\Http\Requests\Affiche\AfficheContributorsRequest;
use App\Services\Affiche\Implementations\Services\AfficheServiceFactory;

class AfficheContributorsController extends Controller
{
    use AfficheServiceFactory;

    public function list(AfficheContributorsRequest $request)
    {
        $afficheId = $request->id;

        $contributors = self::afficheService($request->affiche_type)->getContributors($afficheId);

        $view = view('cp.affiche.contributors', compact('contributors'))->render();

        //TODO: remove this line in production!
        sleep(3);

        return response()->json(['status' => true, 'data' => $view, 'length' => $contributors->count()]);
    }
}
