<?php

namespace App\Http\Controllers\Affiche;

use Exception;
use Illuminate\Http\Request;
use App\Services\Ware\WareConfing;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Affiche\AfficheBaseRequest;
use App\Services\Affiche\Implementations\Services\NullAfficheService;
use App\Services\Affiche\Implementations\Services\AfficheServiceFactory;

class AfficheWaresController extends Controller
{
    use AfficheServiceFactory;

    public function list(string $type, int $afficheId)
    {
        $service = self::afficheService($type);

        if ($service instanceof NullAfficheService) {
            return $service->back();
        }

        $affiche = $service->find($afficheId);

        $this->authorize('affiches.view-wares', $affiche);

        // TODO: Use array of ware services to get all wares of affiche
        $afficheWares = $affiche->syncer('default')->draftWares(['user', 'categories'])->flatten();

        session(['affiche' => $affiche, 'afficheWares' => $afficheWares]);

        return redirect()->route('affiches.wares.view');
    }

    public function view()
    {
        $affiche = session('affiche');

        $afficheWares = session('afficheWares');

        $afficheWares->map(function ($ware) {
            $ware['ware_status'] = $ware->pivot->ware_status;
        });

        return view('cp.affiche.view-wares', compact('affiche', 'afficheWares'));
    }

    public function edit(string $type, int $afficheId)
    {
        $service = self::afficheService($type);

        if ($service instanceof NullAfficheService) {
            return $service->back();
        }

        $affiche = $service->find($afficheId);

        $this->authorize('affiches.edit-wares', $affiche);

        $wares = $service->allWares();

        // TODO: Use array of ware services to get all wares of affiche
        $aWares = $affiche->syncer('default')->draftWares()->flatten();

        $wares = $this->markAsAfficheWare($wares, $aWares);

        return view('cp.affiche.sync-wares', compact('affiche', 'wares'));
    }

    public function update(int $afficheId, AfficheBaseRequest $request)
    {
        $service = self::afficheService($request['affiche_type']);

        $affiche = $service->find($afficheId);

        $this->authorize('affiches.edit-wares', $affiche);

        //TODO: Use config helper to make code extendable!
        $wareIds = [
            'uncountable' => $request->input('uncountable_wares'),
            'countable' => $request->input('countable_wares')
        ];

        // TODO: Use array of ware services to get all wares of affiche
        $associated = $affiche->syncer('default')->validateAssociatingWares([
            'uncountable' => $wareIds['uncountable']
        ]);

        if (!empty($associated)) {
            return back()->with('wareAssociating', $associated);
        }

        DB::beginTransaction();

        try {
            $affiche->syncer('default')->syncDraftWares($wareIds, WareConfing::status('accepted'), true);

            $affiche->syncer('default')->syncFinalWares($wareIds);

            $message = 'لیست تجهیزات آفیش بروزرسانی شد';

            $result = true;

            DB::commit();
        } catch (Exception $e) {
            $message = 'خطایی رخ داده است';

            $result = false;

            DB::rollBack();

            throw $e;
        }

        return back()->with($result ? 'success' : 'error', $message);
    }

    private function markAsAfficheWare($allWares, $afficheWares)
    {
        foreach ($allWares as $wares) {
            foreach ($wares as $ware) {
                $afficheWares->each(function ($item) use ($ware) {
                    if ($item->id == $ware->id && $item->type == $ware->type) {
                        $ware['ware_status'] = $item->pivot->ware_status;
                        $ware['ware_type'] = $item->pivot->ware_type;
                        $ware['ware_quantity'] = $item->pivot->ware_quantity;
                    }
                });
            }
        }

        return $allWares;
    }
}
