<?php

namespace App\Http\Controllers\Affiche;

use Exception;
use App\Http\Controllers\Controller;
use App\Events\Affiche\AfficheAccepted;
use App\Http\Requests\Affiche\AfficheBaseRequest;
use App\Services\Affiche\Implementations\Services\NullAfficheService;
use App\Services\Affiche\Implementations\Services\AfficheServiceFactory;

class AfficheCreatorController extends Controller
{
    use AfficheServiceFactory;

    public function create(string $type)
    {
        $this->authorize('affiches.create', $type);

        $service = static::afficheService($type);

        return $service->create();
    }

    public function store(AfficheBaseRequest $request)
    {
        $service = self::afficheService($request['affiche_type']);

        try {
            $service->store($request);
        } catch (Exception $e) {
            throw $e;
            return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
            ->with('success', 'آفیش ایجاد شد');
    }

    public function edit(string $type, int $afficheId)
    {
        $service = self::afficheService($type);

        if ($service instanceof NullAfficheService) {
            return $service->back();
        }

        $this->authorize('affiches.edit', $service->find($afficheId));

        return $service->edit($afficheId);
    }

    public function update(int $afficheId, AfficheBaseRequest $request)
    {
        $service = self::afficheService($request['affiche_type']);

        try {
            $service->update($afficheId, $request);
        } catch (Exception $e) {
            return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
            ->with('success', 'آفیش بروزرسانی شد');
    }

    public function delete(int $afficheId, AfficheBaseRequest $request)
    {
        $service = self::afficheService($request['affiche_type']);

        $this->authorize('affiches.delete', $service->find($afficheId));

        $result = false;

        try {
            $result = $service->delete($afficheId);
        } catch (Exception $e) {
            return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('error', 'خطایی رخ داده است!');
        }

        if (!$result) {
            return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('error', 'خطایی رخ داده است!');
        }

        return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
            ->with('success', 'آفیش حذف شد');
    }

    public function send(int $afficheId, AfficheBaseRequest $request)
    {
        $service = self::afficheService($request['affiche_type']);

        try {
            $service->send($afficheId);

            return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('success', 'آفیش با موفقیت ارسال شد.');
        } catch (Exception $e) {
            redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('error', 'خطایی رخ داده است!');
        }
    }

    public function archive(int $afficheId, AfficheBaseRequest $request)
    {
        $service = self::afficheService($request['affiche_type']);

        try {
            $service->archive($afficheId);

            return redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('success', 'آفیش با موفقیت آرشیو شد.');
        } catch (Exception $e) {
            redirect()->route('affiches.list', ['type' => $request['affiche_type']])
                ->with('error', 'خطایی رخ داده است!');
        }
    }
}
