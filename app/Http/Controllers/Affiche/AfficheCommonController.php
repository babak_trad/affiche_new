<?php

namespace App\Http\Controllers\Affiche;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Affiche\CarAfficheAttachingRequest;
use App\Services\Affiche\AfficheConfig;
use App\Services\Affiche\Implementations\Services\AfficheServiceFactory;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Auth;

class AfficheCommonController extends Controller
{
    use AfficheServiceFactory;

    private $userService;

    public function __construct()
    {
        $this->userService = resolve(UserService::class);
    }

    public function list(string $type = 'portable')
    {
        $service = self::afficheService($type);

        return $service->list();
    }

    public function actionForm(int $afficheId, string $action, Request $request)
    {
        $service = self::afficheService($request['affiche_type']);

        $affiche = $service->find($afficheId);

        $this->authorize('affiches.action', $affiche);

        session(['affiche' => $affiche, 'action' => $action, 'afficheType' => $request['affiche_type']]);

        return redirect()->route('affiches.action');
    }

    public function action()
    {
        $type = session('afficheType');
        $affiche = session('affiche');
        $action = session('action');

        $this->authorize('affiches.action', $affiche);

        $service = self::afficheService($type);

        return $service->action($affiche, $action);
    }

    public function accept(int $afficheId, Request $request)
    {
        $service = self::afficheService($request['affiche_type']);

        $affiche = $service->find($afficheId, ['user']);

        $this->authorize('affiches.action', $affiche);

        $result = $service->accept($affiche, $request);

        if (array_key_exists('pendingWaresError', $result)) {
            return back()->with('pendingWaresError', count($result['pendingWaresError']));
        }

        if (array_key_exists('failedValidation', $result)) {
            return back()->withErrors($result['failedValidation']);
        }

        [$nextUnit, $isNewAccept, $affiche] = [...$result];

        $affiche->current_state = $nextUnit;
        $affiche->save();

        session()->forget(['afficheType', 'affiche', 'action']);

        $message = $isNewAccept ? 'آفیش باموفقیت تأیید شد' : 'نظر شما باموفقیت بروزرسانی شد';

        return redirect()->route('affiches.list', ['type' => $request['affiche_type']])->with('success', $message);
    }

    public function reject(int $afficheId, Request $request)
    {
        $service = self::afficheService($request['affiche_type']);

        $affiche = $service->find($afficheId);

        $this->authorize('affiches.action', $affiche);

        $this->authorize('affiches.reject', $affiche);

        [$newReject, $error, $affiche] = $service->reject($affiche, $request);

        $messageType = $error ? 'error' : 'success';

        if ($error) {
            $message = $error;
        } elseif ($newReject) {
            $affiche->current_state = $affiche->user->unit->slug;
            $affiche->status = AfficheConfig::statuses('rejected');
            $affiche->save();
            $message = 'آفیش باموفقیت رد شد';
        } else {
            $message = 'نظر شما باموفقیت بروزرسانی شد';
        }

        return redirect()->route('affiches.list', ['type' => $request['affiche_type']])->with($messageType, $message);
    }

    public function userAffiches(CarAfficheAttachingRequest $request)
    {
        $affiches = $this->userService->userAffiches(
            Auth::user(),
            $request->attached_type,
            [['status', '=', AfficheConfig::statuses('active')]]
        );

        return response()->json($affiches);
    }
}
