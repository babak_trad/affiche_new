<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $categories = Category::all();

        return view('cp.category.list-category', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cp.category.create-category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug')
        ]);

        if (!($category instanceof Category)) {
            return redirect()->route('categories.list')->with('error', 'در ایجاد دسته بندی خطایی رخ داده است.');
        }

        return redirect()->route('categories.list')->with('success', 'دسته بندی با موفقیت ایجاد شد.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category  $category
     * @return \Illuminate\View\View
     */
    public function edit(Category $category)
    {
        return view('cp.category.edit-category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Category  $category
     * @return
     */
    public function update(Category $category, Request $request)
    {
        $result = $category->update([
            'name' => $request->input('name'),
            'slug' => $request->input('slug')
        ]);

        if (!$result) {
            return redirect()->route('categories.list')->with('error', __('AuthFA.categories.edit.error'));
        }

        return redirect()->route('categories.list')->with('success', __('AuthFA.categories.edit.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category  $category
     * @return
     */
    public function delete(Category $category)
    {
        $result = $category->delete();

        if (!$result) {
            return redirect()->route('categories.list')->with('error', __('AuthFA.categories.delete.error'));
        }

        return redirect()->route('categories.list')->with('success', __('AuthFA.categories.delete.success'));
    }
}
