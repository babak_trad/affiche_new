<?php

namespace App\Http\Controllers\Link;

use App\Models\Link;
use App\Http\Controllers\Controller;

class LinksController extends Controller
{
    public function list()
    {
        $links = Link::all();

        return view('cp.link.list-link', compact('links'));
    }
}
