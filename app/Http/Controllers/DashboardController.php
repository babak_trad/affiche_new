<?php

namespace App\Http\Controllers;

use App\Models\Affiches\CarAffiche;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\Unit\UnitService;
use App\Services\User\UserService;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Support\Facades\Auth;
use App\Services\Affiche\AfficheConfig;
use App\Models\Affiches\PortableAffiche;
use App\Services\Affiche\Implementations\Services\AfficheServiceFactory;
use App\Services\Car\CarService;
use App\Services\Ware\WareConfing;
use DateInterval;
use DateTime;
use DateTimeZone;

class DashboardController extends Controller
{
    use AfficheServiceFactory;

    public function index()
    {
        return view('cp.dashboard');
    }
}
