<?php

namespace App\Http\Controllers\Message;

use App\Models\User;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        if ($user->isSuperAdmin()) {
            $messages = Message::all();
        } else {
            $messages = Message::where('user_id', $user->id)->get();
        }

        return view('cp.inbox.list-message', compact('messages'));
    }

    public function showCreateMessageForm()
    {
        return view('cp.inbox.create-message');
    }

    public function insertMessage(Request $request)
    {
        $this->validateForm($request);
        $unit = $this->create($request->all());
        return redirect()->route('message.list.form')->with('success', 'پیش نویس پیام ایجاد شد');
    }

    protected function create(array $data)
    {
        $user_id = Auth::user()->id;
        return Message::create([
            'subject' => $data['subject'],
            'message' => $data['message'],
            'user_id' => $user_id,
            'status' => 0,
        ]);
    }

    public function validateForm(Request $request)
    {
        $request->validate([
            'subject' => ['required', 'string', 'max:255'],
            'message' => ['required'],
        ]);
    }

    public function edit($id)
    {
        $message = Message::find($id);
        return view('cp.inbox.edit-message', compact('message'));
    }

    public function update(Request $request, $id)
    {
        $this->validateForm($request);
        $message = Message::find($id);
        $message->subject = $request->get('subject');
        $message->message = $request->get('message');
        $message->status = $request->get('status');
        $message->save();
        return redirect()->route('message.list.form')->with('success', 'پیوند بروزرسانی شد');
    }

    public function destroy($id)
    {
        $message = Message::find($id);
        $message->delete();
        return redirect()->route('message.list.form')->with('success', 'پیام حذف شد');
    }
}
