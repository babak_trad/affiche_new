<?php

namespace App\Http\Controllers\Notification;

use App\Models\User;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Notification\NotificationListRequest;

class NotificationController extends Controller
{

    public function list(NotificationListRequest $request)
    {
        $userId = $request->user;

        if (Auth::guard('web_admin')->check()) {
            $user = Admin::find($userId);
        } else {
            $user = User::find($userId);
        }

        $notifications = $user->unreadNotifications()->get();

        $notifications = $notifications->map->only(['id', 'data']);

        return response()->json($notifications);
    }

    public function clear(NotificationListRequest $request)
    {
        $userId = $request->user;

        if (Auth::guard('web_admin')->check()) {
            $user = Admin::find($userId);
        } else {
            $user = User::find($userId);
        }

        $result = $user->notifications()->delete();

        return response()->json(['status' => $result]);
    }
}
