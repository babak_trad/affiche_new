<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Unit;

class RegisterController extends Controller
{

    public function showUsersList()
    {
        $users = User::with(['roles', 'unit'])->get();
        return view('cp.user.list-user', compact('users'));
    }

    public function showRegistrationForm()
    {
        $units = Unit::get(['id', 'name']);
        $roles = Role::get(['id', 'name']);
        return view('cp.user.create-user', compact('units', 'roles'));
    }

    public function register(Request $request)
    {
        $this->validateForm($request);
        $this->validatePasswordForm($request);
        $user = $this->create($request->all());
        return redirect()->route('users.list')->with('success', 'کاربر ایجاد شد');
    }

    public function validateForm(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'family_name' => ['required', 'string', 'max:255'],
            'employee_num' => ['required', 'numeric'], //, 'max:11'
            'phone_num' => ['required', 'numeric'], //, 'digits:11'
            'unit_id' => ['required'],
            'role_id' => ['required'],
        ]);
    }

    public function validatePasswordForm(Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'confirmed'], //, 'min:6'
        ]);
    }

    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'family_name' => $data['family_name'],
            'employee_num' => $data['employee_num'],
            'phone_num' => $data['phone_num'],
            'unit_id' => $data['unit_id'],
            'password' => Hash::make($data['password']),
        ]);

        foreach ($data['role_id'] as $role_id) {
            $user->roles()->attach($role_id);
        }

        return $user;
    }

    public function edit($id)
    {
        $user = User::with(['unit', 'roles'])->find($id);
        $units = Unit::all();
        $roles = Role::all();
        return view('cp.user.edit-user', compact('user', 'units', 'roles'));
    }

    public function update(Request $request, int $id)
    {
        $this->validateForm($request);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->family_name = $request->get('family_name');
        $user->employee_num = $request->get('employee_num');
        $user->phone_num = $request->get('phone_num');
        $user->unit_id = $request->get('unit_id');
        $user->save();

        $user->roles()->sync($request->role_id);

        return redirect()->route('users.list')->with('success', 'اطلاعات کاربر بروزرسانی شد');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('users.list')->with('success', 'کاربر حذف شد');
    }

    public function editPassword($id)
    {
        $user = User::find($id);
        return view('cp.user.reset-password', compact('user'));
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validatePasswordForm($request);

        $user = User::find($id);
        $password = $request->get(('password'));
        $user->password = Hash::make($password);
        $user->save();
        return redirect()->route('users.list')->with('success', 'گذرواژه کاربر تغییر کرد');
    }
}
