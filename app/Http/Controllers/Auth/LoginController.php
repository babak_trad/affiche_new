<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        if ($this->attempLogin($request)) {
            return $this->sendLoginSuccessResponse();
        } else {
            return $this->sendLoginFailResponse();
        }
    }

    protected function attempLogin(Request $request)
    {
        return Auth::guard()->attempt($request->only('employee_num', 'password'));
    }

    protected function sendLoginSuccessResponse()
    {
        session()->regenerate();
        return redirect()->route('dashboard');
    }

    protected function sendLoginFailResponse()
    {
        return back()->with('badUserPass', 'شماره کارمندی یا رمز عبور نادرست است');
    }

    public function logout(Request $request)
    {
        Auth::guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->route('login');
    }
}
