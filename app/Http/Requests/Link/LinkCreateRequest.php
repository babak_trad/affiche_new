<?php

namespace App\Http\Requests\Link;

use Illuminate\Foundation\Http\FormRequest;

class LinkCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'url' => 'required|string',
            'status' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'تعیین نام الزامی است',
            'name.string' => 'نام لینک باید شامل حروف  باشد',
            'name.max' => 'نام لینک حداکثر 255  کارکتر باشد',
            'url.required' => 'تعیین آدرس لینک الزامی است',
            'url.string' => 'آدرس لینک باید شامل حروف باشد',
            'status' => 'تعیین وضعیت لینک الزامی است'
        ];
    }
}
