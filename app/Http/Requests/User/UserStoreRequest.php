<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isSuperAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'family_name' => 'required|string|max:255',
            'employee_num' => 'required|numeric|unique:users,employee_num', //, 'max:11'
            'phone_num' => 'required|numeric', //, 'digits:11'
            'unit_id' => 'required',
            'role_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'وارد کردن نام ضروری است',
            'name.string' => 'نام باید شامل حروف باشد',
            'name.max' => 'نام حداکثر 255 کارکتر باشد',
            'family_name.required' => 'وارد کردن نام خانوادگی ضروری است',
            'family_name.string' => 'نام خانوادگی باید شامل حروف باشد',
            'family_name.max' => 'نام خانوادگی حداکثر 255 کارکتر باشد',
            'employee_num.required' => 'وارد کردن شماره کارمندی الزامی است',
            'employee_num.numeric' => 'شماره کارمندی باید صرفا عددی باشد',
            'employee_num.unique' => 'در حال حاضر این شماره کارمندی موجود است',
            'unit_id.required' => 'انتخاب واحد کارمندی الزامی است',
            'role_id.required' => 'انتخاب نقش الزامی است'
        ];
    }
}
