<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_num' => 'required|exists:users',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'employee_num.required' => 'وارد کردن شماره کارمندی الزامی است',
            'employee_num.exists' => 'شماره کارمندی یا رمز عبور نادرست است',
            'password.required' => 'وارد کردن رمز عبور الزامی است'
        ];
    }
}
