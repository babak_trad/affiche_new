<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'family_name' => 'required|string|max:255',
            'employee_num' => 'required|numeric', //, 'max:11'
            'phone_num' => 'required|numeric', //, 'digits:11'
            'unit_id' => 'required',
            'role_id' => 'required',
        ];

        if (!is_null($this->all()['password'])) {
            $rules['password'] = 'min:6|confirmed';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'name.required' => 'وارد کردن نام ضروری است',
            'name.string' => 'نام باید شامل حروف باشد',
            'name.max' => 'نام حداکثر 255 کارکتر باشد',
            'family_name.required' => 'وارد کردن نام خانوادگی ضروری است',
            'family_name.string' => 'نام خانوادگی باید شامل حروف باشد',
            'family_name.max' => 'نام خانوادگی حداکثر 255 کارکتر باشد',
            'employee_num.required' => 'وارد کردن شماره کارمندی الزامی است',
            'employee_num.numeric' => 'شماره کارمندی باید صرفا عددی باشد',
            'unit_id.required' => 'انتخاب واحد کارمندی الزامی است',
            'role_id.required' => 'انتخاب نقش الزامی است'
        ];

        if (array_key_exists('password', $this->rules())) {
            $messages['password.min'] = 'رمز عبور باید حداقل شش کاراکتر باشد!';
            $messages['password.confirmed'] = 'تکرار رمز عبور را بررسی کنید!';
        }

        return $messages;
    }
}
