<?php

namespace App\Http\Requests\Car;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'plate' => [
                'required',
                'unique:cars,plate_number',
                'regex:/^[1-9][0-9]-(([1-3][0-9])|40|41|42)-[1-9][0-9]{2}-[1-9][0-9]$/'
            ]
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('validation.car.name#required'),
            'plate.required' => __('validation.car.plate#required'),
            'plate.regex' => __('validation.car.plate#regex'),
            'plate.unique' => __('validation.car.plate#unique')
        ];
    }
}
