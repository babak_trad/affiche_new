<?php

namespace App\Http\Requests\Unit;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RolesOfUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('web_admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unit' => 'required|exists:units,id'
        ];
    }

    public function messages()
    {
        return [
            'unit.required' => __('validation.unit.get_roles.unit#required'),
            'unit.exists' => __('validation.unit.get_roles.unit#exists')
        ];
    }
}
