<?php

namespace App\Http\Requests\Unit;

use Illuminate\Foundation\Http\FormRequest;

class UnitUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'department' => 'required|string|max:255',
            'slug' => 'required|string'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'وارد کردن نام ضروری است',
            'name.string' => 'نام باید شامل حروف باشد',
            'name.max' => 'نام حداکثر 255 کاراکتر باشد',
            'department.required' => 'وارد کردن نام دپارتمان ضروری است',
            'department.string' => 'نام دپارتمان باید شامل حروف باشد',
            'department.max' => 'نام دپارتمان حداکثر 255 کاراکتر باشد',
            'slug.required' => 'وارد کردن نام انگلیسی واحد الزامی است',
            'slug.string' => 'نام انگلیسی واحد باید شامل حروف باشد'
        ];
    }
}
