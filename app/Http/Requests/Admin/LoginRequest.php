<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:admins',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'وارد کردن ایمیل الزامی است',
            'email.email' => 'فرمت ایمیل نامعتبر است',
            'email.exists' => 'ایمیل یا رمز عبور نادرست است',
            'password.required' => 'وارد کردن رمز عبور الزامی است'
        ];
    }
}
