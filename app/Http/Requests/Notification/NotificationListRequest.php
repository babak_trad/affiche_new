<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class NotificationListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() || Auth::guard('web_admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Auth::guard('web_admin')->check()) {
            return [
                'user' => 'required|exists:admins,id'
            ];
        }

        return [
            'user' => 'required|exists:users,id'
        ];
    }

    public function messages()
    {
        return [
            'user.required' => __('validation.notification.user#required'),
            'user.exists' => __('validation.notification.user#exists')
        ];
    }
}
