<?php

namespace App\Http\Requests\Ware;

use Illuminate\Foundation\Http\FormRequest;

class UncountableWareCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_name' => 'required|string',
            'part_number' => 'required|string|unique:wares',
            'serial_number' => 'required|string|unique:wares',
        ];
    }

    public function messages()
    {
        //TODO: custom validation messages
        return [
            //
        ];
    }
}
