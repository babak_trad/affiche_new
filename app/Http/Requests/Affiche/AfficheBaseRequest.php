<?php

namespace App\Http\Requests\Affiche;

use Illuminate\Support\Facades\Auth;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Foundation\Http\FormRequest;

class AfficheBaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() || Auth::guard('web_admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'affiche_type' => 'required|string|in:' . implode(',', array_keys(AfficheConfig::types()))
        ];
    }

    public function messages()
    {
        return [
            'affiche_type.required' => 'نوع آفیش تعیین نشده است!',
            'affiche_type.string' => 'نوع آفیش موردنظر یافت نشد',
            'affiche_type.in' => 'نوع آفیش موردنظر یافت نشد'
        ];
    }
}
