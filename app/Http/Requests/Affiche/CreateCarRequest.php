<?php

namespace App\Http\Requests\Affiche;

use DateTime;
use App\Helpers\DateFormatter;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Foundation\Http\FormRequest;

class CreateCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->convertTimeToAValidFormat();

        $this['execution_date'] = DateFormatter::toGregorian($this['execution_date'] ?? null);

        $this['this_date'] = today()->format('Y-m-d H:i:s');

        return [
            'name' => 'required',
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i|after:start_time',
            'execution_date' => 'required|date|after_or_equal:this_date',
            'carrier' => 'required|integer',
            'attached_type' => 'string|nullable|in:' . implode(',', array_keys(AfficheConfig::types()))
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('validation.affiches.generals.name#required'),
            'start_time.required' => __('validation.affiches.car.start_time#required'),
            'start_time.date_format' => __('validation.affiches.car.start_time#date_format'),
            'end_time.required' => __('validation.affiches.car.end_time#required'),
            'end_time.date_format' => __('validation.affiches.car.end_time#date_format'),
            'end_time.after' => __('validation.affiches.car.end_time#after'),
            'execution_date.required' => __('validation.affiches.car.execution_date#required'),
            'execution_date.date' => __('validation.affiches.car.execution_date#date'),
            'execution_date.after_or_equal' => __('validation.affiches.car.execution_date#after'),
            'carrier.required' => __('validation.affiches.car.carrier#required'),
            'carrier.integer' => __('validation.affiches.car.carrier#integer'),
            'attached_type.in' => __('validation.affiches.car.attached_type#in_array')
        ];
    }

    private function convertTimeToAValidFormat()
    {
        if ($this->has('start_time')) {
            $time = null;

            try {
                $time = (new DateTime($this['start_time']))->format('H:i:s');
            } catch (\Throwable $th) {
                //throw $th;
            }

            $this['start_time'] = $time ?? 'not time!';
        }

        if ($this->has('end_time')) {
            $time = null;

            try {
                $time = (new DateTime($this['end_time']))->format('H:i:s');
            } catch (\Throwable $th) {
                //throw $th;
            }

            $this['end_time'] = $time ?? 'not time!';
        }
    }
}
