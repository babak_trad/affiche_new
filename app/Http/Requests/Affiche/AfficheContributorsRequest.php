<?php

namespace App\Http\Requests\Affiche;

use Illuminate\Foundation\Http\FormRequest;

class AfficheContributorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'affiche_type' => 'required|alpha_dash'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'ارسال شاخصه رکورد الزامی است',
            'id.integer' => 'رکورد ارسالی نامعتبر است',
            'affiche_type.required' => 'ارسال نوع آفیش الزامی است',
            'affiche_type.alpha_dash' => 'نوع ارسالی برای آفیش نامعتبر است'
        ];
    }
}
