<?php

namespace App\Http\Requests\Affiche;

use Illuminate\Support\Facades\Auth;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Foundation\Http\FormRequest;

class CarAfficheAttachingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() || Auth::guard('web_admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attached_type' => 'required|in:' . implode(',', array_keys(AfficheConfig::types()))
        ];
    }

    public function messages()
    {
        return [
            'attached_type.required' => __('validation.affiches.car.attached_type#required'),
            'attached_type.in' => __('validation.affiches.car.attached_type#in_array')
        ];
    }
}
