<?php

namespace App\Http\Requests\Affiche;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePortableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'producer' => 'required|integer',
            'director' => 'required|integer',
            'sound_recordist' => 'required',
            'cameraman' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('validation.affiches.generals.name#required'),
            'start_date.required' => __('validation.affiches.portable.start_date#required'),
            'start_date.date' => __('validation.affiches.portable.start_date#date'),
            'end_date.required' => __('validation.affiches.portable.end_date#required'),
            'end_date.date' => __('validation.affiches.portable.end_date#date'),
            'end_date.after' => __('validation.affiches.portable.end_date#after'),
            'producer.required' => __('validation.affiches.generals.producer#required'),
            'producer.integer' => __('validation.affiches.generals.producer#integer'),
            'director.required' => __('validation.affiches.generals.director#required'),
            'director.integer' => __('validation.affiches.generals.director#integer'),
            'sound_recordist.required' => __('validation.affiches.generals.sound_recordist#required'),
            'cameraman.required' => __('validation.affiches.generals.cameraman#required'),
        ];
    }
}
