<?php

namespace App\Presenters\Affiche;

use App\Presenters\Affiche\AfficheBasePresenter;
use DateTime;

class CarAffichePresenter extends AfficheBasePresenter
{
    public function type()
    {
        return 'car';
    }

    public function startDate()
    {
        $date = new DateTime($this->entity->start_date);

        return $date->format('H:i:s');
    }

    public function endDate()
    {
        $date = new DateTime($this->entity->end_date);

        return $date->format('H:i:s');
    }

    public function executionDate()
    {
        $date = new DateTime($this->entity->execution_date);

        return $date->format('Y-m-d');
    }
}
