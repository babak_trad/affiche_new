<?php

namespace App\Presenters\Affiche;

use App\Models\Unit;
use App\Presenters\Affiche\AfficheBasePresenter;

class PortableAffichePresenter extends AfficheBasePresenter
{
    public function type()
    {
        return 'portable';
    }
}
