<?php

namespace App\Presenters\Affiche;

use App\Models\Unit;
use App\Presenters\Contracts\Presenter;

class AfficheBasePresenter extends Presenter
{
    public function currentState()
    {
        return Unit::getBySlug($this->entity->current_state)->get()->first()->name;
    }

    public function status()
    {
        switch ($this->entity->status) {
            case 0:
                return $this->colorize('رد شده', 'danger');
                break;
            case 1:
                return $this->colorize('فعال', 'info');
                break;
            case 2:
                return $this->colorize('آرشیو', 'success');
                break;
            default:
                return $this->colorize('پیش نویس', 'warning');
                break;
        }
    }

    private function colorize(string $text, string $color = 'info')
    {
        switch ($color) {
            case 'success':
                $icon = "<i class='fas fa-check-double mx-1 status-icon'></i>";
                break;
            case 'danger':
                $icon = "<i class='far fa-window-close mx-1 status-icon'></i>";
                break;
            case 'warning':
                $icon = "<i class='fas fa-exclamation-circle mx-1 status-icon'></i>";
                break;
            default:
                $icon = "<i class='far fa-check-square mx-1 status-icon'></i>";
                break;
        }

        echo "<span class='badge badge-{$color} float-left ml-4 mt-1 fnt-10'>{$icon}{$text}</span>";
    }
}
