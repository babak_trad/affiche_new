<?php

namespace App\Presenters\Contracts;

use App\Exceptions\Presenter\PresenterInstanceNotFoundException;

trait Presentable
{
    protected $presenterInstance;

    public function present()
    {
        if (!$this->presenter || !class_exists($this->presenter)) {
            throw new PresenterInstanceNotFoundException("Presenter Not Found!", 1);
        }

        if (!$this->presenterInstance) {
            $this->presenterInstance = new $this->presenter($this);
        }

        return $this->presenterInstance;
    }
}
