<?php

namespace App\Presenters\Contracts;

abstract class Presenter
{
    protected $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    protected function toCamelCase($property)
    {
        $temp = explode('_', $property);

        $camel = $temp[0] . ucfirst($temp[1]);

        return $camel;
    }

    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }

        if (method_exists($this, $this->toCamelCase($property))) {
            return $this->{$this->toCamelCase($property)}();
        }

        return $this->entity->{$property};
    }
}
