<?php

namespace App\Presenters\Contracts;

use App\Services\Ware\Implementations\ConfigurerCountableWareService;
use App\Services\Ware\Implementations\ConfigurerUncountableWareService;
use App\Services\Ware\Implementations\DefaultWareService;

trait Wareable
{
    private $relatedWareTypes = ['default'];

    public function syncer(string $type)
    {
        switch ($type) {
            case 'uncountable':
                return (new DefaultWareService(resolve(ConfigurerUncountableWareService::class)))->setAffiche($this);
                break;
            case 'countable':
                return (new DefaultWareService(resolve(ConfigurerCountableWareService::class)))->setAffiche($this);
                break;
            case 'default':
                return (new DefaultWareService(null))->setAffiche($this);
                break;
            default:
                return null;
                break;
        }
    }

    /**
     * Get the value of relatedWareType
     */
    public function getRelatedWareTypes()
    {
        return $this->relatedWareTypes;
    }
}
