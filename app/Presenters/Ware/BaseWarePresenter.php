<?php

namespace App\Presenters\Ware;

use App\Presenters\Contracts\Presenter;

class BaseWarePresenter extends Presenter
{
    public function type()
    {
        switch ($this->entity->type) {
            case 1:
                return 'uncountable';
                break;
            case 2:
                return 'countable';
                break;

            default:
                return 'undefined';
                break;
        }
    }

    public function wareStatus()
    {
        switch ($this->entity->ware_status) {
            case 0:
                return $this->colorize('در انتظار بررسی', 'warning');
                break;
            case 1:
                return $this->colorize('تایید شده', 'success');
                break;
            case 2:
                return $this->colorize('رد شده', 'danger');
                break;
            default:
                return $this->colorize('نامشخص', 'info');
                break;
        }
    }

    private function colorize(string $text, string $color = 'info')
    {
        switch ($color) {
            case 'success':
                $icon = "<i class='far fa-check-square status-icon'></i>";
                break;
            case 'danger':
                $icon = "<i class='far fa-window-close status-icon'></i>";
                break;
            case 'warning':
                $icon = "<i class='fas fa-exclamation-circle status-icon'></i>";
                break;
            default:
                $icon = "<i class='far fa-question-circle status-icon'></i>";
                break;
        }

        echo "<span class='badge badge-{$color} float-left ml-4 mt-1 fnt-10'>{$icon}{$text}</span>";
    }
}
