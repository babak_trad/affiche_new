<?php

namespace App\Helpers;

use Hekmatinasser\Verta\Facades\Verta;

class DateFormatter
{

    /**
     * Convert date from Jalali to Gregorian
     *
     * @param string $jDateTime
     * @return string
     */
    public static function toGregorian($jDateTime)
    {
        $inDateFormat = preg_match("/(\d{4})-(\d+)-(\d{2}\s{1}(\d{2}):(\d{2}):(\d{2}))/i", $jDateTime);

        if (!$inDateFormat) {
            return 'Not Date';
        }

        list($jDate, $jTime) = explode(" ", $jDateTime);

        list($jYear, $jMonth, $jDay) = explode("-", $jDate);

        $gDate = Verta::getGregorian($jYear, $jMonth, $jDay);

        return implode("-", $gDate) . " " . $jTime;
    }

    public static function toJalali($gDateTime)
    {
        $inDateFormat = preg_match("/(\d{4})-(\d+)-(\d{2}\s{1}(\d{2}):(\d{2}):(\d{2}))/i", $gDateTime);

        if (!$inDateFormat) {
            return 'Not Date';
        }

        list($gdate, $gTime) = explode(" ", $gDateTime);

        list($gYear, $gMonth, $gDay) = explode("-", $gdate);

        $jDate = Verta::getJalali($gYear, $gMonth, $gDay);

        return implode("-", $jDate) . " " . $gTime;
    }
}
