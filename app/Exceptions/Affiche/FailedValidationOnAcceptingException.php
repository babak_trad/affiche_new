<?php

namespace App\Exceptions\Affiche;

use Exception;

class FailedValidationOnAcceptingException extends Exception
{
    private $validationErrors;

    public function __construct($message, $validationErrors)
    {
        $this->validationErrors = $validationErrors;
        parent::__construct();
    }

    public function getValidationErrors()
    {
        return $this->validationErrors;
    }
}
