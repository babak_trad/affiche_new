<?php

namespace App\Exceptions\AfficheWare;

use Exception;

class WareWithPendingStatusExistsException extends Exception
{
    private $errData;

    public function __construct($message, $errData)
    {
        $this->errData = $errData;
        parent::__construct($message);
    }

    public function getErrorData()
    {
        return $this->errData;
    }
}
