<?php

namespace App\Exceptions\Presenter;

use Exception;

class PresenterInstanceNotFoundException extends Exception
{
    //
}
