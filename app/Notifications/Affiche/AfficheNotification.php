<?php

namespace App\Notifications\Affiche;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\Notifications\Contracts\NotificationTextInterface;

class AfficheNotification extends Notification
{

    private $content;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(NotificationTextInterface $content)
    {
        $this->content = $content;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'html' => $this->content->html(),
            'json' => $this->content->json(),
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'html' => $this->content->html(),
            'json' => $this->content->json()
        ]);
    }
}
