<?php

namespace App\Notifications\Affiche;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\Contracts\NotificationTextInterface;

class AffichePlacedInCardboard implements NotificationTextInterface
{

    private $affiche;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Model $affiche)
    {
        $this->affiche = $affiche;
    }

    public function html()
    {
        return "<div class='notification-item'>
        آفیش با نام
        <span class='badge badge-info mx-1'>{$this->affiche->name}</span>
        در کارتابل شما قرار دارد
        </div>";
    }

    public function json()
    {
        return json_encode([
            'text' => " در کارتابل شما قرار گرفت" . $this->affiche->name . "آفیش با نام "
        ]);
    }
}
