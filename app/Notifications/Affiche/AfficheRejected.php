<?php

namespace App\Notifications\Affiche;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\Contracts\NotificationTextInterface;

class AfficheRejected implements NotificationTextInterface
{

    private $affiche;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Model $affiche)
    {
        $this->affiche = $affiche;
    }

    public function html()
    {
        return "<div class='notification-item'>
        آفیش شما با نام
        <span class='badge badge-info mx-1'>{$this->affiche->name}</span>
        <span class='badge badge-danger mx-1'>رد</span>
        شد
        </div>";
    }

    public function json()
    {
        return json_encode([
            'text' => " رد شد" . $this->affiche->name . "آفیش شما با نام "
        ]);
    }
}
