<?php

namespace App\Notifications\Contracts;

interface NotificationTextInterface
{
    public function html();

    public function json();
}
