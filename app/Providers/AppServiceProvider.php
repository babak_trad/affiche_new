<?php

namespace App\Providers;

use App\Models\Affiches\CarAffiche;
use App\Models\Affiches\PortableAffiche;
use App\Services\Affiche\AfficheAlarmService;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View as ViewFacade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'portable' => PortableAffiche::class,
            'car' => CarAffiche::class
        ]);

        //View Composers
        ViewFacade::composer('layouts.main-layout', function (View $view) {
            if (Auth::guard('web_admin')->check()) {
                $user = Auth::guard('web_admin')->user();
                $role = $user->role ? $user->role->name : '';
            } else {
                $user = Auth::user();
                $role = $user->load('roles')->roles->first()->name ?? '';
            }

            $view->with(
                [
                    'userId' => $user->id,
                    'username' => $user->name . ' ' . $user->family_name,
                    'role' => $role,
                    'unitSlug' => $user->unit ? $user->unit->slug : 'بدون واحد',
                ]
            );
        });

        ViewFacade::composer('layouts.list-affiches', function (View $view) {
            $view->with(['afficheTypes' => AfficheConfig::types()]);
        });

        // Blade Directives
        Blade::if('admin', function () {
            return Auth::guard('web_admin')->check();
        });
    }
}
