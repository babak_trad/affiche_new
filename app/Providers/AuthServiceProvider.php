<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use App\Models\Affiches\PortableAffiche;
use App\Policies\Affiche\PortableAffichePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //PortableAffiche::class => PortableAffichePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::resource('affiches', 'App\Policies\Affiche\AffichePolicy', [
            'view' => 'view',
            'create' => 'create',
            'edit' => 'edit',
            'delete' => 'delete',
            'visit' => 'visit',
            'action' => 'action',
            'reject' => 'reject',
            'archive' => 'archive',
            'view-wares' => 'viewWares',
            'edit-wares' => 'editWares'
        ]);

        Gate::resource('wares', 'App\Policies\Ware\WarePolicy', [
            'view' => 'view',
            'create' => 'create',
            'edit' => 'edit',
            'delete' => 'delete',
            'edit-affiche' => 'editAffiche',
        ]);

        Gate::define('create-car', function ($user) {
            return $user->unit ? $user->unit->slug === 'transportation' : false;
        });
    }
}
