<?php

namespace App\Scopes;

use App\Services\Affiche\AfficheConfig;

trait PortableAfficheScopes
{
    public function scopeActive($query, $userId = null)
    {
        if ($userId) {
            return $query->where([
                ['user_id', '=', $userId],
                ['status', '=', AfficheConfig::statuses('active')]
            ]);
        }

        return $query->where('status', AfficheConfig::statuses('active'));
    }
}
