<?php

namespace App\Scopes;

trait UnitScopes
{
    public function scopeGetBySlug($query, string $slug)
    {
        return $query->where('slug', '=', $slug);
    }
}
