<?php

namespace App\Scopes;

trait AfficheVisitorScopes
{
    public function scopeOwnerHasNotSeen($query, array $userAffichesId)
    {
        return $query->where('owner_has_not_seen', 0)->whereIn('affiche_id', $userAffichesId);
    }

    public function scopeVisitorAction($query, $id, $visitorId, $flow = 0)
    {
        return $query->where([
            ['affiche_id', '=', $id],
            ['visitor_id', '=', "$visitorId"],
            ['flow', '=', $flow]
        ]);
    }

    public function scopeCanVisit($query, $affiche, $unitSlug)
    {
        return $query->where([
            ['affiche_id', '=', $affiche->id],
            ['next_unit', '=', "$unitSlug"],
            ['flow', '=', $affiche->flow]
        ]);
    }
}
