<?php

namespace App\Services\Affiche;

use App\Services\Affiche\Implementations\RejectionHandling\ActionedByNextUnitRejectionHandler;
use App\Services\Affiche\Implementations\RejectionHandling\DateLimitRejectionHandler;

class AfficheRejectionService
{
    private $chain;

    public function __construct($affiche, $action)
    {
        $actionedByNextUnit = new ActionedByNextUnitRejectionHandler($action);

        $dateLimit = new DateLimitRejectionHandler($affiche, $actionedByNextUnit);

        $this->chain = $dateLimit;
    }

    /**
     * Get the value of chain
     */
    public function getChain()
    {
        return $this->chain;
    }
}
