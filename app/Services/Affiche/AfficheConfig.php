<?php

namespace App\Services\Affiche;

use App\Exceptions\Affiche\AfficheTypeNotDefinedException;

class AfficheConfig
{
    /**
     * Get the slug of next unit that should receives a proper event
     *
     * @param int $afficheType
     * @param boolean $isCreated
     * @return string
     */
    public static function nextUnit($affiche, string $authUnit)
    {
        $visitors = self::visitors($affiche->present()->type);

        $visitors = $affiche->flow === self::flow('foreward') ? $visitors : array_reverse($visitors);

        $index = array_search($authUnit, $visitors);

        if ($index === count($visitors) - 1) {
            switch ($affiche->flow) {
                case self::flow('foreward'):
                    return $visitors[$index];
                    break;
                case self::flow('backward'):
                    return $affiche->user->unit->slug;
                default:
                    return null;
                    break;
            }
        } else {
            return $visitors[$index + 1];
        }
    }

    public static function firstUnit(string $type)
    {
        $visitors = self::visitors($type);

        return reset($visitors);
    }

    public static function lastUnit(string $type)
    {
        $visitors = self::visitors($type);

        return end($visitors);
    }

    public static function visitors(string $type)
    {
        return config('my-lookup.affiche.types')[$type]['visitors'] ?? [];
    }

    public static function contributors(string $type)
    {
        return config('my-lookup.affiche.types')[$type]['contributor-roles'] ?? [];
    }

    public static function creators(string $type)
    {
        return config('my-lookup.affiche.types')[$type]['creators'] ?? [];
    }

    public static function flow(string $flow)
    {
        return config('my-lookup.affiche.flow')[$flow];
    }

    public static function statuses(string $status)
    {
        return config('my-lookup.affiche.statuses')[$status];
    }

    public static function types()
    {
        return array_map(function ($item) {
            return ['id' => $item['id'], 'name' => $item['name']];
        }, config('my-lookup.affiche.types'));
    }

    public static function typeId(string $type)
    {
        try {
            return config('my-lookup.affiche.types')[$type]['id'];
        } catch (AfficheTypeNotDefinedException $e) {
            throw $e;
        }
    }
}
