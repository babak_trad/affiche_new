<?php

namespace App\Services\Affiche\Contracts;

abstract class RejectionHandler
{
    private $successor = null;

    public function __construct(RejectionHandler $handler = null)
    {
        $this->successor = $handler;
    }

    final public function handle()
    {
        $processed = $this->process();

        if ($processed && $this->successor !== null) {
            $processed = $this->successor->handle();
        }

        return $processed;
    }

    abstract protected function process();
}
