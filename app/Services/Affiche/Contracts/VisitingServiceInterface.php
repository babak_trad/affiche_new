<?php

namespace App\Services\Affiche\Contracts;

use Illuminate\Http\Request;

interface VisitingServiceInterface
{
    public function archive(int $afficheId);

    public function action($affiche, string $action);

    public function accept($affiche, Request $request);

    public function reject($affiche, Request $request);

    public function visitedBy();
}
