<?php

namespace App\Services\Affiche\Contracts;

use App\Models\User;
use App\Models\Affiches\AfficheVisitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\Affiche\AfficheRejectionService;

abstract class AfficheVisitorContract
{
    protected static $user;

    public function __construct()
    {
        static::$user = User::with(['unit'])->find(Auth::id());
    }

    /**
     * Get the previous unit action
     *
     * @return App\Models\Affiche\AfficheVisitor
     */
    protected static function visitorAction(int $afficheId, int $flow = 0)
    {
        return AfficheVisitor::visitorAction(
            $afficheId,
            self::$user->unit->id,
            $flow
        )->get()->first();
    }

    abstract public function action($affiche, string $action);

    abstract public function accept($affiche, Request $request);

    public function reject($affiche, Request $request)
    {
        $this->validateRequest($request, 0)->validate();

        $action = self::visitorAction($affiche->id, $affiche->flow);

        $error = null;

        if ($action instanceof AfficheVisitor) {
            if ($action->action === 1) {
                try {
                    $rejectionService = new AfficheRejectionService($affiche, $action);

                    $newRejection = $rejectionService->getChain()->handle();
                } catch (\Exception $e) {
                    return [false, $e->getMessage()]; // [$newRejection, $error]
                }
            } else {
                $newRejection = false;
            }

            $affiche->visitors()->detach($action->visitor_id);
        } else {
            $newRejection = true;
        }

        return [$newRejection, $error];
    }

    abstract public function validateRequest($request, int $actionType);
}
