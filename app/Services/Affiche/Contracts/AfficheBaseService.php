<?php

namespace App\Services\Affiche\Contracts;

use Illuminate\Http\Request;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Auth;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Affiche\AfficheAccepted;
use App\Notifications\Affiche\AfficheRejected;
use App\Notifications\Affiche\AfficheNotification;
use App\Notifications\Affiche\AffichePlacedInCardboard;

abstract class AfficheBaseService
{
    /**
     * Affiche model class
     *
     * @var Illluminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Affiche type
     *
     * @var int $type
     */
    protected $type;

    /**
     * Get the proper visitor of affiche
     *
     * @var App\Services\Affiche\AfficheVisitorFactory;
     */
    protected $afficheVisitor;

    /**
     * Authenticated user with his Unit
     *
     * @var App\Models\User
     */
    protected $auth;

    public function __construct()
    {
        $this->auth = Auth::user()->load('unit');
    }

    public function all(array $relations = [])
    {
        return $this->model::with($relations)->get();
    }

    public function find(int $afficheId, array $relations = [])
    {
        return $this->model::with($relations)->find($afficheId);
    }

    abstract protected function list();

    abstract protected function create();

    abstract protected function store(Request $request);

    abstract protected function edit(int $afficheId);

    abstract protected function update(int $afficheId, Request $request);

    abstract protected function delete(int $afficheId);

    abstract protected function archive(int $afficheId);

    //abstract protected function validator(array $data);

    /**
     * Insert new record into the affiche_contributor table
     *
     * @param Affiche $affiche
     * @param aaray $contributors
     * @param string $method
     * @return void
     */
    protected function saveContributors($affiche, array $contributors, $method = 'attach')
    {
        $mapped = [];

        foreach ($contributors as $id) {
            $mapped[$id] = ['affiche_type' => AfficheConfig::typeId($this->type)];
        }

        $affiche->contributors()->{$method}($mapped);
    }

    /**
     * Get the contributors id for a specific affiche
     *
     * @param int $afficheID
     * @return Collection
     */
    public function getContributors(int $afficheId)
    {
        $affiche = $this->model::with('contributors.roles')->findOrFail($afficheId);
        return $affiche['contributors'];
    }

    /**
     * Flat the collection of ids of contributors from request
     *
     * @param Request $request
     * @return array
     */
    protected function contributorsFromRequest($request)
    {
        $predefinedContributors = config('my-lookup.affiche.types')[$this->type]['contributor-roles'];

        $contributors = collect([]);

        foreach ($predefinedContributors as $input) {
            $contributors->push($request[$input] ?? null);
        }

        return $contributors->flatten()->values()->filter(function ($item) {
            return !is_null($item);
        })->all();
    }

    protected function changeFlow($affiche, int $flow)
    {
        $unit = $this->auth['unit']->slug;

        $lastUnit = AfficheConfig::lastUnit($affiche->present()->type);

        if ($unit === $lastUnit && $flow === AfficheConfig::flow('foreward')) {
            $affiche->flow = AfficheConfig::flow('backward');

            $affiche->save();
        }
    }

    //TODO: Why public modifier??!
    public function send(int $afficheId)
    {
        $affiche = $this->model::find($afficheId);

        if ($affiche instanceof Model) {
            $affiche->status = AfficheConfig::statuses('active');
            $affiche->current_state = AfficheConfig::firstUnit($this->type);
            $affiche->save();

            $affichePlacedContent = new AffichePlacedInCardboard($affiche);

            $this->sendNotificationsForUnits($affichePlacedContent, [$affiche->current_state]);
        }

        return $affiche;
    }

    protected function sendAcceptingNotifications($affiche, ?array $nextUnits)
    {
        $acceptContent = new AfficheAccepted($affiche);

        Notification::send($affiche->user, new AfficheNotification($acceptContent));

        $affichePlacedContent = new AffichePlacedInCardboard($affiche);

        $this->sendNotificationsForUnits($affichePlacedContent, $nextUnits);
    }

    protected function sendRejectingNotifications($affiche, ?array $nextUnits)
    {
        $rejectContent = new AfficheRejected($affiche);

        Notification::send($affiche->user, new AfficheNotification($rejectContent));
    }

    protected function sendNotificationsForUnits($content, ?array $units)
    {
        if (empty($units) || is_null($units)) {
            return false;
        }

        $userService = resolve(UserService::class);

        foreach ($units as $unit) {
            $users = $userService->findByUnit($unit);

            Notification::send($users, new AfficheNotification($content));
        }
    }
}
