<?php

namespace App\Services\Affiche\Implementations\Services;

use App\Services\Affiche\Implementations\Services\CarAfficheService;
use App\Services\Affiche\Implementations\Services\PortableAfficheService;

trait AfficheServiceFactory
{
    public static function afficheService(string $type)
    {
        switch ($type) {
            case 'portable':
                return resolve(PortableAfficheService::class);
                break;
            case 'car':
                return resolve(CarAfficheService::class);
                break;
            default:
                return resolve(NullAfficheService::class);
                break;
        }
    }
}
