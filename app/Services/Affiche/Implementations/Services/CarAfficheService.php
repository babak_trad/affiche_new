<?php

namespace App\Services\Affiche\Implementations\Services;

use DateTime;
use Exception;
use Illuminate\Http\Request;
use App\Services\User\UserService;
use Illuminate\Support\Facades\DB;
use App\Models\Affiches\CarAffiche;
use Illuminate\Support\Facades\Auth;
use App\Models\Affiches\CarAfficheWare;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\Affiche\CreateCarRequest;
use App\Http\Requests\Affiche\UpdateCarRequest;
use App\Services\Affiche\Contracts\AfficheBaseService;
use App\Services\Affiche\Contracts\VisitingServiceInterface;
use App\Services\FormValidator\FormRequestValidationService;
use App\Exceptions\Affiche\FailedValidationOnAcceptingException;
use App\Services\Affiche\Implementations\Visitors\Car\CarSecurityVisitor;
use App\Services\Affiche\Implementations\Visitors\Car\CarTransportationVisitor;

class CarAfficheService extends AfficheBaseService implements VisitingServiceInterface
{
    use AfficheServiceFactory;

    protected $model = CarAffiche::class;

    protected $type = 'car';

    protected $validationService;

    private $userService;

    public function __construct()
    {
        $this->auth = Auth::user()->load('unit');

        $this->userService = resolve(UserService::class);

        $this->validationService = resolve(FormRequestValidationService::class);

        $this->validationService->setValidators([
            'store' => CreateCarRequest::class,
            'update' => UpdateCarRequest::class
        ]);
    }

    public function list()
    {
        $affiches = $this->all(['user']);

        return view('cp.affiche.car.list-car-affiche', compact('affiches'));
    }

    public function create()
    {
        $types = AfficheConfig::types();

        return view('cp.affiche.car.create-car-affiche', compact('types'));
    }

    public function store(Request $request)
    {
        // $request['execution_date'] = DateFormatter::toGregorian($request->execution_date ?? null);

        $request = resolve(CreateCarRequest::class);
        // $this->validationService->formValidator($request, 'store');

        [$startDate, $endDate] = $this->changeToDate($request->only('execution_date', 'start_time', 'end_time'));

        $current_state = AfficheConfig::firstUnit($this->type);

        DB::beginTransaction();

        try {
            $affiche = $this->model::create([
                'name' => $request->name,
                'issuer_id' => $this->auth->id,
                'carrier_id' => $request->carrier,
                'to_description' => $request->to_description,
                'for_description' => $request->for_description,
                'start_date' => $startDate,
                'end_date' => $endDate,
                'execution_date' => $request->execution_date,
                'issuer_note' => $request->issuer_note,
                'current_state' => $current_state
            ]);

            $contributorsId = $this->contributorsFromRequest($request->all());

            // bind requested contributors
            $this->saveContributors($affiche, $contributorsId);

            // set attched affiche
            if ($request->has('attached_type') && $request->has('attached_affiche')) {
                $this->saveAttachedAffiche(
                    $affiche,
                    $request->only(['attached_type', 'attached_affiche'])
                );
            }

            // bind requested wares
            if ($request->has('wares')) {
                $this->saveWares($affiche, $request->only(['wares']));
            }

            DB::commit();

            return $affiche;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function edit(int $afficheId)
    {
        $affiche = $this->model::with(['contributors', 'wares'])->find($afficheId);

        $wares = $affiche['wares']->map->only(['name', 'serial_number', 'quantity']);

        $carrier = $this->userService->findById($affiche->carrier_id, ['id', 'name', 'family_name']);

        $attached = $affiche->carable;

        $types = AfficheConfig::types();

        return view(
            'cp.affiche.car.edit-car-affiche',
            compact('affiche', 'carrier', 'attached', 'wares', 'types')
        );
    }

    public function update(int $afficheId, Request $request)
    {
        /* $request['execution_date'] = DateFormatter::toGregorian($request->execution_date);

        $this->validationService->formValidator($request, 'update')->validate(); */

        $request = resolve(UpdateCarRequest::class);

        [$startDate, $endDate] = $this->changeToDate($request->only('execution_date', 'start_time', 'end_time'));

        $affiche = $this->find($afficheId);

        DB::beginTransaction();

        try {
            $affiche->update([
                'name' => $request->name,
                'carrier_id' => $request->carrier,
                'to_description' => $request->to_description,
                'for_description' => $request->for_description,
                'start_date' => $startDate,
                'end_date' => $endDate,
                'execution_date' => $request->execution_date,
                'issuer_note' => $request->issuer_note
            ]);

            $contributorsId = $this->contributorsFromRequest($request->all());

            // bind requested contributors
            $this->saveContributors($affiche, $contributorsId, 'sync');

            // set attched affiche
            if ($request->has('attached_type') && $request->has('attached_affiche')) {
                $this->saveAttachedAffiche(
                    $affiche,
                    $request->only(['attached_type', 'attached_affiche'])
                );
            }

            // bind requested wares
            //TODO: Make validation on wares
            $this->saveWares($affiche, $request->only(['wares']));

            DB::commit();

            return $affiche;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function delete(int $afficheId)
    {
        $affiche = $this->model::with('contributors')->find($afficheId);

        $contributorsId = isset($affiche['contributors']) ? $affiche['contributors']->pluck('id')->all() : [];

        $affiche->contributors()->detach($contributorsId);

        return $affiche->delete();
    }

    public function archive(int $afficheId)
    {
        $affiche = $this->find($afficheId);

        if (!($affiche instanceof Model)) {
            return false;
        }

        $affiche->status = AfficheConfig::statuses('archive');
        $affiche->flow = AfficheConfig::flow('done');
        $affiche->save();
    }

    public function action($affiche, string $action)
    {
        return $this->visitedBy()->action($affiche, $action);
    }

    public function accept($affiche, Request $request)
    {
        // Accepting may contains an error! Check warehouse visitor class.
        try {
            $newAccept = $this->visitedBy()->accept($affiche, $request);
        } catch (FailedValidationOnAcceptingException $e) {
            return ['failedValidation' => $e->getValidationErrors()];
        }

        $nextUnit = AfficheConfig::nextUnit($affiche, $this->auth['unit']->slug);

        $affiche->visitors()->attach(
            [
                $this->auth->id => [
                    'affiche_type' => $affiche->type,
                    'action' => AfficheConfig::statuses('active'),
                    'comment' => $request->has('accepted-comment') ? $request['accepted-comment'] : 'accepted',
                    'next_unit' => $nextUnit,
                    'flow' => $affiche->flow
                ]
            ]
        );

        if ($nextUnit && $newAccept) {
            $this->sendAcceptingNotifications($affiche, [$nextUnit]);
        }

        $this->changeFlow($affiche, $affiche->flow);

        return [$nextUnit, $newAccept, $affiche];
    }

    public function reject($affiche, Request $request)
    {
        [$newReject, $error] = $this->visitedBy()->reject($affiche, $request);

        if (!$error) {
            $affiche->visitors()->attach(
                [
                    $this->auth->id => [
                        'affiche_type' => $affiche->type,
                        'action' => AfficheConfig::statuses('rejected'),
                        'comment' => $request->has('rejected-comment') ? $request['rejected-comment'] : 'rejected',
                        'next_unit' => null,
                        'flow' => $affiche->flow
                    ]
                ]
            );
        }

        if ($newReject) {
            $this->sendRejectingNotifications($affiche, null);
        }

        return [$newReject, $error, $affiche];
    }

    public function visitedBy()
    {
        $unit = $this->auth['unit']->slug;

        switch ($unit) {
            case 'transportation':
                return resolve(CarTransportationVisitor::class);
                break;
            case 'physical-security':
                return resolve(CarSecurityVisitor::class);
                break;
            default:
                return null;
                break;
        }
    }

    private function saveAttachedAffiche($affiche, $data)
    {
        $service = self::afficheService($data['attached_type']);

        if ($service instanceof NullAfficheService) {
            return false;
        }

        $attchedAffiche = $service->find((int) $data['attached_affiche']);

        if ($attchedAffiche instanceof Model) {
            $affiche->update([
                'carable_id' => $attchedAffiche->id ?? null,
                'carable_type' => $attchedAffiche->present()->type ?? null
            ]);
        }
    }

    private function saveWares($affiche, $data)
    {
        // First we dissociate all wares from affiche using scope capability
        CarAfficheWare::dissociate($affiche->id);

        // Then, add all requested wares to the affiche
        if (is_array($data['wares']) && !empty($data['wares'])) {
            foreach ($data['wares'] as $ware) {
                if ($ware) {
                    $decoded = json_decode($ware, true);

                    $affiche->wares()->create([
                        'name' => $decoded['name'] ?? null,
                        'serial_number' => $decoded['serial_number'] ?? 0,
                        'quantity' => $decoded['quantity'] ?? 1
                    ]);
                }
            }
        }
    }

    private function changeToDate(array $data)
    {
        $executionDate = explode(' ', $data['execution_date'])[0];

        $startDate = $executionDate . ' ' . $data['start_time'];

        $endDate = $executionDate . ' ' . $data['end_time'];

        return [$startDate, $endDate];
    }
}
