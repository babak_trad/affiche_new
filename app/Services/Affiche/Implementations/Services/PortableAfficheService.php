<?php

namespace App\Services\Affiche\Implementations\Services;

use Exception;
use Illuminate\Http\Request;
use App\Helpers\DateFormatter;
use App\Services\Ware\WareConfing;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Database\Eloquent\Model;
use App\Models\Affiches\PortableAffiche;
use App\Http\Requests\Affiche\CreatePortableRequest;
use App\Http\Requests\Affiche\UpdatePortableRequest;
use App\Services\Affiche\Contracts\AfficheBaseService;
use App\Services\Ware\Implementations\DefaultWareService;
use App\Services\Affiche\Contracts\VisitingServiceInterface;
use App\Services\FormValidator\FormRequestValidationService;
use App\Exceptions\Affiche\FailedValidationOnAcceptingException;
use App\Exceptions\AfficheWare\WareWithPendingStatusExistsException;
use App\Notifications\Affiche\AfficheAccepted;
use App\Notifications\Affiche\AfficheNotification;
use App\Notifications\Affiche\AffichePlacedInCardboard;
use App\Notifications\Affiche\AfficheRejected;
use App\Services\Affiche\Implementations\Visitors\Portable\PortableSecurityVisitor;
use App\Services\Affiche\Implementations\Visitors\Portable\PortableTechnicalVisitor;
use App\Services\Affiche\Implementations\Visitors\Portable\PortableWarehouseVisitor;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Notification;

class PortableAfficheService extends AfficheBaseService implements VisitingServiceInterface
{
    protected $model = PortableAffiche::class;

    protected $type = 'portable';

    protected $validationService;

    public function __construct()
    {
        $this->auth = Auth::user()->load('unit');

        $this->validationService = resolve(FormRequestValidationService::class);

        $this->validationService->setValidators([
            'store' => CreatePortableRequest::class,
            'update' => UpdatePortableRequest::class
        ]);
    }

    public function list()
    {
        $affiches = $this->all(['user']);

        return view('cp.affiche.portable.list-portable-affiche', compact('affiches'));
    }

    public function create()
    {
        $wares = $this->allWares();

        return view('cp.affiche.portable.create-portable-affiche', compact('wares'));
    }

    public function store(Request $request)
    {
        $request['start_date'] = DateFormatter::toGregorian($request['start_date']);
        $request['end_date'] = DateFormatter::toGregorian($request['end_date']);

        $formValidator = $this->validationService->formValidator($request, 'store');

        if ($formValidator) {
            $formValidator->validate();
        }

        $current_state = AfficheConfig::firstUnit($this->type);

        DB::beginTransaction();

        try {
            $affiche = $this->model::create(
                [
                    'name' => $request['name'],
                    'type' => AfficheConfig::typeId($this->type),
                    'user_id' => $this->auth->id,
                    'start_date' => $request['start_date'],
                    'end_date' => $request['end_date'],
                    'status' => AfficheConfig::statuses('draft'),
                    'current_state' => $current_state
                ]
            );

            $contributorsId = $this->contributorsFromRequest($request->all());

            $this->saveContributors($affiche, $contributorsId);

            $waresId = [
                'uncountable' => $request->input('uncountable_wares'),
                'countable' => $request->input('countable_wares')
            ];

            $affiche->syncer('default')->attachDraftWares($waresId);

            DB::commit();

            return $affiche;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function edit(int $afficheId)
    {
        $affiche = $this->find($afficheId, ['contributors.roles']);

        $afficheContributors = $affiche['contributors']->groupBy(function ($user) {
            return $user['roles']->pluck('slug')->all();
        });

        $wares = $this->allWares();

        $afficheWares = $affiche->syncer('default')->draftWares();

        return view(
            'cp.affiche.portable.edit-portable-affiche',
            compact('affiche', 'afficheContributors', 'wares', 'afficheWares')
        );
    }

    public function update(int $afficheId, Request $request)
    {
        $request['start_date'] = DateFormatter::toGregorian($request['start_date']);
        $request['end_date'] = DateFormatter::toGregorian($request['end_date']);

        $formValidator = $this->validationService->formValidator($request, 'update');

        if ($formValidator) {
            $formValidator->validate();
        }

        $affiche = $this->find($afficheId);

        DB::beginTransaction();

        try {
            $affiche->update([
                'name' => $request['name'],
                'start_date' => $request['start_date'],
                'end_date' => $request['end_date'],
            ]);

            $contributorsId = $this->contributorsFromRequest($request->all());

            $this->saveContributors($affiche, $contributorsId, 'sync');

            //TODO: Use config helper to make code extendable!
            $affiche->syncer('default')->syncDraftWares(
                [
                    'uncountable' => $request->input('uncountable_wares'),
                    'countable' => $request->input('countable_wares')
                ],
                WareConfing::status('pending'),
                false
            );

            DB::commit();

            return $affiche;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function delete(int $afficheId)
    {
        $affiche = $this->find($afficheId, ['contributors']);

        $contributorsId = $affiche['contributors']->pluck('id')->all();

        $affiche->contributors()->detach($contributorsId);

        $affiche->syncer('default')->detachDraftWares();

        return $affiche->delete();
    }

    public function archive(int $afficheId)
    {
        $affiche = $this->find($afficheId);

        if (!($affiche instanceof Model)) {
            return false;
        }

        $affiche->status = AfficheConfig::statuses('archive');
        $affiche->flow = AfficheConfig::flow('done');
        $affiche->save();

        $waresId = $affiche->syncer('default')->finalWares()->flatten()->pluck('id')->all();

        if (!empty($waresId)) {
            $affiche->syncer('default')->detachFinalWares($waresId);
        }
    }

    public function action($affiche, string $action)
    {
        return $this->visitedBy()->action($affiche, $action);
    }

    /**
     * Insert new record into affiche-role table with specific action and comment
     *
     * @param Affiche $affiche
     * @param string $comment
     * @return void
     */
    public function accept($affiche, Request $request)
    {
        // Accepting may contains an error! Check warehouse visitor class.
        try {
            $newAccept = $this->visitedBy()->accept($affiche, $request);
        } catch (WareWithPendingStatusExistsException $e) {
            return ['pendingWaresError' => $e->getErrorData()];
        } catch (FailedValidationOnAcceptingException $e) {
            return ['failedValidation' => $e->getValidationErrors()];
        }

        $nextUnit = AfficheConfig::nextUnit($affiche, $this->auth->unit->slug);

        $affiche->visitors()->attach(
            [
                $this->auth->id => [
                    'affiche_type' => $affiche->type,
                    'action' => AfficheConfig::statuses('active'),
                    'comment' => $request->has('accepted-comment') ? $request['accepted-comment'] : 'accepted',
                    'next_unit' => $nextUnit,
                    'flow' => $affiche->flow
                ]
            ]
        );

        if ($nextUnit && $newAccept) {
            $this->sendAcceptingNotifications($affiche, [$nextUnit]);
        }

        $this->changeFlow($affiche, $affiche->flow);

        return [$nextUnit, $newAccept, $affiche];
    }

    public function reject($affiche, Request $request)
    {
        [$newReject, $error] = $this->visitedBy()->reject($affiche, $request);

        if (!$error) {
            $affiche->visitors()->attach(
                [
                    $this->auth->id => [
                        'affiche_type' => $affiche->type,
                        'action' => AfficheConfig::statuses('rejected'),
                        'comment' => $request->has('rejected-comment') ? $request['rejected-comment'] : 'rejected',
                        'next_unit' => null,
                        'flow' => $affiche->flow
                    ]
                ]
            );
        }

        if ($newReject) {
            $this->sendRejectingNotifications($affiche, null);
        }

        return [$newReject, $error, $affiche];
    }

    public function visitedBy()
    {
        $unit = $this->auth['unit']->slug;

        switch ($unit) {
            case 'technical-support':
                return resolve(PortableTechnicalVisitor::class);
                break;
            case 'ware-house':
                return resolve(PortableWarehouseVisitor::class);
                break;
            case 'physical-security':
                return resolve(PortableSecurityVisitor::class);
                break;
            default:
                return null;
                break;
        }
    }

    public function allWares()
    {
        $wareService = new DefaultWareService(null);

        return $wareService->allGrouped();
    }
}
