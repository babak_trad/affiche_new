<?php

namespace App\Services\Affiche\Implementations\RejectionHandling;

use App\Services\Affiche\Contracts\RejectionHandler;

class DateLimitRejectionHandler extends RejectionHandler
{
    private $affiche;

    public function __construct($affiche, RejectionHandler $rejectionHandler)
    {
        parent::__construct($rejectionHandler);

        $this->affiche = $affiche;
    }

    protected function process()
    {
        return true;
    }
}
