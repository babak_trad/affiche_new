<?php

namespace App\Services\Affiche\Implementations\RejectionHandling;

use App\Exceptions\Affiche\RejectionHandlerExceptions\ActionedByNextUnitException;
use App\Models\Affiches\AfficheVisitor;
use App\Models\Unit;
use App\Services\Affiche\Contracts\RejectionHandler;

class ActionedByNextUnitRejectionHandler extends RejectionHandler
{

    private $action;

    public function __construct($action)
    {
        $this->action = $action;
    }

    protected function process()
    {
        //TODO: Why first record?
        $nextUnitId = Unit::where('slug', '=', $this->action->next_unit)->get()->first()->id;

        $nextUnitAction = AfficheVisitor::where([
            ['affiche_id', '=', $this->action->affiche_id],
            ['unit_id', '=', $nextUnitId],
            ['flow', '=', $this->action->flow]
        ])->get()->first();

        if ($nextUnitAction) {
            throw new ActionedByNextUnitException("این آفیش توسط واحد بعدی تأیید شده است!");
        }

        return true;
    }
}
