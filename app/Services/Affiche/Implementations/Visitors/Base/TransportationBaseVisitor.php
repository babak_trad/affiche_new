<?php

namespace App\Services\Affiche\Implementations\Visitors\Base;

use App\Models\Car;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Affiches\AfficheVisitor;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Support\Facades\Validator;
use App\Services\Affiche\Contracts\AfficheVisitorContract;

class TransportationBaseVisitor extends AfficheVisitorContract
{
    public function action($affiche, string $actionType)
    {
        $action = $this->visitorAction($affiche->id, $affiche->flow);

        if ($actionType === 'accept') {
            return view('cp.affiche.visitors.transportation-base-accept', compact('affiche', 'action'));
        } else {
            return view('cp.affiche.visitors.transportation-base-reject', compact('affiche', 'action'));
        }
    }

    public function accept($affiche, Request $request)
    {
        $request->merge(['flow' => $affiche->flow]);

        $this->validateRequest($request, 1)->validate();

        if ($affiche->flow === AfficheConfig::flow('foreward')) {
            $this->bindCarAndDriverToAffiche($request->car, $request->driver, $affiche);
        }

        $action = $this->visitorAction($affiche->id, $affiche->flow);

        if ($action || $action instanceof AfficheVisitor) {
            $newAcceptation = $action->action === 0 ? true : false;

            $affiche->visitors()->detach($action->visitor_id);
        } else {
            $newAcceptation = true;
        }

        return $newAcceptation;
    }

    public function validateRequest($request, int $actionType)
    {
        if ($actionType === 1) {
            if ($request['flow'] === AfficheConfig::flow('foreward')) {
                $rules = [
                    'car' => 'required|exists:cars,id',
                    'driver' => 'required|exists:users,id'
                ];

                $messages = [
                    'car.required' => 'تعیین خودرو الزامی است',
                    'car.exists' => 'خودروی انتخاب شده موجود نیست',
                    'driver.required' => 'تعیین راننده الزامی است',
                    'driver.exists' => 'راننده انتخاب شده موجود نیست'
                ];
            } else {
                $rules = [];
                $messages = [];
            }
        } elseif ($actionType === 0) {
            $rules = ['rejected-comment' => 'required'];

            $messages = ['rejected-comment.required' => 'وارد کردن یک کامنت الزامی است'];
        }

        return Validator::make($request->all(), $rules, $messages);
    }

    private function bindCarAndDriverToAffiche(int $carId, int $driverId, $affiche)
    {
        $car = Car::find($carId);

        $driver = User::find($driverId);

        $affiche->cars()->save($car);

        $car->driver()->associate($driver);

        $car->save();
    }
}
