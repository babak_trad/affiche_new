<?php

namespace App\Services\Affiche\Implementations\Visitors\Base;

use Illuminate\Http\Request;
use App\Helpers\DateFormatter;
use App\Models\Affiches\AfficheVisitor;
use App\Services\Affiche\AfficheConfig;
use Illuminate\Support\Facades\Validator;
use App\Services\Affiche\Contracts\AfficheVisitorContract;
use App\Exceptions\Affiche\FailedValidationOnAcceptingException;

class SecurityBaseVisitor extends AfficheVisitorContract
{
    public function action($affiche, string $actionType)
    {
        $action = $this->visitorAction($affiche->id, $affiche->flow);

        if ($actionType === 'accept') {
            return view('cp.affiche.visitors.security-base-accept', compact('affiche', 'action'));
        }
    }

    public function accept($affiche, Request $request)
    {
        $request->merge(
            [
                'flow' => $affiche->flow,
                'affiche_start_date' => DateFormatter::toGregorian($affiche->start_date),
                'affiche_end_date' => DateFormatter::toGregorian($affiche->end_date)
            ]
        );

        $validator = $this->validateRequest($request, 1);

        if ($validator->fails()) {
            throw new FailedValidationOnAcceptingException("accepting form failed!", $validator);
        }

        $action = $this->visitorAction($affiche->id, $affiche->flow);

        if ($action instanceof AfficheVisitor) {
            $newAcceptation = $action->action === 0 ? true : false;

            $affiche->visitors()->detach($action->visitor_id);
        } else {
            $newAcceptation = true;
        }

        if ($request['flow'] === AfficheConfig::flow('foreward')) {
            $affiche->security_exit_date = $request['date1'];
        } else {
            $affiche->security_enter_date = $request['date2'];
        }

        $affiche->save();

        return $newAcceptation;
    }

    public function validateRequest($request, int $actionType)
    {
        if ($actionType === 1) {
            if ($request['flow'] === AfficheConfig::flow('foreward')) {
                $request['date1'] = DateFormatter::toGregorian($request['date1']);

                $rules = ['date1' => 'required|date|before:affiche_end_date'];

                $messages = [
                    'date1.required' => 'وارد کردن تاریخ خروج عوامل الزامی است',
                    'date1.date' => 'فرمت تاریخ انتخابی نامعتبر است',
                    'date1.before' => 'تاریخ انتخابی می بایست قبل از تاریخ پایان آفیش باشد'
                ];
            } else {
                $request['date2'] = DateFormatter::toGregorian($request['date2']);

                $rules = ['date2' => 'required|date|after:affiche_start_date'];

                $messages = [
                    'date2.required' => 'وارد کردن تاریخ ورود عوامل الزامی است',
                    'date2.date' => 'فرمت تاریخ انتخابی نامعتبر است',
                    'date2.after' => 'تاریخ انتخابی می بایست بعد از تاریخ شروع آفیش باشد'
                ];
            }
        } else {
            $rules = ['rejected-comment' => 'required'];

            $messages = ['rejected-comment.required' => 'وارد کردن کامنت الزامی است'];
        }

        return Validator::make($request->all(), $rules, $messages);
    }
}
