<?php

namespace App\Services\Affiche\Implementations\Visitors\Base;

use Illuminate\Http\Request;
use App\Models\Affiches\AfficheVisitor;
use Illuminate\Support\Facades\Validator;
use App\Services\Affiche\Contracts\AfficheVisitorContract;

class TechnicalBaseVisitor extends AfficheVisitorContract
{
    public function action($affiche, string $actionType)
    {
        $action = $this->visitorAction($affiche->id, $affiche->flow);

        if ($actionType === 'accept') {
            return view('cp.affiche.visitors.technical-base-accept', compact('affiche', 'action'));
        } else {
            return view('cp.affiche.visitors.technical-base-reject', compact('affiche', 'action'));
        }
    }

    public function accept($affiche, Request $request)
    {
        $this->validateRequest($request, 1)->validate();

        $action = $this->visitorAction($affiche->id, $affiche->flow);

        if ($action || $action instanceof AfficheVisitor) {
            $newAcceptation = $action->action === 0 ? true : false;

            $affiche->visitors()->detach($action->visitor_id);
        } else {
            $newAcceptation = true;
        }

        return $newAcceptation;
    }

    public function validateRequest($request, int $actionType)
    {
        if ($actionType === 1) {
            $rules = ['accepted-comment' => 'required'];

            $messages = ['accepted-comment.required' => 'وارد کردن یک کامنت الزامی است'];
        } elseif ($actionType === 0) {
            $rules = ['rejected-comment' => 'required'];

            $messages = ['rejected-comment.required' => 'وارد کردن یک کامنت الزامی است'];
        }

        return Validator::make($request->all(), $rules, $messages);
    }
}
