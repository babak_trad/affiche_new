<?php

namespace App\Services\FormValidator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormRequestValidationService
{
    /**
     * Array of form request classes
     *
     * @var array
     */
    private $validators;

    public function formValidator(Request $request, string $method = 'store')
    {
        $validator = $this->validators[$method] ? resolve($this->validators[$method]) : null;

        return $validator ?
            Validator::make($request->all(), $validator->rules(), $validator->messages())
            : null;
    }

    /**
     * Get the value of validators
     */
    public function getValidators()
    {
        return $this->validators;
    }

    /**
     * Set the value of validators
     *
     * @return self
     */
    public function setValidators(array $validators)
    {
        $this->validators = $validators;
    }
}
