<?php

namespace App\Services\Car;

use App\Models\Car;

class CarService
{
    public function all(bool $transform = true, array $attributes = ['*'], array $relations = [])
    {
        $cars = Car::all($attributes)->load($relations);

        return $transform ? $this->transformPlate($cars) : $cars;
    }

    public function find(string $term, bool $transform = true, array $attributes = ['*'], array $relations = [])
    {
        $cars = $term ? Car::with($relations)->where('name', 'like', "%{$term}%")
            ->orWhere('plate_number', 'like', "%{$term}%")->get($attributes) : collect([]);

        return $transform ? $this->transformPlate($cars) : $cars;
    }

    public function transformPlate($collection)
    {
        return $collection->transform(function ($item) {
            list($left, $word, $right, $state) = explode('-', $item->plate_number);

            $word = __('labels.car.plate_word')[$word] ?? 'الف';

            $item->plate_number = $state . ' | ' . $right . $word . $left;

            return $item;
        });
    }
}
