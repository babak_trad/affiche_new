<?php

namespace App\Services\Role;

use App\Models\Role;

class RoleService
{
    public function findBySlug(string $slug, $relations = [])
    {
        return Role::with($relations)->where('slug', $slug)->first();
    }
}
