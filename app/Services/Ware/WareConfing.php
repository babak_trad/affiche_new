<?php

namespace App\Services\Ware;

class WareConfing
{
    public static function typeId(string $type)
    {
        return config('my-lookup.ware.types')[$type]['id'] ?? 0;
    }

    public static function status(string $status)
    {
        return config('my-lookup.ware.statuses')[$status] ?? 0;
    }
}
