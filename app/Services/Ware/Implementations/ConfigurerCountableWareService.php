<?php

namespace App\Services\Ware\Implementations;

use Illuminate\Http\Request;
use App\Http\Requests\Ware\CountableWareCreateRequest;
use App\Http\Requests\Ware\CountableWareUpdateRequest;
use App\Models\Ware;
use App\Services\FormValidator\FormRequestValidationService;
use App\Services\Ware\Contracts\WareServiceConfigurerInterface;
use App\Services\Ware\WareConfing;

class ConfigurerCountableWareService implements WareServiceConfigurerInterface
{
    private $validator;

    const LIST_VIEW = 'cp.ware.countable.list-countable-ware';

    const CREATE_VIEW = 'cp.ware.countable.create-countable-ware';

    const EDIT_VIEW = 'cp.ware.countable.edit-countable-ware';

    public function __construct()
    {
        $this->validator = resolve(FormRequestValidationService::class);

        $this->validator->setValidators([
            'store' => CountableWareCreateRequest::class,
            'update' => CountableWareUpdateRequest::class
        ]);
    }

    public function createWare(Request $request)
    {
        //TODO: Authenticated user that can create a ware will be the transferee!?
        return Ware::create(
            [
                'device_name' => $request['device_name'],
                'part_number' => $request['part_number'],
                'serial_number' => $request['serial_number'],
                'type' => WareConfing::typeId('countable'),
                'stock' => $request['stock'],
                'transferee' => auth()->id(),
            ]
        );
    }

    public function updateWare($ware, Request $request)
    {
        return $ware->update([
            'device_name' => $request->get('device_name'),
            'part_number' => $request->get('part_number'),
            'serial_number' => $request->get('serial_number'),
            'stock' => $request->get('stock')
        ]);
    }

    /**
     * Get the value of validator
     */
    public function getValidator()
    {
        return $this->validator;
    }
}
