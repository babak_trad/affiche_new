<?php

namespace App\Services\Ware\Implementations;

use Illuminate\Http\Request;
use App\Http\Requests\Ware\UncountableWareCreateRequest;
use App\Http\Requests\Ware\UncountableWareUpdateRequest;
use App\Models\Ware;
use App\Services\FormValidator\FormRequestValidationService;
use App\Services\Ware\Contracts\WareServiceConfigurerInterface;
use App\Services\Ware\WareConfing;

class ConfigurerUncountableWareService implements WareServiceConfigurerInterface
{
    private $validator;

    const LIST_VIEW = 'cp.ware.uncountable.list-uncountable-ware';

    const CREATE_VIEW = 'cp.ware.uncountable.create-uncountable-ware';

    const EDIT_VIEW = 'cp.ware.uncountable.edit-uncountable-ware';

    public function __construct()
    {
        $this->validator = resolve(FormRequestValidationService::class);

        $this->validator->setValidators([
            'store' => UncountableWareCreateRequest::class,
            'update' => UncountableWareUpdateRequest::class
        ]);
    }

    public function createWare(Request $request)
    {
        //TODO: Authenticated user that can create a ware will be the transferee!?
        return Ware::create(
            [
                'device_name' => $request['device_name'],
                'part_number' => $request['part_number'],
                'serial_number' => $request['serial_number'],
                'type' => WareConfing::typeId('uncountable'),
                'transferee' => auth()->id(),
            ]
        );
    }

    public function updateWare($ware, Request $request)
    {
        return $ware->update([
            'device_name' => $request->get('device_name'),
            'part_number' => $request->get('part_number'),
            'serial_number' => $request->get('serial_number')
        ]);
    }

    /**
     * Get the value of validator
     */
    public function getValidator()
    {
        return $this->validator;
    }
}
