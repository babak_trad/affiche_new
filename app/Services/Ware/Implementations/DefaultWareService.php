<?php

namespace App\Services\Ware\Implementations;

use App\Models\Ware;
use Illuminate\Http\Request;
use App\Services\Ware\WareConfing;
use App\Services\Ware\Contracts\WareBaseService;
use App\Exceptions\AfficheWare\NotFoundWareTypeException;
use App\Services\Ware\Contracts\AfficheWareSyncerInterface;
use App\Services\Ware\Contracts\WareServiceConfigurerInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class DefaultWareService extends WareBaseService implements AfficheWareSyncerInterface
{
    protected $model = Ware::class;

    private $configurer;

    private $affiche;

    public function __construct(?WareServiceConfigurerInterface $configurer)
    {
        if ($configurer) {
            $this->configurer = $configurer;

            $this->validationService = $configurer->getValidator();

            $this->listView = $configurer::LIST_VIEW;
            $this->createView = $configurer::CREATE_VIEW;
            $this->editView = $configurer::EDIT_VIEW;
        }
    }

    public function setAffiche($affiche)
    {
        $this->affiche = $affiche;
        return $this;
    }

    public function createWare(Request $request)
    {
        return $this->configurer->createWare($request);
    }

    public function updateWare($ware, Request $request)
    {
        return $this->configurer->updateWare($ware, $request);
    }

    public function draftWares(array $relations = [])
    {
        return $this->affiche->draftDefaultWares()->with($relations)->get()->groupBy(function ($item) {
            return $item->present()->type;
        });
    }

    public function finalWares(array $relations = [])
    {
        return $this->affiche->finalDefaultWares()->with($relations)->get()->groupBy(function ($item) {
            return $item->present()->type;
        });
    }

    public function attachDraftWares(array $waresData)
    {
        $mappedData = $this->mapWaresData($waresData, WareConfing::status('pending'));

        $this->affiche->draftDefaultWares()->attach($mappedData);
    }

    public function attachFinalWares($waresData)
    {
        switch ($waresData) {
            case is_int($waresData):
                $this->affiche->finalDefaultWares()->save($this->find($waresData));
                break;
            case is_array($waresData):
                $this->affiche->finalDefaultWares($this->findMany($waresData));
                break;
            case ($waresData instanceof Model):
                $this->affiche->finalDefaultWares()->save($waresData);
                break;
            case ($waresData instanceof Collection):
                $this->affiche->finalDefaultWares()->saveMany($waresData->flatten());
                break;
            default:
                break;
        }
    }

    public function syncDraftWares(array $waresId, int $status, bool $syncWithoutDetaching)
    {
        $data = $this->mapWaresData($waresId, $status);

        $method = $syncWithoutDetaching ? 'syncWithoutDetaching' : 'sync';

        if ($syncWithoutDetaching) {
            $this->acceptOrRejectAllDraftWares('rejected');
        }

        return $this->affiche->draftDefaultWares()->{$method}($data);
    }

    public function syncFinalWares(array $waresData)
    {
        $flattenIds = $this->flatWaresIds($waresData);

        $allFinalWares = $this->finalWares()->flatten()->values()->pluck('id')->all();

        $this->detachFinalWares($allFinalWares);

        $this->affiche->finalDefaultWares()->saveMany($this->findMany($flattenIds));
    }

    public function detachDraftWares(array $waresData = [])
    {
        if (empty($waresData)) {
            $waresData = $this->draftWares()->values()->flatten()->pluck('id')->all();
        }

        return $this->affiche->draftDefaultWares()->detach($waresData);
    }

    public function detachFinalWares(array $waresData = [])
    {
        //Detach is not available on MorphMany Relation, So we should manually update the records!
        return $this->affiche->finalDefaultWares()->whereIn('id', $waresData)->update([
            'wareable_id' => null,
            'wareable_type' => null
        ]);
    }

    public function acceptOrRejectAllDraftWares(string $status)
    {
        $allWares = $this->affiche->draftDefaultWares()->pluck('id')->all();

        $this->affiche->draftDefaultWares()->updateExistingPivot(
            $allWares,
            ['ware_status' => WareConfing::status($status)]
        );
    }

    public function waresWithPendingStatus()
    {
        $drafts = $this->draftWares()->flatten();

        return $drafts->takeWhile(function ($ware) {
            return $ware->pivot->ware_status === WareConfing::status('pending');
        });
    }

    public function validateAssociatingWares(array $wareIds)
    {
        $flattenIds = $this->flatWaresIds($wareIds);

        $wares = $this->findMany($flattenIds);

        $associated = [];

        if ($flattenIds) {
            foreach ($wares as $ware) {
                if (!$ware->wareable_id && !$ware->wareable_type) {
                    $this->affiche->finalDefaultWares()->save($ware);
                } elseif (($ware->wareable_id != $this->affiche->id) ||
                    ($ware->wareable_type != $this->affiche->present()->type)
                ) {
                    $associated[] = $ware;
                }
            }
        }

        return $associated;
    }

    private function mapWaresData(array $data, $status)
    {
        $mapped = [];

        foreach ($data as $type => $ids) {
            if (!$ids || empty($ids)) {
                break;
            }

            switch ($type) {
                case 'uncountable':
                    foreach ($ids as $id) {
                        $mapped[$id] = [
                            'ware_type' => 1,
                            'ware_quantity' => 1,
                            'ware_status' => $status
                        ];
                    }
                    break;
                case 'countable':
                    foreach ($ids as $id => $quantity) {
                        $mapped[$id] = [
                            'ware_type' =>  2,
                            'ware_quantity' => $quantity,
                            'ware_status' => $status
                        ];
                    }
                    break;
                default:
                    break;
            }
        }

        return $mapped;
    }

    private function flatWaresIds(array $data)
    {
        $mapped = [];

        foreach ($data as $type => $ids) {
            if (!$ids || empty($ids)) {
                break;
            }

            switch ($type) {
                case 'uncountable':
                    foreach ($ids as $id) {
                        array_push($mapped, $id);
                    }
                    break;
                case 'countable':
                    foreach ($ids as $id => $quantity) {
                        array_push($mapped, $id);
                    }
                    break;
                default:
                    break;
            }
        }

        return $mapped;
    }
}
