<?php

namespace App\Services\Ware\Implementations;

class NullWareService
{
    public function __call($name, $arguments)
    {
        return back()->with('badUserPass', 'خطایی رخ داده است، لطفا اطلاعات ارسالی را بررسی کنید');
    }

    public static function __callStatic($name, $arguments)
    {
        return back()->with('badUserPass', 'خطایی رخ داده است، لطفا اطلاعات ارسالی را بررسی کنید');
    }
}
