<?php

namespace App\Services\Ware;

use App\Services\Ware\Implementations\ConfigurerCountableWareService;
use App\Services\Ware\Implementations\ConfigurerUncountableWareService;
use App\Services\Ware\Implementations\DefaultWareService;
use App\Services\Ware\Implementations\NullWareService;

trait WareServiceFactory
{
    private static function wareService(string $type)
    {
        switch ($type) {
            case 'uncountable':
                return new DefaultWareService(resolve(ConfigurerUncountableWareService::class));
                break;
            case 'countable':
                return new DefaultWareService(resolve(ConfigurerCountableWareService::class));
                break;
            case 'default':
                return resolve(DefaultWareService::class);
                break;
            default:
                return resolve(NullWareService::class);
                break;
        }
    }
}
