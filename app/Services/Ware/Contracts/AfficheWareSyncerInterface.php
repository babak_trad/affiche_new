<?php

namespace App\Services\Ware\Contracts;

interface AfficheWareSyncerInterface
{
    public function draftWares(array $relations = []);

    public function finalWares(array $relations = []);

    public function syncDraftWares(array $waresData, int $status, bool $withoutDetaching);

    public function syncFinalWares(array $waresData);

    public function attachDraftWares(array $waresData);

    public function attachFinalWares(mixed $waresData);

    public function detachDraftWares(array $waresData = []);

    public function detachFinalWares(array $waresData = []);

    public function validateAssociatingWares(array $wareIds);
}
