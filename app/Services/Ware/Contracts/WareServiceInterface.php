<?php

namespace App\Services\Ware\Contracts;

use Illuminate\Http\Request;

interface WareServiceInterface
{
    public function all(string $type, array $relations);

    public function find(int $wareId, array $relations = []);

    public function findMany(array $wareId, array $relations = []);

    public function list(string $type = null);

    public function create();

    public function store(Request $request);

    public function edit(int $wareId);

    public function update(int $wareId, Request $request);

    public function delete(int $wareId);
}
