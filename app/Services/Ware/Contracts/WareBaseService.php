<?php

namespace App\Services\Ware\Contracts;

use App\Exceptions\Ware\CreateViewPropertyNotDefinedException;
use App\Exceptions\Ware\EditViewPropertyNotDefinedException;
use App\Exceptions\Ware\ListViewPropertyNotDefinedException;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\Ware\Contracts\WareServiceInterface;
use App\Services\FormValidator\FormRequestValidationService;
use App\Services\Ware\WareConfing;
use Illuminate\Support\Facades\View;

abstract class WareBaseService implements WareServiceInterface
{
    protected $model;

    protected $listView;

    protected $createView;

    protected $editView;

    protected $validationService;

    public function __construct()
    {
        $this->validationService = resolve(FormRequestValidationService::class);
    }

    abstract protected function createWare(Request $request);

    abstract protected function updateWare($ware, Request $request);

    public function all(string $type = '', array $relations = [])
    {
        if (!is_null($type) && $type !== '') {
            return $this->model::with($relations)
                ->where('type', WareConfing::typeId($type))->get();
        } else {
            return $this->model::with($relations)->get();
        }
    }

    public function allGrouped(array $relations = [])
    {
        return $this->model::with($relations)->get()->groupBy(function ($ware) {
            return $ware->present()->type;
        });
    }

    public function find(int $wareId, array $relations = [])
    {
        return $this->model::with($relations)->find($wareId);
    }

    public function findMany(array $wareId, array $relations = [])
    {
        return $this->model::with($relations)->find($wareId);
    }

    public function list(string $type = null)
    {
        $wares = $this->all($type, ['user', 'categories', 'wareable']);

        if (!property_exists($this, 'listView') || !View::exists($this->listView)) {
            throw new ListViewPropertyNotDefinedException(
                "The property 'listView' is not defined or the view does not exists. "
            );
        }

        return view($this->listView, compact('wares'));
    }

    public function create()
    {
        $categories = Category::all();

        if (!property_exists($this, 'createView') || !View::exists($this->createView)) {
            throw new CreateViewPropertyNotDefinedException(
                "The property 'createView' is not defined or the view does not exists. "
            );
        }

        return view($this->createView, compact('categories'));
    }

    public function store(Request $request)
    {
        $formValidator = $this->validationService->formValidator($request, 'store');

        if ($formValidator) {
            $formValidator->validate();
        }

        $ware = $this->createWare($request);

        $ware->categories()->attach($request->input('categories'));

        return $ware;
    }

    public function edit(int $wareId)
    {
        $categories = Category::all();

        $ware = $this->model::with(['categories', 'user'])->findOrFail($wareId);

        $wareCategories = $ware->categories->pluck('id')->all();

        if (!property_exists($this, 'editView') || !View::exists($this->editView)) {
            throw new EditViewPropertyNotDefinedException(
                "The property 'editView' is not defined or the view does not exists. "
            );
        }

        return view($this->editView, compact('ware', 'categories', 'wareCategories'));
    }

    public function update(int $wareId, Request $request)
    {
        $formValidator = $this->validationService->formValidator($request, 'update');

        if ($formValidator) {
            $formValidator->validate();
        }

        $ware = $this->model::findOrFail($wareId);

        $result = $this->updateWare($ware, $request);

        $ware->categories()->sync($request->input('categories'));

        return $result;
    }

    public function delete(int $wareId)
    {
        $ware = $this->model::with('categories')->findOrFail($wareId);

        $ware->categories()->detach($ware->categories->pluck('id')->all());

        return $ware->delete();
    }
}
