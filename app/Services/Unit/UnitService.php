<?php

namespace App\Services\Unit;

use App\Models\Unit;

class UnitService
{
    public function __construct()
    {
        # code...
    }

    public function findBySlug(string $slug, $relations = [])
    {
        return Unit::with($relations)->where('slug', $slug)->first();
    }

    public function getRoles(int $unitId)
    {
        $unit = Unit::with('roles')->find($unitId);

        return $unit['roles'];
    }
}
