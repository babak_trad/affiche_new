<?php

namespace App\Services\User;

use App\Models\User;
use App\Services\Role\RoleService;
use App\Services\Unit\UnitService;

class UserService
{
    private $result;

    private $roleService;

    private $unitService;

    public function __construct()
    {
        $this->roleService = resolve(RoleService::class);

        $this->unitService = resolve(UnitService::class);
    }

    public function all(array $attributes = ['*'], array $relations = ['roles'])
    {
        $this->result = User::with($relations)->get($attributes);

        return $this;
    }

    public function allGroupedByRole()
    {
        if ($this->result) {
            $this->result = $this->result->groupBy(function ($item) {
                return $item['roles']->pluck('slug')->all();
            });
        }

        return $this;
    }

    public function findById(int $userId, array $attributes = ['*'], array $relations = [])
    {
        return User::with($relations)->where('id', $userId)->get($attributes)->first();
    }

    public function findByEmployee(int $employeeNum, array $attributes = ['*'], array $relations = [])
    {
        return User::with($relations)->where('employee_num', $employeeNum)->get($attributes)->first();
    }

    public function findByUnit(string $slug)
    {
        $unit = $this->unitService->findBySlug($slug, ['users']);

        return $unit ? $unit['users'] : collect([]);
    }

    public function findByRole(string $slug)
    {
        $role = $this->roleService->findBySlug($slug, ['users']);

        return $role ? $role['users'] : collect([]);
    }

    public function getUnitRoles(int $unitId)
    {
        return $this->unitService->getRoles($unitId);
    }

    public function userAffiches(User $user, string $type, array $criteria)
    {
        $affiches = collect([]);

        $attributes = ['id', 'name', 'type'];

        switch ($type) {
            case 'portable':
                $affiches = $this->getQuery($user->portables(), $criteria)->get($attributes);
                break;
            case 'car':
                $affiches = $this->getQuery($user->cars(), $criteria)->get($attributes);
                break;
            default:
                break;
        }

        return $affiches;
    }

    /**
     * Get the value of result
     */
    public function getResult()
    {
        return $this->result;
    }

    public function getQuery($relation, $conditions)
    {
        return $relation->getQuery()->where(function ($query) use ($conditions) {
            foreach ($conditions as $condition) {
                $query->where($condition[0], $condition[1], $condition[2]);
            }
        });
    }
}
