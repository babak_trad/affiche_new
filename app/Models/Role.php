<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'slug', 'unit_id'];

    public $timestamps = false;

    public function admin()
    {
        return $this->hasOne(Admin::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function affiches()
    {
        return $this->belongsToMany(Affiche::class);
    }
}
