<?php

namespace App\Models\Affiches;

use App\Events\Visitors\VisitorActionCreated;
use App\Events\Visitors\VisitorActionUpdated;
use App\Scopes\AfficheVisitorScopes;
use Illuminate\Database\Eloquent\Model;

class AfficheVisitor extends Model
{
    use AfficheVisitorScopes;

    protected $guarded = [];

    protected $primaryKey = 'affiche_id';

    protected $table = 'affiche_visitor';

    public $incrementing = false;

    public $timestamps = false;

    protected $dispatchesEvents = [
        'created' => VisitorActionCreated::class,
        'updated' => VisitorActionUpdated::class
    ];
}
