<?php

namespace App\Models\Affiches;

use App\Models\Car;
use App\Models\User;
use App\Helpers\DateFormatter;
use Illuminate\Database\Eloquent\Model;
use App\Presenters\Affiche\CarAffichePresenter;
use App\Presenters\Contracts\Presentable;

class CarAffiche extends Model
{
    use Presentable;

    protected $guarded = ['id'];

    protected $table = 'car_affiches';

    protected $presenter = CarAffichePresenter::class;

    public function user()
    {
        return $this->belongsTo(User::class, 'issuer_id');
    }

    public function visitors()
    {
        return $this->belongsToMany(User::class, 'affiche_visitor', 'affiche_id', 'visitor_id');
    }

    public function contributors()
    {
        return $this->belongsToMany(User::class, 'affiche_contributor', 'affiche_id', 'user_id');
    }

    public function carable()
    {
        return $this->morphTo();
    }

    public function wares()
    {
        return $this->hasMany(CarAfficheWare::class, 'car_affiche_id');
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'affiche_id');
    }

    //---------- Accessors
    public function getExecutionDateAttribute($value)
    {
        return DateFormatter::toJalali($value);
    }

    public function getStartDateAttribute($value)
    {
        return DateFormatter::toJalali($value);
    }

    public function getEndDateAttribute($value)
    {
        return DateFormatter::toJalali($value);
    }
}
