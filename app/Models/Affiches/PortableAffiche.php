<?php

namespace App\Models\Affiches;

use App\Models\User;
use App\Models\Ware;
use App\Helpers\DateFormatter;
use App\Scopes\PortableAfficheScopes;
use App\Presenters\Contracts\Wareable;
use Illuminate\Database\Eloquent\Model;
use App\Presenters\Contracts\Presentable;
use App\Presenters\Affiche\PortableAffichePresenter;

class PortableAffiche extends Model
{
    use PortableAfficheScopes, Presentable, Wareable;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $table = 'portable_affiches';

    protected $presenter = PortableAffichePresenter::class;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function visitors()
    {
        return $this->belongsToMany(User::class, 'affiche_visitor', 'affiche_id', 'visitor_id');
    }

    public function draftDefaultWares()
    {
        return $this->morphToMany(Ware::class, 'wareable', 'wareables', 'wareable_id', 'ware_id')
            ->withPivot('ware_type', 'ware_quantity', 'ware_status');
    }

    public function finalDefaultWares()
    {
        return $this->morphMany(Ware::class, 'wareable');
    }

    public function contributors()
    {
        return $this->belongsToMany(User::class, 'affiche_contributor', 'affiche_id', 'user_id');
    }

    public function cars()
    {
        return $this->morphMany(CarAffiche::class, 'carable');
    }

    //---------- Accessors
    public function getStartDateAttribute($value)
    {
        return DateFormatter::toJalali($value);
    }

    public function getEndDateAttribute($value)
    {
        return DateFormatter::toJalali($value);
    }
}
