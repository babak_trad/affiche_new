<?php

namespace App\Models\Affiches;

use Illuminate\Database\Eloquent\Model;

class CarAfficheWare extends Model
{
    protected $guarded = ['id'];

    protected $table = 'car_affiche_wares';

    protected $primaryKey = 'serial_number';

    public $timestamps = false;

    public function affiche()
    {
        return $this->belongsTo(CarAffiche::class, 'car_affiche_id');
    }

    public static function dissociate(int $afficheId)
    {
        return self::where('car_affiche_id', $afficheId)->delete();
    }
}
