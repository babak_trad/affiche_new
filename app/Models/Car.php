<?php

namespace App\Models;

use App\Models\Affiches\CarAffiche;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = ['id'];

    public function affiche()
    {
        return $this->belongsTo(CarAffiche::class, 'affiche_id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }
}
