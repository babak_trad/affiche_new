<?php

namespace App\Models;

use App\Models\Affiches\CarAffiche;
use App\Models\Affiches\PortableAffiche;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'family_name', 'employee_num', 'phone_num', 'unit_id', 'role_id', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'users.' . $this->id;
    }

    public function portables()
    {
        return $this->hasMany(PortableAffiche::class);
    }

    public function cars()
    {
        return $this->hasMany(CarAffiche::class, 'issuer_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function wares()
    {
        return $this->hasMany(Ware::class, 'transferee', 'id');
    }

    public function isSuperAdmin()
    {
        // return $this->roles->contains('slug', 'super-admin');
        return false;
    }
}
