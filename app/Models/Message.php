<?php
namespace App\Models;



use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['subject', 'message','user_id', 'status'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
