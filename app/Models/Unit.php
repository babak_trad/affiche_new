<?php

namespace App\Models;

use App\Scopes\UnitScopes;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use UnitScopes;

    protected $fillable = ['name', 'department', 'slug'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function roles()
    {
        return $this->hasMany(Role::class);
    }
}
