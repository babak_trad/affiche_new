<?php

namespace App\Models;

use App\Models\Affiches\PortableAffiche;
use App\Presenters\Contracts\Presentable;
use App\Presenters\Ware\BaseWarePresenter;
use Illuminate\Database\Eloquent\Model;

class Ware extends Model
{
    use Presentable;

    protected $guarded = ['id'];

    protected $presenter = BaseWarePresenter::class;

    public function user()
    {
        return $this->belongsTo(User::class, 'transferee', 'id');
    }

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }

    public function wareable()
    {
        return $this->morphTo();
    }

    public function portables()
    {
        return $this->morphedByMany(PortableAffiche::class, 'wareable', 'wareables', 'ware_id', 'wareable_id');
    }
}
